﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using Wizards.Automation.Framework.Core;
using WLTR.PageObjects;
using Wizards.Automation.Framework;

namespace WLTR.Common
{
    public class WLTRCommon : WebPageBase
    {
        public static string solutionPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
        public static DirectoryInfo directory = new DirectoryInfo(
        Path.GetFullPath(Path.Combine(solutionPath, @"..\..\")));
        public static int count = 0;
        #region tags
        public static By trTag = By.TagName("tr");
        public static By tdTag = By.TagName("td");
        public static By liTag = By.TagName("li");
        public static By divTag = By.TagName("div");
        public static By iTag = By.TagName("i");
        #endregion

        #region CommonPageObjects
       public By playersListTableDiv = By.CssSelector("div#enrollment-table-region div.grid-canvas");
        public By tableRows = By.CssSelector("div[class*='ui-widget']");
       public By tableHeaders = By.CssSelector("div#enrollment-table-region div.slick-header-columns span.slick-column-name");
        #endregion


        #region spinner
        public static By spinner = By.CssSelector("div.loading-view");
        #endregion
        By modaldialog = By.ClassName("modal-dialog");
        By modalBootboxBody = By.ClassName("bootbox-body");
        By okButton = By.CssSelector("button[data-bb-handler=ok]");

        public IWebDriver driver;
        public WLTRCommon(IWebDriver driver) : base(driver)
        {
            this.driver = driver;
        }

        public void CheckServerConnection()
        {
            try
            {
                IWebElement element = driver.FindElement(modaldialog);
                if (element.Displayed)
                {
                    Sync.TextToBePresentInElementLocated(driver, modalBootboxBody, "Reconnected to localhost.");
                    if (Locate(modalBootboxBody).Text == "Reconnected to localhost.")
                    {
                        Locate(okButton).Click();
                    }
                    Sync.InvisibilityOfElementLocated(driver, modaldialog);
                }
            }
            catch(Exception)
            {

            }
        }

        /// <summary>
        /// To Create New WLTR Event
        /// </summary>
        /// 
        public void CreateEvent(string formatType,string TeamStructure)
        {
            EventPage eventPage = new EventPage(driver);
            Assert.IsTrue(eventPage.CheckWLTRPage(), "WLTR Home page not displayed");
            eventPage.ClickNewEvent();
            EventDetails eventDetails = new EventDetails(driver);
            eventDetails.FillEventDetails(formatType,TeamStructure);
            eventDetails.clickEventDetailsSaveBtn();
        }

        /// <summary>
        /// To  Check the lines count in CSV file
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>

        public int CheckLinesCountInCSV(string fileName)
        {
            string filePath = WLTRCommon.directory + ConfigurationManager.AppSettings["TestDataFolder"] + "\\" + fileName;
            Regex csvSplit = new Regex("(?:^|,)(\"(?:[^\"]+|\"\")*\"|[^,]*)", RegexOptions.Compiled);
            string[] lines = File.ReadAllLines(filePath);
            return lines.Length - 1;
        }

        /// <summary>
        /// To get column index by passing column name
        /// </summary>
        /// <param name="headerName"></param>
        /// <returns></returns>
        public int GetColNumByHeader(string headerName, By grid)
        {
            IList<IWebElement> tabHeaders = FindElementsByDriver(grid);
            int count = 0;
            foreach (IWebElement eleHeader in tabHeaders)
            {
               
                    if (eleHeader.Text.Contains(headerName))
                    {
                        return count;
                    }
                    count++;
                
            }
            return count;
        }

        /// <summary>
        /// To get column index by passing column name for right table in results tab
        /// </summary>
        /// <param name="headerName"></param>
        /// <returns></returns>
        public int GetColNumByHeader(string headerName, By parentEle, By childEle)
        {
            IList<IWebElement> tabHeaders = FindElements(parentEle, childEle);
            int count = 0;
            foreach (IWebElement eleHeader in tabHeaders)
            {

                if (eleHeader.Text.Contains(headerName))
                {
                    return count;
                }
                count++;

            }
            return count;
        }

        /// <summary>
        /// To wait for the spinner
        /// </summary>
        public void Spinner()
        {
            Thread.Sleep(3000);
            string source = driver.PageSource;
            while (source.Contains("loading-view"))
            {
                count++;
                if(count==10)
                {
                    count = 0;
                    return;
                }
                Spinner();
            }
        }

        /// <summary>
        /// To check Column value exists by using header name
        /// </summary>
        /// <param name="headerName"></param>
        /// <returns></returns>
        public bool CheckColValExistsByHeader(string headerName, string rowValue, By playersListTableDiv, By tableRows, By tableHeaders, By reqTag)
        {
            bool checkTableRow = false;
            IList<IWebElement> tabHeaders = FindElements(playersListTableDiv, tableRows);
            int colNum = GetColNumByHeader(headerName, tableHeaders);
            foreach (IWebElement ele in tabHeaders)
            {
                if (ele.Displayed)
                {
                    IList<IWebElement> colVals = FindElements(ele, reqTag);
                    IWebElement eleVal = colVals[colNum];
                    if (eleVal.Text.Replace("\r\n", " ").Contains(rowValue))
                    {
                        checkTableRow = true;
                    }
                    else
                    {
                        return false;
                    }

                }

            }

            return checkTableRow;
        }

        /// <summary>
        /// To check Column value exists by using header name
        /// </summary>
        /// <param name="headerName"></param>
        /// <returns></returns>
        public bool CheckColValExistsByHeader(string headerName, string rowValue, By playersListTableDiv, By tableHeaders, By reqTag)
        {
            bool checkTableRow = false;
            IList<IWebElement> tabHeaders = FindElementsByDriver(playersListTableDiv);
            int colNum = GetColNumByHeader(headerName, tableHeaders);
            foreach (IWebElement ele in tabHeaders)
            {
                if (ele.Displayed)
                {
                    IList<IWebElement> colVals = FindElements(ele, reqTag);
                    IWebElement eleVal = colVals[colNum];
                    if (eleVal.Text.Replace("\r\n", " ").Contains(rowValue))
                    {
                        checkTableRow = true;
                        
                    }
                    else
                    {
                        return false;
                    }

                }

            }

            return checkTableRow;
        }


        /// <summary>
        /// To check Column value not exists by using header name
        /// </summary>
        /// <param name="headerName"></param>
        /// <returns></returns>
        public bool CheckColValNotExistsByHeader(string headerName, string rqStringToCheck, By tableRows, By tableHeaders, By reqTag)
        {
            bool checkTableRow = true;
            IList<IWebElement> tableRowsElements = FindElementsByDriver(tableRows);
            int colNum = GetColNumByHeader(headerName, tableHeaders);
            foreach (IWebElement ele in tableRowsElements)
            {
                if (ele.Displayed)
                {
                    IList<IWebElement> colVals = FindElements(ele, reqTag);
                    IWebElement eleVal = colVals[colNum];
                    if (eleVal.Displayed && eleVal.Text.Contains(rqStringToCheck))
                    {
                        return false;
                    }

                }

            }

            return checkTableRow;
        }

        /// <summary>
        /// To check Column value exists by using header name in first row
        /// </summary>
        /// <param name="headerName"></param> 
        /// <returns></returns>
        public string GetColValByHeader(string headerName, By leftTableGrid, By tableHeaders, By reqTag)
        {
            IList<IWebElement> tabHeaders = FindElementsByDriver(leftTableGrid);
            IList<IWebElement> colVals = FindElements(tabHeaders[0], reqTag);
            return colVals[GetColNumByHeader(headerName, tableHeaders)].Text;
        }


        /// <summary>
        /// To Click on required row in table
        /// </summary>
        /// <param name="headerName"></param>
        /// <returns></returns>
        public void ClickOnRequiredRow(string headerName, string tableNum, By leftTableGrid, By tableHeaders, By reqTag)
        {
            IList<IWebElement> tabHeaders = FindElementsByDriver(leftTableGrid);
            int colNum = GetColNumByHeader(headerName, tableHeaders);
            foreach (IWebElement ele in tabHeaders)
            {
                if (ele.Displayed)
                {
                    IList<IWebElement> colVals = FindElements(ele, reqTag);
                    IWebElement eleVal = colVals[colNum];
                    if (eleVal.Text.Contains(tableNum))
                    {
                        eleVal.Click();
                        return;
                    }
                  
                }

            }
        }


        /// <summary>
        /// To check Column value exists by using header name and exists when found
        /// </summary>
        /// <param name="headerName"></param>
        /// <returns></returns>
        public bool CheckColValExists(string headerName, string rowValue, By playersListTableDiv, By tableHeaders, By reqTag)
        {
            bool checkTableRow = false;
            IList<IWebElement> tabHeaders = FindElementsByDriver(playersListTableDiv);
            int colNum = GetColNumByHeader(headerName, tableHeaders);
            foreach (IWebElement ele in tabHeaders) 
            {
                if (ele.Displayed)
                {
                    IList<IWebElement> colVals = FindElements(ele, reqTag);
                    IWebElement eleVal = colVals[colNum];
                    if (eleVal.Text.Replace("\r\n", " ").Contains(rowValue))
                    {
                        return true;
                    }
                   


                }

            }

            return checkTableRow;
        }


        /// <summary>
        /// To check required player exists and is enrolled or not
        /// </summary>
        /// <param name="headerName"></param>
        /// <returns></returns>
        public bool CheckCol(string rowValue1, string rowValue2, By playersListTableDiv, By tableRows)
        {
            Spinner();
            bool checkTableRow = false;
            int count = 0;
            IList<IWebElement> tabHeaders = FindElements(playersListTableDiv, tableRows);
            
            foreach (IWebElement ele in tabHeaders)
            {
                if (ele.Text.Replace("\r\n", " ").Contains(rowValue1))
                {
                    if (ele.Text.Replace("\r\n", " ").Contains(rowValue2))
                        return true;
                }
                

            }

            return checkTableRow;
        }


        /// <summary>
        /// To get required row from the list of Rows
        /// </summary>
        /// <param name="headerName"></param>
        /// <returns></returns>
        public IWebElement GetRequiredRow(By TableGrid, int index)
        {
            IList<IWebElement> tabRows = FindElementsByDriver(TableGrid);
            return tabRows[index];
        }


        /// <summary>
        /// To get required cell from the list of Rows
        /// </summary>
        /// <param name="headerName"></param>
        /// <returns></returns>
        public IWebElement GetRequiredCell(By TableGrid, int rowNum, int cellNum, By reqTag)
        {
            IList<IWebElement> tabRows = FindElementsByDriver(TableGrid);
            IList<IWebElement> tabCells = FindElements(tabRows[rowNum], reqTag);
            return tabCells[cellNum];
        }


        /// <summary>
        /// Check column value in table rows by giving column number
        /// </summary>
        /// <param name="headerName"></param>
        /// <param name="rqStringToCheck"></param>
        /// <param name="tableRows"></param>
        /// <param name="reqTag"></param>
        /// <param name="ColNum"></param>
        /// <returns></returns>
        public bool CheckColValNotExistsByHeader(string rqStringToCheck, By tableRows, By reqTag, int ColNum)
        {
            bool checkTableRow = true;
            IList<IWebElement> tableRowsElements = FindElementsByDriver(tableRows);
           // int colNum = GetColNumByHeader(headerName, tableHeaders);
            foreach (IWebElement ele in tableRowsElements)
            {
                if (ele.Displayed)
                {
                    IList<IWebElement> colVals = FindElements(ele, reqTag);
                    IWebElement eleVal = colVals[ColNum];
                    if (eleVal.Displayed && eleVal.Text.Contains(rqStringToCheck))
                    {
                        return false;
                    }

                }

            }

            return checkTableRow;
        }

    }
}

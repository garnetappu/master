﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Configuration;
using Wizards.Automation.Framework.Extensions;
using WLTR.Common;
using WLTR.PageObjects;

namespace WLTR.Tests
{
    [TestClass]
    public class WLTRSmokeTest : BaseTest
    {

        [TestMethod]
        //Create event, restart app and check event
        public void TC524124_CreateCheckEvent()
        {
            try
            {
                report.logMessage("Launched app successfully");
                WLTRCommon objCommon = new WLTRCommon(GetWebDriver());
                objCommon.CreateEvent(Constants.Format, Constants.TeamStructure);
                report.logMessage("Event created successfully");
                EventDetails eventDetails = new EventDetails(GetWebDriver());
                eventDetails.clickEventListLink();
                EventPage eventPage = new EventPage(GetWebDriver());
                Assert.IsTrue(eventPage.CheckWLTRPage(), "WLTR Home page not displayed");
                Assert.IsTrue(eventPage.VerifyEventCreated(EventDetails.eventNameRandom), "Created Event is not displayed in Premium Event list page");
                report.logMessage("Created Event is displayed in Premium Event list page");
                cleanUp();
                report.logMessage("closed the Application");
                launchApp();
                report.logMessage("restarted the Application");
                EventPage eventPage1 = new EventPage(GetWebDriver());
                Assert.IsTrue(eventPage1.CheckWLTRPage(), "WLTR Home page not displayed");
                Assert.IsTrue(eventPage1.VerifyEventCreated(EventDetails.eventNameRandom), "Created Event is not displayed in Premium Event list page");
                report.logMessage("Created Event is displayed in Premium Event list page after the restart of application");
            }
            catch (Exception e)
            {
                TestContext.Properties.Add("Error", e.Message);
                throw;
            }
        }


        [TestMethod]
        //Create simple event
        public void TC553616_CreateSimpleEvent()
        {
            try
            {
                report.logMessage("Launched app successfully");
                EventPage eventPage = new EventPage(GetWebDriver());
                eventPage.ClickNotConLink();
                eventPage.ClickConnectLink();
                eventPage.Login(testData.UserName, testData.Password);
                Assert.IsTrue(eventPage.VerifyLoginSuccess(), "Login Failed");           
                WLTRCommon objCommon = new WLTRCommon(GetWebDriver());           
                objCommon.CreateEvent(Constants.Format, Constants.TeamStructure);
                report.logMessage("Event created successfully");
                EventDetails eventDetails = new EventDetails(GetWebDriver());
                eventDetails.clickEventListLink();
                Assert.IsTrue(eventPage.VerifyEventCreated(EventDetails.eventNameRandom), "Created Event is not displayed in Premium Event list page");
                report.logMessage("Created Event is displayed in Premium Event list page");
                cleanUp();
                report.logMessage("closed the Application");

            }
            catch (Exception e)
            {
                TestContext.Properties.Add("Error", e.Message);
                throw;
            }
        }




        [TestMethod]
        //Player DCI# database search
        public void TC524960_DCIDataBaseSearch()
        {
            try
            {
                report.logMessage("Launched app successfully");
                EventPage eventPage = new EventPage(GetWebDriver());
                eventPage.ClickNotConLink();
                eventPage.ClickConnectLink();
                eventPage.Login(testData.UserName, testData.Password);
                Assert.IsTrue(eventPage.VerifyLoginSuccess(), "Login Failed");
                WLTRCommon objCommon = new WLTRCommon(GetWebDriver());
                objCommon.CreateEvent(Constants.Format, Constants.TeamStructure);
                report.logMessage("Event created successfully");
                EventDetails eventDetails = new EventDetails(GetWebDriver());
                eventDetails.clickContinuetoManageParticipantsBtn();
                EventManageParticipants manageparticipant = new EventManageParticipants(GetWebDriver());
                manageparticipant.ClickSearchResultTab();
                Assert.IsTrue(manageparticipant.VerifySearchResultTab(), "serach tab not displayed ");

                //need to add code


            }
            catch (Exception e)
            {
                TestContext.Properties.Add("Error", e.Message);
                throw;
            }
        }




        [TestMethod]
        //Import players using a .csv file Test Case
        public void TC553615_ImportPlayersValidData()
        {
            try
            {
                report.logMessage("App launched successfully");
                WLTRCommon objCommon = new WLTRCommon(GetWebDriver());
                objCommon.CreateEvent(Constants.Format, Constants.TeamStructure);
                report.logMessage("Event created successfully");
                EventDetails eventDetails = new EventDetails(GetWebDriver());
                eventDetails.clickContinuetoManageParticipantsBtn();
                report.logMessage("user is on Manage participants page");
                EventManageParticipants manageparticipant = new EventManageParticipants(GetWebDriver());
                manageparticipant.ClickToolsAndSelectOption(Constants.importText);
                report.logMessage("Import Event Players window opens with Browse Computer for File button");
                manageparticipant.ImportEventPlayers(Constants.playersCSV, Constants.validCSVSt);
                report.logMessage("Fields are populated and Import Status shows Inserted with green beside them and Import Event Players window closes.");
                Assert.AreEqual(objCommon.CheckLinesCountInCSV(Constants.playersCSV), manageparticipant.GetCountEventParticipants(), "Count in CSV and Enrollment table is not same");
                Assert.IsTrue(manageparticipant.CheckStatusOfPlayersIsEnrolled(), "Players status is not Enrolled");
                report.logMessage("Player has the status as Enrolled");
            }
            catch (Exception e)
            {
                TestContext.Properties.Add("Error", e.Message);
                throw;
            }
        }


        [TestMethod]
        //Enroll Teams Test Case
        public void TC535912_EnrollTeams()
        {
            try
            {
                report.logMessage("App launched succesfully");
                WLTRCommon objCommon = new WLTRCommon(GetWebDriver());
                objCommon.CreateEvent(Constants.Format, Constants.TeamStructure);
                report.logMessage("Event details are entered");
                EventDetails eventDetails = new EventDetails(GetWebDriver());
                eventDetails.clickContinuetoManageParticipantsBtn();
                report.logMessage("User is on Manage Participants/Event Participants subtab");
                EventManageParticipants manageparticipant = new EventManageParticipants(GetWebDriver());
                manageparticipant.ClickToolsAndSelectOption(Constants.importText);
                report.logMessage("Import Event Players window opens with Browse Computer for File button");
                manageparticipant.ImportEventPlayers(Constants.teamCSV, Constants.validCSVSt);
                report.logMessage("Fields are populated and Import Status shows Inserted with green beside them and Import Event Players window closes.");
                Assert.AreEqual(objCommon.CheckLinesCountInCSV(Constants.playersCSV), manageparticipant.GetCountEventParticipants(), "Count in CSV and Enrollment table is not same");
                Assert.IsTrue(manageparticipant.CheckStatusOfPlayersIsEnrolled(), "Players status is not Enrolled");
                report.logMessage("Player has the status as Enrolled");
            }
            catch (Exception e)
            {
                TestContext.Properties.Add("Error", e.Message);
                throw;
            }
        }


        [TestMethod]
        //Import players using a .csv files with invalid data
        public void TC524108_ImportPlayersInvalidData()
        {
            try
            {
                report.logMessage("App launched successfully");
                EventPage eventPage = new EventPage(GetWebDriver());
                Assert.IsTrue(eventPage.CheckWLTRPage(), "WLTR Home page not displayed");
                eventPage.ClickNewEvent();
                EventDetails eventDetails = new EventDetails(GetWebDriver());
                eventDetails.FillEventDetails(Constants.Format, Constants.TeamStructure);
                report.logMessage("user is on Event Details page, and user see the event details form and able to fill it in with details");
                eventDetails.enterToRangeInRequiredRound(Constants.firstEventRound, Constants.toRange9);
                eventDetails.clickRequiredSeatingCheckBx(Constants.firstEventRound);
                eventDetails.clickOnAddRoundRangeBtn();
                Assert.IsTrue(eventDetails.checkToRangeInRequiredRound(Constants.secondEventRound, Constants.toRange10), " " + Constants.toRange10 + " " + "Round Number is not displayed in To Round Range");
                eventDetails.enterToRangeInRequiredRound(Constants.secondEventRound, Constants.toRange15);
                eventDetails.selectFormatInReqEntRange(Constants.secondEventRound, Constants.Format);
                eventDetails.clickRequiredPerformCutCheckBx(Constants.secondEventRound);
                eventDetails.clickRequiredDoesNotEffAbvRadioBtn(Constants.secondEventRound);
                eventDetails.enterRequiredNotAbovePoint(Constants.secondEventRound, Constants.noOfPoints21);
                eventDetails.clickOnAddRoundRangeBtn();
                Assert.IsTrue(eventDetails.checkToRangeInRequiredRound(Constants.thirdEventRound, Constants.toRange16), " " + Constants.toRange16 + " " + "Round Number is not displayed in To Round Range");
                eventDetails.enterToRangeInRequiredRound(Constants.thirdEventRound, Constants.toRange18);
                eventDetails.selectFormatInReqEntRange(Constants.thirdEventRound, Constants.Format);
                eventDetails.selectPairingInReqEntRange(Constants.thirdEventRound, Constants.singleElimationPlayOffOpt);
                eventDetails.clickRequiredPerformCutCheckBx(Constants.thirdEventRound);
                Assert.IsTrue(eventDetails.checkDoesNotEffectFieldsEnabled(Constants.thirdEventRound), "Does Not Effect Field are not Enabled after clicking on Perform cut before check box");
                eventDetails.enterReqStandAtOrOver(Constants.thirdEventRound, Constants.standAt8);
                eventDetails.clickContinuetoManageParticipantsBtn();
                EventManageParticipants manageparticipant = new EventManageParticipants(GetWebDriver());
                manageparticipant.ClickToolsAndSelectOption(Constants.importText);
                manageparticipant.ImportEventPlayers(Constants.InvalidCSV, Constants.inValidCSVSt);
                Assert.IsTrue(manageparticipant.CheckPopUpErrTxt(Constants.popUpErrTxtImport), "Pop Up Error message is not matched or displayed when invalid csv is uploaded");
                manageparticipant.ClickOkBtnInErrPopUp();
                Assert.IsTrue(manageparticipant.CheckImportStatusErrMsgInColumns(), "Import Column Error message is not displayed");
                report.logMessage("User able to check import Error One or more players could not be imported.User able to see in the import status Not inserted and  or invalid role, DCI");
                eventDetails.enterReqStandAtOrOver(Constants.thirdEventRound, Constants.standAt8);
            }
            catch (Exception e)
            {
                TestContext.Properties.Add("Error", e.Message);
                throw;
            }

        }

        [TestMethod]
        //Create Typical Single Constructed Grand Prix Event Test Case
        public void TC553613_CreateTypicalSingleConstructedGrandPrixEvent()
        {
            try
            {
                report.logMessage("Launched app successfully");
                EventPage eventPage = new EventPage(GetWebDriver());
                Assert.IsTrue(eventPage.CheckWLTRPage(), "WLTR Home page not displayed");
                eventPage.ClickNewEvent();
                report.logMessage("user is on Event Details page");
                EventDetails eventDetails = new EventDetails(GetWebDriver());
                eventDetails.FillEventDetails(Constants.Format, Constants.TeamStructure);
                report.logMessage("App accepted all entries");
                Assert.IsTrue(eventDetails.checkToRangeInRequiredRound(Constants.firstEventRound, Constants.toRange1), " " + Constants.toRange1 + " " + "Round Number is not displayed in To Round Range");
                eventDetails.enterToRangeInRequiredRound(Constants.firstEventRound, Constants.toRange9);
                report.logMessage("Round Range 1 displayed 1 to 9 for 9 rounds in the first round range");
                eventDetails.clickRequiredSeatingCheckBx(Constants.firstEventRound);
                report.logMessage("Seating box is checked");
                eventDetails.clickOnAddRoundRangeBtn();
                Assert.IsTrue(eventDetails.checkToRangeInRequiredRound(Constants.secondEventRound, Constants.toRange10), " " + Constants.toRange10 + " " + "Round Number is not displayed in To Round Range");
                report.logMessage("Second round range is created 10 to 10");
                eventDetails.enterToRangeInRequiredRound(Constants.secondEventRound, Constants.toRange15);
                report.logMessage("Second round range is 10-15 rounds");
                eventDetails.selectFormatInReqEntRange(Constants.secondEventRound, Constants.Format);
                eventDetails.clickRequiredPerformCutCheckBx(Constants.secondEventRound);
                Assert.IsTrue(eventDetails.checkDoesNotEffectFieldsEnabled(Constants.secondEventRound), "Does Not Effect Field are not Enabled after clicking on Perform cut before check box");
                report.logMessage("Above points/Minimum Number of Players/Standing At or Over boxes are enabled");
                eventDetails.clickRequiredDoesNotEffAbvRadioBtn(Constants.secondEventRound);
                eventDetails.enterRequiredNotAbovePoint(Constants.secondEventRound, Constants.noOfPoints21);
                report.logMessage("App accepts entry");
                eventDetails.clickOnAddRoundRangeBtn();
                Assert.IsTrue(eventDetails.checkToRangeInRequiredRound(Constants.thirdEventRound, Constants.toRange16), " " + Constants.toRange16 + " " + "Round Number is not displayed in To Round Range");
                report.logMessage("Third round range is created 16 to 16");
                eventDetails.enterToRangeInRequiredRound(Constants.thirdEventRound, Constants.toRange18);
                report.logMessage("Third round range is 16-18 rounds");
                eventDetails.selectFormatInReqEntRange(Constants.thirdEventRound, Constants.Format);
                eventDetails.selectPairingInReqEntRange(Constants.thirdEventRound, Constants.singleElimationPlayOffOpt);
                eventDetails.clickRequiredPerformCutCheckBx(Constants.thirdEventRound);
                Assert.IsTrue(eventDetails.checkDoesNotEffectFieldsEnabled(Constants.thirdEventRound), "Does Not Effect Field are not Enabled after clicking on Perform cut before check box");
                report.logMessage("Above points/Minimum Number of Players/Standing At or Over boxes are enabled ");
                eventDetails.enterReqStandAtOrOver(Constants.thirdEventRound, Constants.standAt8);
                report.logMessage("App accepts entry");
                eventDetails.selectPairingInReqEntRange(Constants.thirdEventRound, Constants.singleElimationPlayOffOpt);
                report.logMessage("Pairing is now Single Elimination Playoff");
                eventDetails.clickEventDetailsSaveBtn();
                eventDetails.clickEventListLink();
                Assert.IsTrue(eventPage.CheckWLTRPage(), "WLTR Home page not displayed");
                Assert.IsTrue(eventPage.VerifyEventCreated(EventDetails.eventNameRandom), "Created Event is not displayed in Premium Event list page");
                report.logMessage("The event is successfully saved");
            }
            catch (Exception e)
            {
                TestContext.Properties.Add("Error", e.Message);
                throw;
            }
        }

        [TestMethod]
        //Create Typical Single Limited Grand Prix Event Test Case
        public void TC553612_CreateTypicalSingleLimitedGrandPrixEvent()
        {
            try
            {
                report.logMessage("Launched app successfully");
                EventPage eventPage = new EventPage(GetWebDriver());
                Assert.IsTrue(eventPage.CheckWLTRPage(), "WLTR Home page not displayed");
                eventPage.ClickNewEvent();
                report.logMessage("user is on Event Details page");
                EventDetails eventDetails = new EventDetails(GetWebDriver());
                eventDetails.FillEventDetails(Constants.formatSealed, Constants.TeamStructure);
                report.logMessage("App accepted all entries");
                report.logMessage("Sealed is selected");
                Assert.IsTrue(eventDetails.checkToRangeInRequiredRound(Constants.firstEventRound, Constants.toRange1), " " + Constants.toRange1 + " " + "Round Number is not displayed in To Round Range");
                eventDetails.enterToRangeInRequiredRound(Constants.firstEventRound, Constants.toRange9);
                report.logMessage("Round Range 1 displayed 1 to 9 for 9 rounds in the first round range");
                eventDetails.clickRequiredSeatingCheckBx(Constants.firstEventRound);
                report.logMessage("Seating box is checked.");
                eventDetails.clickOnAddRoundRangeBtn();
                Assert.IsTrue(eventDetails.checkToRangeInRequiredRound(Constants.secondEventRound, Constants.toRange10), " " + Constants.toRange10 + " " + "Round Number is not displayed in To Round Range");
                report.logMessage("A round range is added 10 to 10");
                eventDetails.enterToRangeInRequiredRound(Constants.secondEventRound, Constants.toRange12);
                report.logMessage("Second round range 10-12 is created.");
                eventDetails.selectFormatInReqEntRange(Constants.secondEventRound, Constants.formatBooster);
                report.logMessage("Booster Draft is selected in format");
                Assert.IsTrue(eventDetails.CheckDraftPodAssignmentsChecked(Constants.secondEventRound), "Draft Pod Assignment is not checked");
                Assert.IsTrue(eventDetails.CheckDeckConstAssignmentChecked(Constants.secondEventRound), "Deck Construction Assignment is not checked");
                report.logMessage("The Draft Pod Assignment and Deck Construction Assignment boxes are automatically checked.");
                eventDetails.clickRequiredSeatingCheckBx(Constants.secondEventRound);
                report.logMessage("Seating box is checked");
                eventDetails.clickRequiredPerformCutCheckBx(Constants.secondEventRound);
                Assert.IsTrue(eventDetails.checkDoesNotEffectFieldsEnabled(Constants.secondEventRound), "Does Not Effect Field are not Enabled after clicking on Perform cut before check box");
                report.logMessage("Above points/Minimum Number of Players/Standing At or Over boxes are enabled");
                eventDetails.clickRequiredDoesNotEffAbvRadioBtn(Constants.secondEventRound);
                eventDetails.enterRequiredNotAbovePoint(Constants.secondEventRound, Constants.noOfPoints21);
                report.logMessage("App accepts entry");
                eventDetails.clickOnAddRoundRangeBtn();
                Assert.IsTrue(eventDetails.checkToRangeInRequiredRound(Constants.thirdEventRound, Constants.toRange13), " " + Constants.toRange13 + " " + "Round Number is not displayed in To Round Range");
                report.logMessage("A third round range is added 13 to 13");
                eventDetails.enterToRangeInRequiredRound(Constants.thirdEventRound, Constants.toRange15);
                report.logMessage("Third round range is 13-15 rounds");
                eventDetails.selectFormatInReqEntRange(Constants.thirdEventRound, Constants.formatBooster);
                report.logMessage("Booster Draft is selected in format");
                Assert.IsTrue(eventDetails.CheckDraftPodAssignmentsChecked(Constants.thirdEventRound), "Draft Pod Assignment is not checked");
                Assert.IsTrue(eventDetails.CheckDeckConstAssignmentChecked(Constants.thirdEventRound), "Deck Construction Assignment is not checked");
                report.logMessage("The Draft Pod Assignment and Deck Construction Assignment boxes are automatically checked.");
                eventDetails.clickRequiredSeatingCheckBx(Constants.thirdEventRound);
                report.logMessage("Seating box is checked");
                eventDetails.clickOnAddRoundRangeBtn();
                Assert.IsTrue(eventDetails.checkToRangeInRequiredRound(Constants.fourthEventRound, Constants.toRange16), " " + Constants.toRange16 + " " + "Round Number is not displayed in To Round Range");
                report.logMessage("A fourth round range is added 16 to 16");
                eventDetails.enterToRangeInRequiredRound(Constants.fourthEventRound, Constants.toRange18);
                report.logMessage("Fourth round range is 16-18");
                eventDetails.selectFormatInReqEntRange(Constants.fourthEventRound, Constants.formatBooster);
                report.logMessage("Booster Draft is selected in format");
                Assert.IsTrue(eventDetails.CheckDraftPodAssignmentsChecked(Constants.fourthEventRound), "Draft Pod Assignment is not checked");
                Assert.IsTrue(eventDetails.CheckDeckConstAssignmentChecked(Constants.fourthEventRound), "Deck Construction Assignment is not checked");
                report.logMessage("The Draft Pod Assignment and Deck Construction Assignment boxes are automatically checked.");
                eventDetails.clickRequiredSeatingCheckBx(Constants.fourthEventRound);
                report.logMessage("Seating box is checked");
                eventDetails.selectPairingInReqEntRange(Constants.fourthEventRound, Constants.singleElimationPlayOffOpt);
                report.logMessage("Single Elimination Playoff is selected ");
                eventDetails.clickRequiredPerformCutCheckBx(Constants.fourthEventRound);
                Assert.IsTrue(eventDetails.checkDoesNotEffectFieldsEnabled(Constants.fourthEventRound), "Does Not Effect Field are not Enabled after clicking on Perform cut before check box");
                report.logMessage("Above points/Minimum Number of Players/Standing At or Over boxes are enabled ");
                eventDetails.ClickReqStandAtOverRadioInReqEntRange(Constants.fourthEventRound);
                eventDetails.enterReqStandAtOrOver(Constants.fourthEventRound, Constants.standAt8);
                report.logMessage("App accepts entry");
                eventDetails.clickEventDetailsSaveBtn();
                eventDetails.clickEventListLink();
                Assert.IsTrue(eventPage.CheckWLTRPage(), "WLTR Home page not displayed");
                Assert.IsTrue(eventPage.VerifyEventCreated(EventDetails.eventNameRandom), "Created Event is not displayed in Premium Event list page");
                report.logMessage("Event is successfully saved");
            }
            catch (Exception e)
            {
                TestContext.Properties.Add("Error", e.Message);
                throw;
            }
        }

        [TestMethod]
        //To test Launching of WLTR application
        public void TC553614_LaunchApp()
        {
            try
            {

                EventPage eventPage = new EventPage(GetWebDriver());
                Assert.IsTrue(eventPage.CheckWLTRPage(), "WLTR Home page not displayed");
                report.logMessage("App launched successfully");

            }
            catch (Exception e)
            {
                TestContext.Properties.Add("Error", e.Message);
                throw;
            }
        }


        [TestMethod]
        //Un-Seat single players (Seating phase) Test Case
        public void TC524129_UnseatSinglePlayer()
        {
            try
            {
                report.logMessage("App is launched successfully");
                WLTRCommon objCommon = new WLTRCommon(GetWebDriver());
                objCommon.CreateEvent(Constants.Format, Constants.TeamStructure);
                EventDetails eventDetails = new EventDetails(GetWebDriver());
                eventDetails.clickRequiredSeatingCheckBx(Constants.firstEventRound);
                report.logMessage("Event created successfully");
                eventDetails.clickContinuetoManageParticipantsBtn();
                report.logMessage("user is on Manage Event page");
                EventManageParticipants manageparticipant = new EventManageParticipants(GetWebDriver());
                manageparticipant.ClickToolsAndSelectOption(Constants.importText);
                report.logMessage("Import Event Players window opens with Browse Computer for File button");
                manageparticipant.ImportEventPlayers(Constants.playersCSV, Constants.validCSVSt);
                report.logMessage("Fields are populated and Import Status shows Inserted with green beside them and Import Event Players window closes.");
                Assert.AreEqual(objCommon.CheckLinesCountInCSV(Constants.playersCSV), manageparticipant.GetCountEventParticipants(), "Count in CSV and Enrollment table is not same");
                Assert.IsTrue(manageparticipant.CheckStatusOfPlayersIsEnrolled(), "Players status is not Enrolled");
                report.logMessage("Player has the status as Enrolled");
                manageparticipant.ClickOnContinueToManageEvent();
                ManageEvent mEvent = new ManageEvent(GetWebDriver());
                Assert.IsTrue(mEvent.CheckUnseatingOfPlayers(Constants.headerPlayer1, Constants.headerPlayer2, Constants.playerRole));
                report.logMessage("The selcted players are unseated and moved to the Unused column ");

            }
            catch (Exception e)
            {
                TestContext.Properties.Add("Error", e.Message);
                throw;
            }
        }

        [TestMethod]
        //To validate login to WLTR using a authorized using credentials configured in accounts page and Primev2
        public void TC610037_LoginToWLTR()
        {
            try
            {
                report.logMessage("app lauched successfully");
                EventPage eventPage = new EventPage(GetWebDriver());
                eventPage.ClickNotConLink();
                eventPage.ClickConnectLink();
                eventPage.Login(testData.UserName, testData.Password);
                Assert.IsTrue(eventPage.VerifyLoginSuccess(), "Login Failed");
                report.logMessage("login to app successfully");

            }
            catch (Exception e)
            {
                TestContext.Properties.Add("Error", e.Message);
                throw;
            }
        }

        [TestMethod]
        //Scorelock Method - single event Test Case
        public void TC534559_ScorelockMethodSingleEvent()
        {
            try
            {
                report.logMessage("App launched successfully");
                WLTRCommon objCommon = new WLTRCommon(GetWebDriver());
                objCommon.CreateEvent(Constants.Format, Constants.TeamStructure);
                report.logMessage("Event has been created");
                EventDetails eventDetails = new EventDetails(GetWebDriver());
                eventDetails.clickContinuetoManageParticipantsBtn();
                report.logMessage("User gets taken to Manage Participants tab");
                EventManageParticipants manageparticipant = new EventManageParticipants(GetWebDriver());
                manageparticipant.ClickToolsAndSelectOption(Constants.importText);
                manageparticipant.ImportEventPlayers(Constants.playersCSV, Constants.validCSVSt);
                Assert.AreEqual(objCommon.CheckLinesCountInCSV(Constants.playersCSV), manageparticipant.GetCountEventParticipants(), "Count in CSV and Enrollment table is not same");
                Assert.IsTrue(manageparticipant.CheckStatusOfPlayersIsEnrolled(), "Players status is not Enrolled");
                report.logMessage("User is ready to move to the Manage Event phase");
                manageparticipant.ClickOnContinueToManageEvent();
                ManageEvent mEvent = new ManageEvent(GetWebDriver());
                mEvent.ClickOnContinueBtnInMatching();
                mEvent.ClickScoreLockRadio();
                report.logMessage("Scorelock method is selected");
                Assert.IsFalse(mEvent.CheckScoreCheckBoxGrayedOut(), "Score Check Box is still Enabled");
                report.logMessage("Score text box grayed out and should not able to enter values in it");
                mEvent.MoveReqPlayerToReportedTable(Constants.headerNameTb, Constants.datakey1);
                Assert.IsTrue(mEvent.CheckPlayerIsRemovedFromNotReportedTab(Constants.datakey1), "Players not deleted from left side table in Results");
                Assert.IsTrue(mEvent.CheckPlayerIsAddedToReportedTab(Constants.datakey1), "Players is not added in right side table in Results");
                Assert.IsTrue(mEvent.CheckScoreAssignedToPlayersByKeys(Constants.headerNameScore, Constants.datakey1Score), "Players is not added with correct score according to keys right side table in Results");
                report.logMessage("The score is entered and displayed in Reported column");
            }
            catch (Exception e)
            {
                TestContext.Properties.Add("Error", e.Message);
                throw;
            }

        }

        [TestMethod]
        //Create new player Test Case
        public void TC525277_CreateNewPlayer()
        {
            try
            {
                report.logMessage("Launched app successfully");
                WLTRCommon objCommon = new WLTRCommon(GetWebDriver());
                objCommon.CreateEvent(Constants.Format, Constants.TeamStructure);
                report.logMessage("Event created successfully");
                EventDetails eventDetails = new EventDetails(GetWebDriver());
                eventDetails.clickContinuetoManageParticipantsBtn();
                EventManageParticipants eventMngParticipants = new EventManageParticipants(GetWebDriver());
                eventMngParticipants.ClickCreateNewPlayerBtn();
                eventMngParticipants.FillPlayerDetails();
                eventMngParticipants.SavePlayerDetails();
                Assert.IsTrue(eventMngParticipants.VerifyPlayerCreated());
                report.logMessage("New Player created successfully");

            }
            catch (Exception e)
            {
                TestContext.Properties.Add("Error", e.Message);
                throw;
            }

        }

        [TestMethod]
        //Assign Penalty from Player Card Test Case
        public void TC526952_AssignPenaltyfromPlayer()
        {
            try
            {

                report.logMessage("Launched app successfully");
                WLTRCommon objCommon = new WLTRCommon(GetWebDriver());
                objCommon.CreateEvent(Constants.Format, Constants.TeamStructure);
                report.logMessage("Event created successfully");
                EventDetails eventDetails = new EventDetails(GetWebDriver());
                eventDetails.clickContinuetoManageParticipantsBtn();
                EventManageParticipants eventMngParticipants = new EventManageParticipants(GetWebDriver());
                eventMngParticipants.ClickToolsAndSelectOption(Constants.importText);
                eventMngParticipants.ImportEventPlayers(Constants.playersCSV, Constants.validCSVSt);
                eventMngParticipants.ClickOnContinueToManageEvent();
                ManageEvent manageEvent = new ManageEvent(GetWebDriver());
                manageEvent.ClickOnContinueBtnInMatching();
                Assert.IsTrue(manageEvent.OpenParticipantCard(Constants.ResultsLeftTabName));
                report.logMessage("playercardopens with correct player");
                eventMngParticipants.ClickPenaltiesTab();
                Assert.IsTrue(eventMngParticipants.VerifyPenaltiesTab());
                eventMngParticipants.ClickpenaltyNewBtn();
                eventMngParticipants.FillPenaltyDetails();
                eventMngParticipants.SavePenality();
                Assert.IsTrue(eventMngParticipants.VerifyPenalityDetails());
                report.logMessage("penality added successfully");


            }
            catch (Exception e)
            {
                TestContext.Properties.Add("Error", e.Message);
                throw;
            }

        }

        [TestMethod]
        //Submit Results by Keyboard - Hotkey Method - Single Test Case
        public void TC524087_SubmitResultsKeyboardHotkeyMethod()
        {
            try
            {
                report.logMessage("App is launched successfully");
                WLTRCommon objCommon = new WLTRCommon(GetWebDriver());
                objCommon.CreateEvent(Constants.Format, Constants.TeamStructure);
                EventDetails eventDetails = new EventDetails(GetWebDriver());
                eventDetails.clickContinuetoManageParticipantsBtn();
                report.logMessage("User is on Manage Participants page ");
                EventManageParticipants manageparticipant = new EventManageParticipants(GetWebDriver());
                manageparticipant.ClickToolsAndSelectOption(Constants.importText);
                manageparticipant.ImportEventPlayers(Constants.playersCSV, Constants.validCSVSt);
                manageparticipant.ClickOnContinueToManageEvent();
                ManageEvent mEvent = new ManageEvent(GetWebDriver());
                report.logMessage("User is on Match Tables phase");
                mEvent.ClickOnContinueBtnInMatching();
                report.logMessage("User is on Results phase");
                mEvent.ClickHotKeyRadio();
                Assert.IsTrue(mEvent.CheckScoresForAllHotKeys(), "Checking hot keys values for numeric values got failed");
                report.logMessage("Results are entered for the round");
            }
            catch (Exception e)
            {
                TestContext.Properties.Add("Error", e.Message);
                throw;
            }

        }

        [TestMethod]
        //Reopen a Finished Event
        public void TC528744_ReopenFinishedEvent()
        {
            try
            {
                WLTRCommon objCommon = new WLTRCommon(GetWebDriver());
                objCommon.CreateEvent(Constants.Format, Constants.TeamStructure);
                EventDetails eventDetails = new EventDetails(GetWebDriver());
                eventDetails.clickRequiredSeatingCheckBx(Constants.firstEventRound);
                eventDetails.clickContinuetoManageParticipantsBtn();
                EventManageParticipants manageparticipant = new EventManageParticipants(GetWebDriver());
                manageparticipant.ClickToolsAndSelectOption(Constants.importText);
                manageparticipant.ImportEventPlayers(Constants.playersCSV, Constants.validCSVSt);
                manageparticipant.ClickOnContinueToManageEvent();
                ManageEvent mEvent = new ManageEvent(GetWebDriver());
                mEvent.ClickOnContinueBtnInSeating();
                mEvent.ClickOnContinueBtnInMatching();
                mEvent.ClickScoreLockRadio();
                Assert.IsFalse(mEvent.CheckScoreCheckBoxGrayedOut(), "Score Check Box is still Enabled");
                Assert.IsTrue(mEvent.CheckScoresForAllHotKeys(), "Checking hot keys values for numeric values got failed");
                ///Need to add more code....
            }
            catch (Exception e)
            {
                TestContext.Properties.Add("Error", e.Message);
                throw;
            }

        }

        [TestMethod]
        public void TC528749_Verifyusercanrightclicktoopenparticipantcard()
        {
            try
            {

                report.logMessage("Launched app successfully");
                EventPage eventPage = new EventPage(GetWebDriver());
                Assert.IsTrue(eventPage.CheckWLTRPage(), "WLTR Home page not displayed");
                eventPage.ClickNewEvent();
                EventDetails eventDetails = new EventDetails(GetWebDriver());
                eventDetails.FillEventDetails(Constants.Format, Constants.TeamStructure);
                eventDetails.clickRequiredSeatingCheckBx(Constants.firstEventRound);
                eventDetails.clickEventDetailsSaveBtn();
                report.logMessage("Event created successfully");
                eventDetails.clickContinuetoManageParticipantsBtn();
                EventManageParticipants eventMngParticipants = new EventManageParticipants(GetWebDriver());
                eventMngParticipants.ClickToolsAndSelectOption(Constants.importText);
                eventMngParticipants.ImportEventPlayers(Constants.playersCSV, Constants.validCSVSt);
                eventMngParticipants.ClickOnContinueToManageEvent();
                ManageEvent manageEvent = new ManageEvent(GetWebDriver());
                Assert.IsTrue(manageEvent.OpenParticipantCard(Constants.SeatingTabName));
                report.logMessage("Participant card in seating phase opened successfully");
                eventMngParticipants.PlayerCardClose();
                manageEvent.ClickOnContinueBtnInSeating();
                Assert.IsTrue(manageEvent.OpenParticipantCard(Constants.MatchTablesTabName));
                report.logMessage("Participant card in Matching tables phase opened successfully");
                eventMngParticipants.PlayerCardClose();
                manageEvent.ClickOnContinueBtnInMatching();
                Assert.IsTrue(manageEvent.OpenParticipantCard(Constants.ResultsLeftTabName));
                report.logMessage("Participant card in Results Left grid opened successfully");
                eventMngParticipants.PlayerCardClose();
                manageEvent.EnterAllResultsbyScorelockMethod(Constants.headerNameTb);
                Assert.IsTrue(manageEvent.OpenParticipantCard(Constants.ResultsRightTabName));
                report.logMessage("Participant card in Results right grid opened successfully");
                eventMngParticipants.PlayerCardClose();
                eventDetails.clickEventListLink();
                WLTRCommon objCommon = new WLTRCommon(GetWebDriver());
                objCommon.CreateEvent(Constants.formatBooster, Constants.TeamStructure);
                report.logMessage("Limited Event created successfully");
                eventDetails.clickContinuetoManageParticipantsBtn();
                eventMngParticipants.ClickToolsAndSelectOption(Constants.importText);
                eventMngParticipants.ImportEventPlayers(Constants.playersCSV, Constants.validCSVSt);
                eventMngParticipants.ClickOnContinueToManageEvent();
                Assert.IsTrue(manageEvent.OpenParticipantCard(Constants.PodsTabName));
                report.logMessage("Participant card in Pods right grid opened successfully");
                eventMngParticipants.PlayerCardClose();
                manageEvent.ClickOnContinueBtnInPods();
                Assert.IsTrue(manageEvent.OpenParticipantCard(Constants.DeckTabName));
                report.logMessage("Participant card in Deck construction right grid opened successfully");
                eventMngParticipants.PlayerCardClose();
            }
            catch (Exception e)
            {
                TestContext.Properties.Add("Error", e.Message);
                throw;
            }

        }
        /// <summary>
        /// Assign Byes in Enrollment Phase Test Case
        /// </summary>
        [TestMethod]
        public void TC525608_AssignByesinEnrollmentPhase()
        {
            try
            {
                report.logMessage("Launched app successfully");
                WLTRCommon objCommon = new WLTRCommon(GetWebDriver());
                objCommon.CreateEvent(Constants.Format, Constants.TeamStructure);
                report.logMessage("Event created successfully");
                EventDetails eventDetails = new EventDetails(GetWebDriver());
                eventDetails.clickContinuetoManageParticipantsBtn();
                EventManageParticipants eventMngParticipants = new EventManageParticipants(GetWebDriver());
                eventMngParticipants.ClickToolsAndSelectOption(Constants.importText);
                eventMngParticipants.ImportEventPlayers(Constants.playersCSV, Constants.validCSVSt);
                report.logMessage("Players imported successfully");
                EventManageParticipants eventManageParticipants = new EventManageParticipants(GetWebDriver());
                int Byes = eventManageParticipants.AssignByesForPlayer();
                eventManageParticipants.SavePlayerDetails();
                Assert.AreEqual(Byes.ToString(), eventManageParticipants.VerifyByesAssigned(Constants.headerNameByes));
                report.logMessage("Byes assiged successfully");
            }
            catch (Exception e)
            {
                TestContext.Properties.Add("Error", e.Message);
                throw;
            }

        }

        [TestMethod]
        //Manage Teams tab - Create a new player Test Case
        public void TC535939_CreatenewplayerManageTeamstab()
        {
            try
            {
                report.logMessage("Launched app successfully");
                WLTRCommon objCommon = new WLTRCommon(GetWebDriver());
                objCommon.CreateEvent(Constants.formatLimited, Constants.TriosTeamStructure);
                report.logMessage("Event created successfully");
                EventDetails eventDetails = new EventDetails(GetWebDriver());
                eventDetails.clickContinuetoManageParticipantsBtn();
                EventManageParticipants eventMngParticipants = new EventManageParticipants(GetWebDriver());
                eventMngParticipants.ClickToolsAndSelectOption(Constants.importText);
                eventMngParticipants.ImportEventPlayers(Constants.teamCSV, Constants.validCSVSt);
                report.logMessage("Players imported successfully");
                eventMngParticipants.ClickOnContinueToManageEvent();
                eventMngParticipants.ClickCreateNewPlayerBtn();
                eventMngParticipants.FillPlayerDetails();
                eventMngParticipants.SavePlayerDetails();
                Assert.IsTrue(eventMngParticipants.VerifyPlayerCreated());
                report.logMessage("New Player created successfully");
                eventDetails.clickEventListLink();
                objCommon.CreateEvent(Constants.formatConstruct, Constants.TriosTeamStructure);
                report.logMessage("Event created successfully");
                eventDetails.clickContinuetoManageParticipantsBtn();
                eventMngParticipants.ClickToolsAndSelectOption(Constants.importText);
                eventMngParticipants.ImportEventPlayers(Constants.teamCSV, Constants.validCSVSt);
                report.logMessage("Players imported successfully");
                eventMngParticipants.ClickOnContinueToManageEvent();
                eventMngParticipants.ClickCreateNewPlayerBtn();
                eventMngParticipants.FillPlayerDetails();
                eventMngParticipants.SavePlayerDetails();
                Assert.IsTrue(eventMngParticipants.VerifyPlayerCreated());
                report.logMessage("New Player created successfully");

            }
            catch (Exception e)
            {
                TestContext.Properties.Add("Error", e.Message);
                throw;
            }

        }

        [TestMethod]
        //Disband a Team event teams Test Case 
        public void TC525653_DisbandaTeamevent()
        {
            try
            {
                report.logMessage("Launched app successfully");
                WLTRCommon objCommon = new WLTRCommon(GetWebDriver());
                objCommon.CreateEvent(Constants.formatLimited, Constants.TriosTeamStructure);
                report.logMessage("Event created successfully");
                EventDetails eventDetails = new EventDetails(GetWebDriver());
                eventDetails.clickContinuetoManageParticipantsBtn();
                EventManageParticipants eventMngParticipants = new EventManageParticipants(GetWebDriver());
                eventMngParticipants.ClickToolsAndSelectOption(Constants.importText);
                eventMngParticipants.ImportEventPlayers(Constants.teamCSV, Constants.validCSVSt);
                report.logMessage("Players imported successfully");
                eventMngParticipants.ClickOnContinueToManageEvent();
                ManageTeams manageteam = new ManageTeams(GetWebDriver());
                Assert.IsTrue(manageteam.CheckTeamDisband());
                report.logMessage("Disband Team successfully");


            }
            catch (Exception e)
            {
                TestContext.Properties.Add("Error", e.Message);
                throw;
            }
        }


        [TestMethod]
        //Team - Create Limited Team event Test Case
        public void TC571851_CreateLimitedTeamEvent()
        {
            try
            {
                report.logMessage("Launched app successfully");
                EventPage eventPage = new EventPage(GetWebDriver());
                Assert.IsTrue(eventPage.CheckWLTRPage(), "WLTR Home page not displayed");
                eventPage.ClickNewEvent();
                report.logMessage("user is on Event Details page");
                EventDetails eventDetails = new EventDetails(GetWebDriver());
                eventDetails.FillEventDetails(Constants.formatLimited, Constants.TriosTeamStructure);
                report.logMessage("App accepted all entries");
                Assert.IsTrue(eventDetails.checkToRangeInRequiredRound(Constants.firstEventRound, Constants.toRange1), " " + Constants.toRange1 + " " + "Round Number is not displayed in To Round Range");
                eventDetails.enterToRangeInRequiredRound(Constants.firstEventRound, Constants.toRange9);
                report.logMessage("Round Range 1 displayed 1 to 9 for 9 rounds in the first round range");
                eventDetails.clickRequiredSeatingCheckBx(Constants.firstEventRound);
                report.logMessage("Seating box is checked");
                eventDetails.ClickReqDeckConstAssign(Constants.firstEventRound);
                report.logMessage("Deck Construction assignment  box is checked");
                eventDetails.ClickReqCopyBrand(Constants.firstEventRound);
                Assert.IsTrue(eventDetails.checkToRangeInRequiredRound(Constants.secondEventRound, Constants.toRange18), " " + Constants.toRange18 + " " + "Round Number is not displayed in To Round Range");
                Assert.AreEqual(eventDetails.GetFormatTypeInRoundRange(Constants.secondEventRound), Constants.formatLimited, true, "Format is not same as above Round Range");
                Assert.IsTrue(eventDetails.CheckDeckConstAssignmentChecked(Constants.secondEventRound), "Deck Construction Assignment is not checked");
                Assert.IsTrue(eventDetails.CheckSeatingBoxChecked(Constants.secondEventRound), "Seating check box is not checked");
                Assert.AreEqual(eventDetails.GetPairingTypeInRoundRange(Constants.secondEventRound), Constants.pairingSwissOpt, true, "Pairing is not same as above Round Range");
                report.logMessage("A second round range is added with same selections in the first round range");
                eventDetails.enterToRangeInRequiredRound(Constants.secondEventRound, Constants.toRange14);
                report.logMessage("App accepts entry");
                eventDetails.clickOnAddRoundRangeBtn();
                Assert.IsTrue(eventDetails.checkToRangeInRequiredRound(Constants.thirdEventRound, Constants.toRange15), " " + Constants.toRange15 + " " + "Round Number is not displayed in To Round Range");
                report.logMessage("Third round range is created 15 to 15");
                eventDetails.clickRequiredSeatingCheckBx(Constants.thirdEventRound);
                report.logMessage("Seating box is checked");
                eventDetails.ClickReqDeckConstAssign(Constants.thirdEventRound);
                report.logMessage("Deck Construction box is checked");
                eventDetails.ClickReqDraftPodAssignment(Constants.thirdEventRound);
                report.logMessage("Draft Pod Assignment box is checked");
                eventDetails.selectFormatInReqEntRange(Constants.thirdEventRound, Constants.formatLimited);
                report.logMessage("Limited format is selected");
                eventDetails.selectPairingInReqEntRange(Constants.thirdEventRound, Constants.singleElimationPlayOffOpt);
                eventDetails.clickRequiredPerformCutCheckBx(Constants.thirdEventRound);
                Assert.IsTrue(eventDetails.checkDoesNotEffectFieldsEnabled(Constants.thirdEventRound), "Does Not Effect Field are not Enabled after clicking on Perform cut before check box");
                report.logMessage("Pairing type is now displaying Single Elimination Playoff and the Perform cut before round and Standing At or Over boxes are enabled");
                eventDetails.enterReqStandAtOrOver(Constants.thirdEventRound, Constants.standAt4);
                report.logMessage("Entries persist. Standing At or Over is 4");
                eventDetails.ClickReqCopyBrand(Constants.thirdEventRound);
                Assert.IsTrue(eventDetails.checkToRangeInRequiredRound(Constants.fourthEventRound, Constants.toRange16), " " + Constants.toRange16 + " " + "Round Number is not displayed in To Round Range");
                Assert.AreEqual(eventDetails.GetFormatTypeInRoundRange(Constants.fourthEventRound), Constants.formatLimited, true, "Format is not same as above Round Range");
                Assert.IsTrue(eventDetails.CheckDraftPodAssignmentsChecked(Constants.fourthEventRound), "Draft Pod Assignment is not checked");
                Assert.IsTrue(eventDetails.CheckDeckConstAssignmentChecked(Constants.fourthEventRound), "Deck Construction Assignment is not checked");
                Assert.IsTrue(eventDetails.CheckSeatingBoxChecked(Constants.fourthEventRound), "Seating check box is not checked");
                Assert.AreEqual(eventDetails.GetPairingTypeInRoundRange(Constants.fourthEventRound), Constants.singleElimationPlayOffOpt, true, "Pairing is not same as above Round Range");
                report.logMessage("Round range 16 to 16 is added with same entries as round 15");
                eventDetails.enterReqStandAtOrOver(Constants.thirdEventRound, Constants.standAt2);
                report.logMessage("The Standing at or Over text box now shows 2");
                eventDetails.clickEventDetailsSaveBtn();
                eventDetails.clickEventListLink();
                Assert.IsTrue(eventPage.CheckWLTRPage(), "WLTR Home page not displayed");
                Assert.IsTrue(eventPage.VerifyEventCreated(EventDetails.eventNameRandom), "Created Event is not displayed in Premium Event list page");
                report.logMessage("The event is successfully saved");
            }
            catch (Exception e)
            {
                TestContext.Properties.Add("Error", e.Message);
                throw;
            }
        }


        [TestMethod]
        //Swap Players in Pod Test Case
        public void TC525549_SwapPlayersPod()
        {
            report.logMessage("Launched app successfully");
            EventPage eventPage = new EventPage(GetWebDriver());
            Assert.IsTrue(eventPage.CheckWLTRPage(), "WLTR Home page not displayed");
            eventPage.ClickNewEvent();
            report.logMessage("user is on Event Details page");
            EventDetails eventDetails = new EventDetails(GetWebDriver());
            eventDetails.FillEventDetails(Constants.BoosterFormat, Constants.TeamStructure);
            report.logMessage("App accepted all entries");
            report.logMessage("Sealed is selected");
            report.logMessage("Booster Draft is selected in format");
            Assert.IsTrue(eventDetails.CheckDraftPodAssignmentsChecked(Constants.secondEventRound), "Draft Pod Assignment is not checked");
            Assert.IsTrue(eventDetails.CheckDeckConstAssignmentChecked(Constants.secondEventRound), "Deck Construction Assignment is not checked");
            report.logMessage("The Draft Pod Assignment and Deck Construction Assignment boxes are automatically checked.");
            eventDetails.clickRequiredSeatingCheckBx(Constants.firstEventRound);
            eventDetails.clickContinuetoManageParticipantsBtn();
            report.logMessage("User is on Manage Participants/Event Participants subtab");
            EventManageParticipants manageparticipant = new EventManageParticipants(GetWebDriver());
            manageparticipant.ClickToolsAndSelectOption(Constants.importText);
            report.logMessage("Import Event Players window opens with Browse Computer for File button");
            manageparticipant.ImportEventPlayers(Constants.teamCSV, Constants.validCSVSt);
            report.logMessage("Fields are populated and Import Status shows Inserted with green beside them and Import Event Players window closes.");
            WLTRCommon objCommon = new WLTRCommon(GetWebDriver());
            Assert.AreEqual(objCommon.CheckLinesCountInCSV(Constants.playersCSV), manageparticipant.GetCountEventParticipants(), "Count in CSV and Enrollment table is not same");
            Assert.IsTrue(manageparticipant.CheckStatusOfPlayersIsEnrolled(), "Players status is not Enrolled");
            report.logMessage("Player has the status as Enrolled");
            manageparticipant.ClickOnContinueToManageEvent();
            ManageEvent mEvent = new ManageEvent(GetWebDriver());
            report.logMessage("User is on Seating Phase");
            mEvent.ClickOnContinueBtnInSeating();
            Assert.AreEqual(mEvent.GetUnUsedTablePlayerCount(), Constants.unUsedCount, true, "UnUsed Table is not Empty");
            report.logMessage("User proceeds to Pods phase. All players are podded in Used column");
            //still In Progress
        }

    }

}

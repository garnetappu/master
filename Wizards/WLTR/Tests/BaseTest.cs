﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading;
using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using Wizards.Automation.Framework.Reports.HTML;
using Wizards.Automation.Framework.Setup;
using Wizards.Automation.Framework.DataAccess;
using WLTR.Common;


namespace WLTR.Tests
{
    [TestClass]
    public class BaseTest
    {
        public static ThreadLocal<IWebDriver> Webdriver=new ThreadLocal<IWebDriver>();       
        public static Data testData;
        public static HtmlReport report;
        public TestContext TestContext { get; set; }

        [TestInitialize]
        public void launchApp()
        {
            
            readTestData();
            report = new HtmlReport();
            report.StartReport(TestContext.TestName, "");
            IWebDriver driver =new DriverFactory().GetWebDriver();
            SetWebDriver(driver);
            for(int i=0; i<driver.WindowHandles.Count;i++)
            {
                driver.SwitchTo().Window(driver.WindowHandles[i]);
                if (driver.Title.Contains("WLTR"))
                {
                    ((IJavaScriptExecutor)driver).ExecuteScript("window.resizeTo(2048, 768);");
                    break;
                }               
            }
            WLTRCommon wLTRCommon = new WLTRCommon(driver);
            wLTRCommon.CheckServerConnection();


        }


        public static IWebDriver GetWebDriver()
        {
            return Webdriver.Value;
        }

        public static void SetWebDriver(IWebDriver driver)
        {
            Webdriver.Value = driver;
        }

        /// <summary>
        /// Method used to read the test data from the excel file
        /// </summary>
        protected virtual void readTestData()
        {

            ExcelReader readExcel = new ExcelReader();
            testData = readExcel.ReadData<Data>(TestContext.TestName);
        }

        [TestCleanup]
        public void cleanUp()
        {
            try
            {
                if (TestContext.Properties.Contains("Error"))
                {
                    report.logStepWeb("Failed", TestContext.Properties["Error"].ToString(), Webdriver.Value);
                }
                else
                {
                    report.logStepWeb("Passed", "", Webdriver.Value);
                }
            }
            catch (Exception e)
            {

            }
            GetWebDriver().Quit();
        }
    }




}

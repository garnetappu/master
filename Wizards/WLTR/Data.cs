﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WLTR
{
    public class Data
    {

        #region TestCase Details
        public string TestName { get; set; }
       
        public string Name { get; set; }

        #endregion

        #region WLTR Home page
        public string UserName { get; set; }
        public string Password { get; set; }


        #endregion

    }
}

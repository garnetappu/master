﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using Wizards.Automation.Framework.Core;
using WLTR.Common;
using Wizards.Automation.Framework.Extensions;

namespace WLTR.PageObjects
{
    public class ManageTeams : WebPageBase
    {
        IWebDriver driver;
        WLTRCommon objCommon;
        public ManageTeams(IWebDriver driver) : base(driver)
        {
            this.driver = driver;
            objCommon = new WLTRCommon(driver);

        }

        #region ManageTeams

        By teamTableDiv = By.CssSelector("div#teamtable div.grid-canvas div[class*='ui-widget']");
        By teamtableHeaders = By.CssSelector("div#teamtable div.slick-header-columns span.slick-column-name");
        By disbandBtn = By.CssSelector("button.btn.btn-default.btn-sm.js-disband-team");
        By addBtn = By.CssSelector("span[data-i18n='players.buttons.add");
        By playerNamesListinTeam = By.CssSelector("div#teamtable div.grid-canvas div[class*='ui-widget'] div.slick-cell.l2.r2");
        By playerNamesList = By.CssSelector("div#enrollment-table-region div.grid-canvas div[class*='ui-widget'] div.slick-cell.l2.r2");
        By playersTableDiv = By.CssSelector("div#enrollment-table-region div.grid-canvas div[class*='ui-widget']");
        

        #endregion

        /// <summary>
        /// Select a player from team table
        /// </summary>
        /// <returns>Players in the selected team</returns>
        public List<string> SelectPlayerFromTeam()
        {
            List<string> Team = new List<string>();
            var PlayersList = FindElementsByDriver(playerNamesListinTeam);
            for (int i = 0; i < 3; i++)
            {
                Team.Add(PlayersList[i].Text);
            }
            CommonActions.Click(driver, FindElementsByDriver(teamTableDiv)[0]);
            return Team;
        }

        /// <summary>
        /// Performs click on diaband button in Manage teams
        /// </summary>
        public void ClickDisbandButton()
        {
            CommonActions.Click(driver, disbandBtn);
        }

        /// <summary>
        /// It selects a player in team table and on disband click it checks whether it moves to player table.
        /// </summary>
        /// <returns></returns>
        public bool CheckTeamDisband()
        {
            List<string> Team = SelectPlayerFromTeam();
            ClickDisbandButton();
            foreach (string player in Team)
            {
               if (!(objCommon.CheckColValNotExistsByHeader(player, teamTableDiv, WLTRCommon.divTag, Constants.playerColumnInTeamTable) && objCommon.CheckColValExists(Constants.playerHeader, player, playersTableDiv, objCommon.tableHeaders, WLTRCommon.divTag)))
                    return false;
              
            }
            return true;
        }

    }





}

﻿using Wizards.Automation.Framework.Core;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using Wizards.Automation.Framework.Extensions;
using WLTR.Tests;
using WLTR.Common;
using Wizards.Automation.Framework;


namespace WLTR.PageObjects
{
    public class EventDetails : WebPageBase
    {
        IWebDriver driver;
        public static readonly Random getrandom = new Random();
        public static readonly object syncLock = new object();
        public static string eventNameRandom;
        public EventDetails(IWebDriver driver) :base(driver)
        {
            this.driver = driver;
        }

        #region Eventdetails
        By banner = By.Id("banner-region");
        By eventName = By.Name("EventName");
        By eventDate = By.Name("StartDate");
        By eventType = By.Name("EventType");
        By teamStructure = By.Name("TeamStructure");
        By countryCode = By.Name("CountryCode");
        By format = By.Name("Format");
        By saveBtn = By.CssSelector("button.btn.btn-primary.js-save");
        By eventsList = By.CssSelector("a.btn-xs.js-action");
        By calendar = By.CssSelector("div.datepicker-days tbody");
        By continueToManageParticipants = By.CssSelector("button.btn.btn-contrast.js-continue-enrollment");
        By seating = By.Name("HasSeating");      


        #endregion

        #region EventDetails_RoundRange
        By roundEndField = By.CssSelector("input[name='RoundEnd']");
        By seatingChBox = By.CssSelector("input[name='HasSeating']");
        By addRoundRangeBtn = By.CssSelector("button.btn.btn-default.js-add-roundrange");
        By performCutChBx = By.CssSelector("input[name='PerformCut']");
        By notAbovePntField = By.CssSelector("input[name='NotAbovePoints']");
        By notAbovePntRadio = By.CssSelector("input.eventFieldCanbeDisabled.eventFieldOnLoadbeDisabled.PerformCutOn");
        By minNoPlayers = By.CssSelector("input.js-minStandingsAbovePoints");
        By minStandPoints = By.CssSelector("input[name='MinStandingsPoints']");
        By standAtOverRadio = By.CssSelector("input[value='Standings']");
        By statndAtOverField = By.CssSelector("input[name='NotAboveStandings']");
        By pairing = By.CssSelector("select[name='PairingType']");
        By pods = By.Name("HasPods");
        By deckConstruction = By.Name("HasDeckConstruction");
        By copyBrand = By.CssSelector("i.fa.fa-copy.js-copy.brand-primary");
        #endregion

        /// <summary>
        /// To Fill the New Event details Form
        /// </summary>
        /// <param name="testData"></param>
        public void FillEventDetails(string formatType, string TeamStructure)
        {
            eventNameRandom = Constants.EventName + GetRandomNumber(Constants.minNumber, Constants.maxNumber);
            CommonActions.Type(driver, eventName, eventNameRandom);
            CommonActions.Click(driver, eventDate);
            Locate(eventDate).SendKeys(DateTime.Today.ToString("yyyy/MM/dd", CultureInfo.InvariantCulture));            
            Actions action = new Actions(driver);
            action.SendKeys(Keys.Escape).Build().Perform();
            CommonActions.SelectByValue(driver, Locate(eventType), Constants.EventType);
            CommonActions.SelectByValue(driver, Locate(teamStructure), TeamStructure);
            CommonActions.SelectByValue(driver, Locate(countryCode), Constants.CountryCode);
            CommonActions.SelectByValue(driver, Locate(format), formatType);
        }
       


        /// <summary>
        /// To click on save button in Event details page
        /// </summary>
        public void clickEventDetailsSaveBtn()
        {
            CommonActions.Click(driver, saveBtn);
        }

        /// <summary>
        /// To click on Event List link in Event Details page
        /// </summary>
        public void clickEventListLink()
        {
            CommonActions.Click(driver, eventsList);
        }

        /// <summary>
        /// To click Add Round Range in WLTR
        /// </summary>

        public void clickAddRoundRange()
        {
            CommonActions.Click(driver, addRoundRangeBtn);
        }



        ///// <summary>
        ///// To click ContinueToManageParticipants button in DEvent Details Page
        ///// </summary>

        //public void clickContinuetoManageParticipantsBtn()
        //{
        //    CommonActions.Click(driver, ManageParticipantBtn);
        //}

        /// <summary>
        /// To generate random number
        /// </summary>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <returns></returns>
        public static int GetRandomNumber(int min, int max)
        {
            lock (syncLock)
            { // synchronize
                return getrandom.Next(min, max);
            }
        }
       

        /// <summary>
        /// To click on Continue to Manage Participants Button in Event details page
        /// </summary>
        public void clickContinuetoManageParticipantsBtn()
        {
            CommonActions.Click(driver, continueToManageParticipants);
        }

        /// <summary>
        /// To Enter the required Range in to Section in required Range table
        /// </summary>
        /// <param name="roundNum"></param>
        public void enterToRangeInRequiredRound(int roundNum, string noOfRounds)
        {
            IList<IWebElement> roundRangeTable = driver.FindElements(roundEndField);
            IWebElement reqRangeTable = roundRangeTable[roundNum];
            CommonActions.Type(driver, reqRangeTable, noOfRounds);
        }

        /// <summary>
        /// To click on Add Round Range button in Event Rounds
        /// </summary>
        public void clickOnAddRoundRangeBtn()
        {
            CommonActions.Click(driver, addRoundRangeBtn);
        }

        /// <summary>
        /// To click on Seating check box in Required Round Range Table
        /// </summary>
        /// <param name="roundNum"></param>
        public void clickRequiredSeatingCheckBx(int roundNum)
        {
            IList<IWebElement> roundRangeTable = driver.FindElements(seatingChBox);
            IWebElement reqSeatingChBx = roundRangeTable[roundNum];
            CommonActions.Click(driver, reqSeatingChBx);
        }

        /// <summary>
        /// To click on Perform Cut check box in Required Round Range Table
        /// </summary>
        /// <param name="roundNum"></param>
        public void clickRequiredPerformCutCheckBx(int roundNum)
        {
            IList<IWebElement> roundRangeTable = driver.FindElements(performCutChBx);
            IWebElement reqPerformCutChBx = roundRangeTable[roundNum];
            CommonActions.Click(driver, reqPerformCutChBx);
        }

        /// <summary>
        /// To enter notAbove Points in Required Round Range Table
        /// </summary>
        /// <param name="roundNum"></param>
        public void enterRequiredNotAbovePoint(int roundNum, string noOfPoints)
        {
            IList<IWebElement> roundRangeTable = driver.FindElements(notAbovePntField);
            IWebElement reqNotAbvPnt = roundRangeTable[roundNum];
            CommonActions.Type(driver, reqNotAbvPnt, noOfPoints);
        }

        /// <summary>
        /// To click on Not on Above Point Radio button in Required Round Range Table
        /// </summary>
        /// <param name="roundNum"></param>
        public void clickRequiredDoesNotEffAbvRadioBtn(int roundNum)
        {
            IList<IWebElement> roundRangeTable = driver.FindElements(notAbovePntRadio);
            IWebElement reqNotAbvPntRadio = roundRangeTable[roundNum];
            CommonActions.Click(driver, reqNotAbvPntRadio);
        }

        /// <summary>
        /// TO Check expected Round Number is present in TO field in Round Range
        /// </summary>
        /// <param name="roundNum"></param>
        /// <param name="expRound"></param>
        /// <returns></returns>
        public bool checkToRangeInRequiredRound(int roundNum, string expRound)
        {
            Sync.VisibilityOfAllElementsLocatedBy(driver, roundEndField);
            IList<IWebElement> roundRangeTable = driver.FindElements(roundEndField);
            IWebElement reqRangeTable = roundRangeTable[roundNum];
            return reqRangeTable.GetAttribute("value").Contains(expRound);
        }

        /// <summary>
        /// 
        /// To Check Perform Cut Before Does Not Effect Region is Enabled or not
        /// </summary>
        /// <param name="roundNum"></param>
        /// <returns></returns>
        public bool checkDoesNotEffectFieldsEnabled(int roundNum)
        {
            IList<IWebElement> roundRangeTable = driver.FindElements(notAbovePntRadio);
            IWebElement reqNotAbvPntRadio = roundRangeTable[roundNum];
            IList<IWebElement> roundRangeTable1 = driver.FindElements(notAbovePntField);
            IWebElement reqNotAbvPnt = roundRangeTable1[roundNum];
            IList<IWebElement> roundRangeTable2 = driver.FindElements(minNoPlayers);
            IWebElement reqMinNoPlayChekBox = roundRangeTable2[roundNum];
            IList<IWebElement> roundRangeTable3 = driver.FindElements(minStandPoints);
            IWebElement reqMinNoStandPoints = roundRangeTable3[roundNum];
            IList<IWebElement> roundRangeTable4 = driver.FindElements(standAtOverRadio);
            IWebElement reqstandAtOverRadio = roundRangeTable4[roundNum];
            bool doesNotEffAboveCheck = (reqNotAbvPntRadio.Enabled && reqNotAbvPnt.Enabled && reqMinNoPlayChekBox.Enabled && reqMinNoStandPoints.Enabled && reqstandAtOverRadio.Enabled);
            CommonActions.Click(driver, reqstandAtOverRadio);
            IList<IWebElement> roundRangeTable5 = driver.FindElements(statndAtOverField);
            IWebElement reqstatndAtOverField = roundRangeTable5[roundNum];
            return (doesNotEffAboveCheck && reqstatndAtOverField.Enabled);
        }

        /// <summary>
        /// To Enter Standing At or Over in Required Round Range
        /// </summary>
        /// <param name="roundNum"></param>
        /// <param name="standOver"></param>
        public void enterReqStandAtOrOver(int roundNum, string standOver)
        {
            IList<IWebElement> roundRangeTable = driver.FindElements(statndAtOverField);
            IWebElement reqstatndAtOverField = roundRangeTable[roundNum];
            CommonActions.Type(driver, reqstatndAtOverField, standOver);
        }

        /// <summary>
        /// To select required format in required event range
        /// </summary>
        /// <param name="roundNum"></param>
        public void selectFormatInReqEntRange(int roundNum, string formatType)
        {
            IList<IWebElement> roundRangeTable = driver.FindElements(format);
            IWebElement reqFormat = roundRangeTable[roundNum];
            CommonActions.SelectByValue(driver, reqFormat, formatType);
        }

        /// <summary>
        /// To select required format in required event range
        /// </summary>
        /// <param name="roundNum"></param>
        public void selectPairingInReqEntRange(int roundNum, string rqOpt)
        {
            IList<IWebElement> roundRangeTable = driver.FindElements(pairing);
            IWebElement reqPairing = roundRangeTable[roundNum];
            CommonActions.SelectByValue(driver, reqPairing, rqOpt);
        }

        /// <summary>
        /// To check Draft pod Assignments check box is selected or not
        /// </summary>
        /// <param name="roundNum"></param>
        /// <returns></returns>
        public bool CheckDraftPodAssignmentsChecked(int roundNum)
        {
            IList<IWebElement> roundRangeTable = driver.FindElements(pods);
            IWebElement reqPods = roundRangeTable[roundNum];
            return reqPods.Selected;
        }

        /// <summary>
        /// To check Deck Construction Assignment check box is selected or not
        /// </summary>
        /// <param name="roundNum"></param>
        /// <returns></returns>
        public bool CheckDeckConstAssignmentChecked(int roundNum)
        {
            IList<IWebElement> roundRangeTable = driver.FindElements(deckConstruction);
            IWebElement reqPods = roundRangeTable[roundNum];
            return reqPods.Selected;
        }

        /// <summary>
        /// 
        /// To click on Stand at or over check box
        /// </summary>
        /// <param name="roundNum"></param>
        /// <returns></returns>
        public void ClickReqStandAtOverRadioInReqEntRange(int roundNum)
        {
            IList<IWebElement> roundRangeTable = driver.FindElements(standAtOverRadio);
            IWebElement reqstandAtOverRadio = roundRangeTable[roundNum];
            CommonActions.Click(driver, reqstandAtOverRadio);
        }


        /// <summary>
        /// To click deck Construction Assignments
        /// </summary>
        /// <param name="roundNum"></param>
        /// <returns></returns>
        public void ClickReqDeckConstAssign(int roundNum)
        {
            IList<IWebElement> roundRangeTable = driver.FindElements(deckConstruction);
            IWebElement reqDeck = roundRangeTable[roundNum];
            CommonActions.Click(driver, reqDeck);
        }

        /// <summary>
        /// To click on required Copy brand icon
        /// </summary>
        /// <param name="roundNum"></param>
        public void ClickReqCopyBrand(int roundNum)
        {
            IList<IWebElement> roundRangeTable = driver.FindElements(copyBrand);
            IWebElement reqCopyBrand = roundRangeTable[roundNum];
            CommonActions.Click(driver, reqCopyBrand);
        }

        /// <summary>
        /// To get the value which is selected in Format drop down
        /// </summary>
        /// <param name="roundNum"></param>
        /// <returns></returns>
        public string GetFormatTypeInRoundRange(int roundNum)
        {
            IList<IWebElement> roundRangeTable = driver.FindElements(format);
            IWebElement reqFormat = roundRangeTable[roundNum];
            return reqFormat.GetAttribute("value");
        }

        /// <summary>
        /// To check required seating check box is selected or not
        /// </summary>
        /// <param name="roundNum"></param>
        /// <returns></returns>
        public bool CheckSeatingBoxChecked(int roundNum)
        {
            IList<IWebElement> roundRangeTable = driver.FindElements(seatingChBox);
            IWebElement reqSeatingChBox = roundRangeTable[roundNum];
            return reqSeatingChBox.Selected;
        }

        /// <summary>
        /// To get the value which is selected in Pairing dropdown
        /// </summary>
        /// <param name="roundNum"></param>
        /// <returns></returns>
        public string GetPairingTypeInRoundRange(int roundNum)
        {
            IList<IWebElement> roundRangeTable = driver.FindElements(pairing);
            IWebElement reqPairing = roundRangeTable[roundNum];
            return reqPairing.GetAttribute("value");
        }

        /// <summary>
        /// To click on required Draft Pod Assignments
        /// </summary>
        /// <param name="roundNum"></param>
        public void ClickReqDraftPodAssignment(int roundNum)
        {
            IList<IWebElement> roundRangeTable = driver.FindElements(pods);
            IWebElement reqPods = roundRangeTable[roundNum];
            CommonActions.Click(driver, reqPods);
        }

    }
}

﻿using System.Collections.Generic;
using System.Configuration;
using System.Reflection;
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using Wizards.Automation.Framework.Extensions;
using Wizards.Automation.Framework.Core;
using WLTR.Common;
using Wizards.Automation.Framework;
using System.Threading;



namespace WLTR.PageObjects
{
    public class EventManageParticipants : WebPageBase
    {
        IWebDriver driver;
        WLTRCommon objCommon;
        public static string playerfirstName;
        public static string playerLastName;
        public EventManageParticipants(IWebDriver driver) :base(driver)
        {
            this.driver = driver;
            objCommon = new WLTRCommon(driver);
        }



        #region CreateNewPlayer
        By createPlayerBtn = By.CssSelector("span[data-i18n='players.buttons.create']");
        By newPlayerPopUp = By.Id("person-main-region");
        By firstNametxt = By.CssSelector("div#person-main-region input[name='FirstName']");
        By lastNametxt = By.CssSelector("div#person-main-region input[name='LastName']");
        By dciTxt = By.Id("dciTxt");
        By validateDciBtn = By.CssSelector("button.btn.btn-default.form-control.text-small.js-dci-validate");
        By country = By.Name("Country");
        By roleDrpdwn = By.Id("roleOpt");
        By saveAndCloseBtn = By.CssSelector("button[data-i18n='create-player.buttons.save-close']");
       // By playersListTableDiv = By.CssSelector("div#enrollment-table-region div.grid-canvas");
        By playersTableColDiv = By.CssSelector("div#enrollment-table-region div.slick-header-columns span[class*='slick-column-name']");
        By tableHeaders = By.CssSelector("div#enrollment-table-region div.slick-header-columns span.slick-column-name");
        By playersColNames = By.CssSelector("div[id*='slickgrid']");
        By playersColValue = By.CssSelector("div[class*='slick-cell']");
       // By tableRows = By.CssSelector("div[class*='ui-widget']");
        By playercardCloseBtn = By.CssSelector("div#person-main-region button.close.modal-header-button-close");
        By byesInput = By.Name("Byes");
        By eventParticipantsCount = By.CssSelector("a[data-source='event'] div.number-spot");
        #endregion

        #region Penalities
        By penaltyTabBtn = By.CssSelector("a[data-i18n='create-player.labels.penalties']");
        By newBtn = By.CssSelector("button[data-i18n='create-player.buttons.new']");
        By removeBtn = By.CssSelector("button[data-i18n='create-player.buttons.remove']");
        By roundOneSelect = By.Name("RoundNumber");
        By judgeSelect = By.Name("JudgeName");
        By infractionSelect = By.Name("Infraction");
        By penaltiesSelect = By.Name("Penalties");
        By savePenalityBtn = By.CssSelector("button[data-i18n='events.buttons.save-penalty']");
        By penalitiesTableDiv = By.CssSelector("div#modal-penalties div.scroller");
        By penalitiesTableRow = By.CssSelector("div#modal-penalties table.table.table-fixed.scrollpart");


        #endregion

        #region importplayerslist
        By toolsdropdown = By.CssSelector("ul#header-nav li.dropdown");
        By importLink = By.CssSelector("a.js-import");
        By importPopUp = By.CssSelector("div#modal");
        By fileNameInput = By.CssSelector("input[name='file']");
        By importBtn = By.CssSelector("button.btn.btn-contrast.large-btn.js-import");
        By closeBtn = By.CssSelector("button.btn.btn-dark-gray.large-btn");
        By continueToManageEventBtn = By.CssSelector("button.btn.btn-contrast.js-continue");
        By toolsDropDownMenu = By.CssSelector("ul.dropdown-menu");
        By csvDataBind = By.CssSelector("span[data-bind='text:filename']");
        By tablePlayersList = By.CssSelector("table.table.scrollpart");
        By tableHeadersImportPopUp = By.CssSelector("table.table th");
        By playersListImportPopUp = By.CssSelector("table.table.scrollpart");
        #endregion

        #region ErrorPopUpInImport
        By popUpErrTxt = By.CssSelector("div.bootbox-body");
        By okBtn = By.CssSelector("button[data-bb-handler='ok']");
        #endregion

        #region ManageEvent
        By continuemanagevent = By.CssSelector("");
        #endregion

        #region SearchResult
        By searchresultTabBtn = By.CssSelector("");
        By searchWizardsCheckBox = By.CssSelector("");
        #endregion

        /// <summary>
        /// To click and select a option from Tools menu
        /// </summary>

        public void ClickToolsAndSelectOption(string option)
        {
            objCommon.Spinner();
            Sync.VisibilityOfAllElementsLocatedBy(driver, toolsdropdown);
            CommonActions.Click(driver, toolsdropdown);
            IList<IWebElement> toolsMenu = FindElements(toolsDropDownMenu, WLTRCommon.liTag);
            foreach(IWebElement menuEle in toolsMenu)
            {
                if(menuEle.Displayed)
                {
                    string eleText = menuEle.Text;
                    if(eleText.Contains(option))
                    {
                        menuEle.Click();
                        return;
                    }
                }
            }


        }



        /// <summary>
        /// To import csv and close pop up
        /// </summary>

        public void ImportEventPlayers(string fileName, string validity)
        {
            string filepath = WLTRCommon.directory + ConfigurationManager.AppSettings["TestDataFolder"] + "\\" + fileName;
            Locate(fileNameInput).SendKeys(filepath);
            Thread.Sleep(10000);
            Sync.VisibilityOfAllElementsLocatedBy(driver, tablePlayersList);
            Sync.ElementIsVisible(driver, csvDataBind);
            CommonActions.Click(driver, importBtn);
            Sync.VisibilityOfAllElementsLocatedBy(driver, tablePlayersList);
            objCommon.Spinner();
            if (validity == "ValidCSV")
            {
                CommonActions.Click(driver, closeBtn);
            }
        }

        /// <summary>
        /// To click on Continue To Manage Event
        /// </summary>
        public void ClickOnContinueToManageEvent()
        {
            objCommon.Spinner();
            Sync.ElementToBeClickable(driver, continueToManageEventBtn);
            CommonActions.Click(driver, continueToManageEventBtn);
            Thread.Sleep(5000);
        }
        /// <summary>
        /// To click create New Player button
        /// </summary>

        public void ClickCreateNewPlayerBtn()
        {
            CommonActions.Click(driver, createPlayerBtn);
        }

        public void FillPlayerDetails()
        {
            int random = EventDetails.GetRandomNumber(Constants.minNumber, Constants.maxNumber);
            playerfirstName = Constants.playerFirstName + random;
            playerLastName = Constants.playerLastName + random;
            CommonActions.Type(driver, firstNametxt, playerfirstName);
            CommonActions.Type(driver, lastNametxt, playerLastName);
            CommonActions.Type(driver, dciTxt, random.ToString());
            CommonActions.SelectByValue(driver, Locate(roleDrpdwn), Constants.playerRole);
            CommonActions.SelectByValue(driver, Locate(country), Constants.CountryCode);
        }

        public void SavePlayerDetails()
        {
            CommonActions.Click(driver, saveAndCloseBtn);
        }

        /// <summary>
        /// To verify player is created
        /// </summary>
        /// <returns></returns>
        public bool VerifyPlayerCreated()
        {
          bool Created=  objCommon.CheckCol(playerfirstName +" "+ playerLastName,Constants.playerStatus,objCommon.playersListTableDiv, objCommon.tableRows);
          return Created;
        }

        public bool VerifyPlayerCardOpen()
        {
            return Locate(newPlayerPopUp).Displayed;
        }



        public void ClickContinuetoManageEvent()
        {
            CommonActions.Click(driver, continueToManageEventBtn);
        }

        public void ClickPenaltiesTab()
        {
            CommonActions.Click(driver, penaltyTabBtn);
        }

        public bool VerifyPenaltiesTab()
        {
            return (Locate(newBtn).Displayed && Locate(removeBtn).Displayed && Locate(roundOneSelect).Displayed && Locate(judgeSelect).Displayed && Locate(infractionSelect).Displayed && Locate(penaltiesSelect).Displayed);
        
        }
        public void ClickpenaltyNewBtn()
        {
            CommonActions.Click(driver, newBtn);
        }

        public void ClickSearchResultTab()
        {
            CommonActions.Click(driver, searchresultTabBtn);
        }

        public bool VerifySearchResultTab()
        {
            //return (Locate(newBtn).Displayed && Locate(removeBtn).Displayed && Locate(roundOneSelect).Displayed && Locate(judgeSelect).Displayed && Locate(infractionSelect).Displayed && Locate(penaltiesSelect).Displayed);
            return (Locate(searchWizardsCheckBox).Displayed);
        }

        /// <summary>
        /// To get the total players count of Event Participants table
        /// </summary>
        /// <returns></returns>
        public int GetCountEventParticipants()
        {
            return Int32.Parse(Locate(eventParticipantsCount).Text);
        }



        public void FillPenaltyDetails()
        {
            CommonActions.SelectByIndex(driver, Locate(roundOneSelect), 1);
            CommonActions.SelectByIndex(driver, Locate(judgeSelect), 1);
            CommonActions.SelectByIndex(driver, Locate(infractionSelect), 1);
            CommonActions.SelectByIndex(driver, Locate(penaltiesSelect), 1);
            
        }

        public void SavePenality()
        {
            CommonActions.Click(driver, savePenalityBtn);
        }

        public bool VerifyPenalityDetails()
        {
            var penalties = Locate(penalitiesTableDiv).FindElements(penalitiesTableRow);
            if (penalties.Count > 0) return true;
            else return false;
        }

        /// <summary>
        /// To check Error message text in pop up when Invalid csv file is uploaded
        /// </summary>
        /// <returns></returns>
        public bool CheckPopUpErrTxt(string reqErrMsg)
        {
            Sync.VisibilityOfAllElementsLocatedBy(driver, popUpErrTxt);
            return Locate(popUpErrTxt).Text.Contains(reqErrMsg);
        }

        /// <summary>
        /// To click on Ok button in Error Pop Up when invalid csv is uploaded
        /// </summary>
        public void ClickOkBtnInErrPopUp()
        {
            CommonActions.Click(driver, okBtn);
        }


        /// <summary>
        /// To check table row is removed from left side
        /// </summary>
        /// <param name="headerName"></param>
        /// <returns></returns>
        public bool CheckImportStatusErrMsgInColumns()
        {
            return objCommon.CheckColValExistsByHeader(Constants.headerImportStatus, Constants.importStatusErrMsg, playersListImportPopUp, WLTRCommon.trTag, tableHeadersImportPopUp, WLTRCommon.tdTag);
        }

        /// <summary>
        /// Closing the participant card
        /// </summary>
        public void PlayerCardClose()
        {
            CommonActions.Click(driver, playercardCloseBtn);
        }


       /// <summary>
        /// To check players status is enrolled in the players list table in event players list
        /// </summary>
        /// <returns></returns>
        public bool CheckStatusOfPlayersIsEnrolled()
        {
            return objCommon.CheckColValExistsByHeader(Constants.playerStatusHeader, Constants.playerStatus,objCommon.playersListTableDiv, objCommon.tableRows, tableHeaders, WLTRCommon.divTag);
        }

        /// <summary>
        /// Assigning byes for a player in playercard
        /// </summary>
        /// <returns></returns>
        public int AssignByesForPlayer()
        {
            int randomByesNum = EventDetails.GetRandomNumber(Constants.minNumber, Constants.maxNumber);
            var colValueList = Locate(objCommon.playersListTableDiv).FindElements(playersColValue);
            colValueList[0].Click();
            CommonActions.ContextClick(driver, colValueList[0]);
            CommonActions.Type(driver, byesInput, randomByesNum.ToString());
            return randomByesNum;
        }

        /// <summary>
        /// Verify byes is saved for a player
        /// </summary>
        /// <returns></returns>
        public string VerifyByesAssigned(string headerName)
        {
            IList<IWebElement> PlayerColumns = Locate(objCommon.playersListTableDiv).FindElements(objCommon.tableRows)[0].FindElements(playersColValue);
            return PlayerColumns[objCommon.GetColNumByHeader(headerName, playersTableColDiv)].Text;
            
        }
    }
}

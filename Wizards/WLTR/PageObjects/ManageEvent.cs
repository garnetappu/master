﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using System.Collections.ObjectModel;
using OpenQA.Selenium.Support.PageObjects;
using Wizards.Automation.Framework;
using Wizards.Automation.Framework.Core;
using Wizards.Automation.Framework.Extensions;
using WLTR.Common;

namespace WLTR.PageObjects
{
    public class ManageEvent : WebPageBase
    {
        IWebDriver driver;
        WLTRCommon objCommon;
        public static string columnVal;
        
        public ManageEvent(IWebDriver driver) : base(driver)
        {
            this.driver = driver;
            objCommon = new WLTRCommon(driver);
        }

        #region ManageEvent
        By continueBtn = By.CssSelector("button.btn.btn-contrast.js-continue");
        By manageEventPage = By.Id("main-region");
        By seatingRightTable = By.Id("seating-right");
        By seatingLeftTable = By.Id("seating-left");
        By seatWithByeBtn = By.CssSelector("button.btn.btn-violet.btn-sm.btn-follow.js-seat-late");
        By seatWithLossBtn = By.CssSelector("button.btn.btn-violet.btn-sm.js-seat-late");

        #endregion

        #region Match tables
        By matchtablesLink = By.CssSelector("span[data-i18n='rounds.breadcrumbs.matchTables']");
        By unusedTableDiv = By.CssSelector("div#seating-left div.scroller");
        By usedTableDiv = By.CssSelector("div#seating-right div.scroller");
        By tableHeaders = By.CssSelector("div#results-left div.slick-header-columns span.slick-column-name");
        By tableHeadersRight = By.CssSelector("div#results-right div.slick-header-columns");
        By columns = By.CssSelector("span.slick-column-name");
        By reportedTableColumns = By.CssSelector("div#results-right div.slick-header-columns span.slick-column-name");
        By leftTableGrid = By.CssSelector("div#results-left div.grid-canvas div[class*='ui-widget']");
        By rightTableGrid = By.CssSelector("div#results-right div.grid-canvas div[class*='ui-widget']");
        By usedTableRows = By.CssSelector("div#seating-right table.scrollpart.table.table-responsive.table-striped.table-fixed.data-table tr");
        By usedTableHeaders = By.CssSelector("div#seating-right table.table.table-responsive.table-striped.table-used-bold.table-fixed.data-table th");
        By unusedTableRows = By.CssSelector("div#seating-left table.table.table-fixed.scrollpart tr");
        By unusedTableHeaders = By.CssSelector("div#seating-left table.table.table-fixed th");
        #endregion

        #region Results
        By resultsLink = By.CssSelector("span[data-i18n='rounds.breadcrumbs.results']");
        By mainRegion = By.CssSelector("div#main-region");
        By notReportedTableDiv = By.CssSelector("div#results-left div.grid-canvas");
        By playerNameMainTab = By.Id("person-tab-region");
        By PlayerNamesTab = By.CssSelector("a[href='#modal-event-new']");
        By notreportedTableRows = By.CssSelector("div[class*=ui-widget-content]");
        By reportedTableDiv = By.CssSelector("div#results-right div.grid-canvas");
        By scorelockRadio = By.CssSelector("input[value='scorelock']");
        By hotkeyRadio = By.CssSelector("input[value='hotkey']");
        By scoreText1 = By.CssSelector("input[name='Result1']");
        By scoreText2 = By.CssSelector("input[name='Result2']");
        By scoreText3 = By.CssSelector("input[name='Draw']");
        By tableNumTxt = By.CssSelector("input[name='searchTable']");
        By resultsTable = By.CssSelector("table.table.table-results tr");
        #endregion

        #region Seating
        By seatingLink = By.CssSelector("span[data-i18n='rounds.breadcrumbs.seating']");
        By unSeatP1Btn = By.CssSelector("span[data-i18n='seating.buttons.unseatP1']");
        By unSeatP2Btn = By.CssSelector("span[data-i18n='seating.buttons.unseatP2']");
        By seatingRightTabDiv = By.CssSelector("div#seating-right div.scroller table[class*='scrollpart']");
        By seatingLeftTabDiv = By.CssSelector("div#results-left div.scroller table[class*='scrollpart']");
        #endregion

        #region Pods
        By podsLink = By.CssSelector("span[data-i18n='rounds.breadcrumbs.pods']");
        By unusedPodsTableDiv = By.CssSelector("div#pods-left div.scroller table[class*='pods-table']");
        By usedPodsTableDiv = By.CssSelector("div#pods-right div.scroller table[class*='pods-table']");
        By unUsedTablePlayerCount = By.CssSelector("span[data-bind='text:unused']");
        #endregion

        #region DeckConstruction
        By deckLink = By.CssSelector("span[data-i18n=' rounds.breadcrumbs.deckConstruction']");
        By unusedDeckTableDiv = By.CssSelector("div#deck_construct-left div.scroller table[class*='scrollpart']");
        By usedDeckTableDiv = By.CssSelector("div#deck_construct-right div.scroller table[class*='scrollpart']");
        #endregion

        /// <summary>
        /// To click on Score Lock Method Radio Button
        /// </summary>
        public void ClickScoreLockRadio()
        {
            Sync.VisibilityOfAllElementsLocatedBy(driver, scorelockRadio);
            CommonActions.Click(driver, scorelockRadio);
        }

        /// <summary>
        /// To check Score check box is disabled
        /// </summary>
        /// <returns></returns>
        public bool CheckScoreCheckBoxGrayedOut()
        {
            return (Locate(scoreText1).Enabled && Locate(scoreText2).Enabled && Locate(scoreText3).Enabled);
        }





        /// <summary>
        /// To enter Table number in Table# field
        /// </summary>
        /// <param name="tableNum"></param>
        public void EnterValInSearchTable(string tableNum)
        {
            CommonActions.Type(driver, tableNumTxt, tableNum);
        }

        public void ClickOnRequiredScoreButton(string scoreButtonNum)
        {
            IList<IWebElement> resTable = FindElementsByDriver(resultsTable);
            IWebElement keys = resTable[3];
            IList<IWebElement> keysList = FindElements(keys, WLTRCommon.tdTag);
            foreach (IWebElement ele in keysList)
            {
                if (ele.Displayed && ele.Text.Contains(scoreButtonNum))
                {
                    ele.Click();
                    return;
                }
            }
        }

        /// <summary>
        /// To press enter in Table# field
        /// </summary>
        public void ClickAndEnterOnSearchTable()
        {
            CommonActions.ClickEnter(driver, Locate(tableNumTxt));
        }

        /// <summary>
        /// TO click on Continue button in seating tables tab
        /// </summary>
        public void ClickOnContinueBtnInSeating()
        {
            objCommon.Spinner();
            CommonActions.Click(driver, continueBtn);
        }

        /// <summary>
        /// TO click on Continue button in Match tables tab
        /// </summary>
        public void ClickOnContinueBtnInMatching()
        {
            objCommon.Spinner();
            CommonActions.Click(driver, continueBtn);
        }

        /// <summary>
        /// To check Player is removed from Not reported Table in Results tab
        /// </summary>
        /// <param name="dataKey"></param>
        /// <returns></returns>
        public bool CheckPlayerIsRemovedFromNotReportedTab(string dataKey)
        {
            return objCommon.CheckColValNotExistsByHeader(Constants.headerNameTb, dataKey, leftTableGrid, tableHeaders, WLTRCommon.divTag);
        }

        /// <summary>
        /// To check player is added to right table Reported one
        /// </summary>
        /// <param name="dataKey"></param>
        /// <returns></returns>
        public bool CheckPlayerIsAddedToReportedTab(string dataKey)
        {
            return objCommon.CheckColValExists(Constants.headerNameTb, dataKey, rightTableGrid, tableHeaders, WLTRCommon.divTag);
        }



        /// <summary>
        /// To check score by required keys in results tab
        /// </summary>
        /// <param name="headerName"></param>
        /// <returns></returns>
        public bool CheckScoreAssignedToPlayersByKeys(string headerName, string score)
        {
            return objCommon.CheckColValExists(headerName, score, rightTableGrid, reportedTableColumns, WLTRCommon.divTag);
        }


        /// <summary>
        /// Verify player card from results page
        /// </summary>
        /// <returns></returns>
        public bool VerifyplayerCard()
        {
            var playerslist = Locate(notReportedTableDiv).FindElements(notreportedTableRows);
            var playerNames = Locate(playerNameMainTab).FindElements(PlayerNamesTab);
            if (playerslist[0].Text.Contains(playerNames[0].Text)) return true;
            else
                return false;
        }





        /// <summary>
        /// TO Move the player from table one to Reported table with required hotkey
        /// </summary>
        /// <param name="headerName"></param>
        /// <param name="hotKey"></param>
        public void MoveReqPlayerToReportedTable(string headerName, string hotKey)
        {
            columnVal = objCommon.GetColValByHeader(headerName, leftTableGrid, tableHeaders, WLTRCommon.divTag);
            ClickOnRequiredScoreButton(hotKey);
            EnterValInSearchTable(columnVal);
            ClickAndEnterOnSearchTable();
        }

        /// <summary>
        /// To check the values of hot keys from 1-8 in reported table
        /// </summary>
        /// <returns></returns>
        public bool CheckScoresForAllNumericHotKeys()
        {
            string datakey = null;
            string dataScore = null;
            bool checkHotKeys = false;
            for (int i = 1; i <= 8; i++)
            {
                switch (i)
                {
                    case 1:
                        datakey = Constants.datakey1;
                        dataScore = Constants.datakey1Score;
                        break;
                    case 2:
                        datakey = Constants.datakey2;
                        dataScore = Constants.datakey2Score;
                        break;
                    case 3:
                        datakey = Constants.datakey3;
                        dataScore = Constants.datakey3Score;
                        break;
                    case 4:
                        datakey = Constants.datakey4;
                        dataScore = Constants.datakey4Score;
                        break;
                    case 5:
                        datakey = Constants.datakey5;
                        dataScore = Constants.datakey5Score;
                        break;
                    case 6:
                        datakey = Constants.datakey6;
                        dataScore = Constants.datakey6Score;
                        break;
                    case 7:
                        datakey = Constants.datakey7;
                        dataScore = Constants.datakey7Score;
                        break;
                    case 8:
                        datakey = Constants.datakey8;
                        dataScore = Constants.datakey8Score;
                        break;
                }

                MoveReqPlayerToReportedTableByHotKey(Constants.headerNameTb, datakey);

                if (CheckPlayerIsRemovedFromNotReportedTab(columnVal) && CheckPlayerIsAddedToReportedTab(columnVal) && CheckScoreAssignedToPlayersByKeys(Constants.headerNameScore, dataScore))
                {
                    checkHotKeys = true;
                }
                else
                {
                    return false;
                }

            }

            return checkHotKeys;
        }

        /// <summary>
        /// To check the values of hot keys from l-i in reported table
        /// </summary>
        /// <returns></returns>
        public bool CheckScoresForAlphabetHotKeys()
        {
            string datakey = null;
            string dataScore = null;
            bool checkHotKeys = false;
            for (int i = 1; i <= 2; i++)
            {
                switch (i)
                {
                    case 1:
                        datakey = Constants.datakeyl;
                        dataScore = Constants.datakeyLScore;
                        break;
                    case 2:
                        datakey = Constants.datakeyi;
                        dataScore = Constants.datakeyIScore;
                        break;

                }

                MoveReqPlayerToReportedTableByHotKey(Constants.headerNameTb, datakey);

                if (CheckPlayerIsRemovedFromNotReportedTab(columnVal) && CheckPlayerIsAddedToReportedTab(columnVal) && CheckScoreAssignedToPlayersByKeys(Constants.headerNameScore, dataScore))
                {
                    checkHotKeys = true;
                }
                else
                {
                    return false;
                }

            }

            return checkHotKeys;

        }

        /// <summary>
        ///  To check the values of all hot keys in reported table
        /// </summary>
        /// <returns></returns>
        public bool CheckScoresForAllHotKeys()
        {
            return (CheckScoresForAlphabetHotKeys() & CheckScoresForAllNumericHotKeys());
        }


        /// <summary>
        /// Open participants card from all pages
        /// </summary>
        /// <param name="Phase"></param>
        /// <returns></returns>
        public bool OpenParticipantCard(string Phase)
        {

            ReadOnlyCollection<IWebElement> playerslist = null;
            Thread.Sleep(2000);
            Sync.InvisibilityOfElementLocated(driver, WLTRCommon.spinner);
            switch (Phase)
            {

                case "Seating":
                case "MatchTables":

                    Sync.VisibilityOfAllElementsLocatedBy(driver, seatingRightTabDiv);
                    playerslist = Locate(seatingRightTabDiv).FindElements(WLTRCommon.trTag); break;
                case "ResultsLeft":

                    Sync.VisibilityOfAllElementsLocatedBy(driver, notReportedTableDiv);
                    playerslist = Locate(notReportedTableDiv).FindElements(notreportedTableRows); break;
                case "ResultsRight":
                    Sync.VisibilityOfAllElementsLocatedBy(driver, reportedTableDiv);
                    playerslist = Locate(reportedTableDiv).FindElements(notreportedTableRows); break;
                case "Deck":

                    Sync.VisibilityOfAllElementsLocatedBy(driver, usedDeckTableDiv);
                    playerslist = Locate(usedDeckTableDiv).FindElements(WLTRCommon.trTag); break;
                case "Pods":

                    Sync.VisibilityOfAllElementsLocatedBy(driver, usedPodsTableDiv);
                    playerslist = Locate(usedPodsTableDiv).FindElements(WLTRCommon.trTag); break;


            }


            playerslist[0].Click();
            CommonActions.ContextClick(driver, playerslist[0]);
            var playerNames = Locate(playerNameMainTab).FindElements(PlayerNamesTab);
            bool Match = true;
            foreach (IWebElement player in playerNames)
            {
                if (playerslist[0].Text.Contains(player.Text)) Match = true;
                else
                    return false;

            }
            return Match;


        }

        /// <summary>
        /// Giving score in results page by scorelock method
        /// </summary>
        /// <param name="headerName"></param>
        public void EnterAllResultsbyScorelockMethod(string headerName)
        {
            //string headerValue = null;
            IList<IWebElement> tabHeaders = FindElementsByDriver(leftTableGrid);

            foreach (IWebElement ele in tabHeaders)
            {
                IList<IWebElement> colVals = FindElements(ele, WLTRCommon.divTag);
                EnterValInSearchTable(colVals[objCommon.GetColNumByHeader(headerName, tableHeaders)].Text);
                ClickAndEnterOnSearchTable();
                CheckPlayerIsRemovedFromNotReportedTab(colVals[objCommon.GetColNumByHeader(headerName, tableHeaders)].Text);
                CheckPlayerIsAddedToReportedTab(colVals[objCommon.GetColNumByHeader(headerName, tableHeaders)].Text);

            }


        }

        /// <summary>
        /// Click continue in Pods page
        /// </summary>

        public void ClickOnContinueBtnInPods()
        {
            objCommon.Spinner();
            CommonActions.Click(driver, continueBtn);

        }


        /// <summary>
        /// To click on Hot Key Method Radio Button
        /// </summary>
        public void ClickHotKeyRadio()
        {
            Sync.VisibilityOfAllElementsLocatedBy(driver, hotkeyRadio);
            CommonActions.Click(driver, hotkeyRadio);
        }



        /// <summary>
        /// TO Move the player from table one to Reported table with required hotkey
        /// </summary>
        /// <param name="headerName"></param>
        /// <param name="hotKey"></param>
        public void MoveReqPlayerToReportedTableByHotKey(string headerName, string hotKey)
        {
            columnVal = objCommon.GetColValByHeader(headerName, leftTableGrid, tableHeaders, WLTRCommon.divTag);
            objCommon.ClickOnRequiredRow(headerName, columnVal, leftTableGrid, tableHeaders, WLTRCommon.divTag);
            ClickOnRequiredScoreButton(hotKey);
        }


        
        /// <summary>
        /// To click on unseat P1 button
        /// </summary>
        public void ClickOnUnSeatP1Btn()
        {
            CommonActions.Click(driver, unSeatP1Btn);
        }

        /// <summary>
        /// To click on unseat P2 button
        /// </summary>
        public void ClickOnUnSeatP2Btn()
        {
            CommonActions.Click(driver, unSeatP2Btn);
        }

        /// <summary>
        /// To check required player is unseated from used table
        /// </summary>
        /// <param name="reqCol"></param>
        /// <param name="playerName"></param>
        /// <returns></returns>
        public bool CheckPlayerIsRemovedFromUsedTable(string playerName, int rowNum)
        {
            return objCommon.GetRequiredRow(usedTableRows, rowNum).Text.Contains(playerName);
        }

        /// <summary>
        /// To check required player is added to un used table
        /// </summary>
        /// <param name="reqCol"></param>
        /// <param name="playerName"></param>
        /// <returns></returns>
        public bool CheckPlayerIsAddedToUnUsedTable(string headerPlayer, string playerName)
        {
            return objCommon.CheckColValExists(headerPlayer, playerName, unusedTableRows, unusedTableHeaders, WLTRCommon.tdTag);
        }

        /// <summary>
        /// To check Unseating of Players in Seating tab
        /// </summary>
        /// <param name="headerPlayer1"></param>
        /// <param name="headerPlayer2"></param>
        /// <param name="headerPlayer"></param>
        /// <returns></returns>
        public bool CheckUnseatingOfPlayers(string headerPlayer1, string headerPlayer2, string headerPlayer)
        {
            IList<IWebElement> playersRows = FindElementsByDriver(usedTableRows);
            int colNumPlayer1 = objCommon.GetColNumByHeader(headerPlayer1, usedTableHeaders);
            int colNumPlayer2 = objCommon.GetColNumByHeader(headerPlayer2, usedTableHeaders);
            bool checkUnseat = false;
            for (int i=0; i<=2; i++)
            {
                CommonActions.Click(driver,objCommon.GetRequiredRow(usedTableRows, i));
                string playerName1 = objCommon.GetRequiredCell(usedTableRows, i, colNumPlayer1, WLTRCommon.tdTag).Text;
                ClickOnUnSeatP1Btn();
                if(!(CheckPlayerIsRemovedFromUsedTable(playerName1, i)) && (CheckPlayerIsAddedToUnUsedTable(headerPlayer, playerName1)))
                {
                    string playerName2 = objCommon.GetRequiredCell(usedTableRows, i, colNumPlayer2, WLTRCommon.tdTag).Text;
                    ClickOnUnSeatP2Btn();
                    if (!(CheckPlayerIsRemovedFromUsedTable(playerName2, i)) && (CheckPlayerIsAddedToUnUsedTable(headerPlayer, playerName2)))
                    {
                        checkUnseat = true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
              
            }

            return checkUnseat;
        }

        /// <summary>
        /// To get the UnUsed table players count
        /// </summary>
        /// <returns></returns>
        public string GetUnUsedTablePlayerCount()
        {
            return Locate(unUsedTablePlayerCount).Text;
        }

    }
}


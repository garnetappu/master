﻿using OpenQA.Selenium;
using System.Linq;
using OpenQA.Selenium.Support.PageObjects;
using Wizards.Automation.Framework.Core;
using Wizards.Automation.Framework;
using Wizards.Automation.Framework.Extensions;
using WLTR.Common;

namespace WLTR.PageObjects
{
    public class EventPage : WebPageBase
    {
        IWebDriver driver;
        
        public EventPage(IWebDriver driver):base(driver)
        {
            this.driver =driver;
        }

        #region EventPage
        By Banner = By.Id("banner-region");
        By createNewEventBtn = By.CssSelector("span[data-i18n='events.buttons.create']");
        By fromrange = By.XPath("//span[@class='number']");
        By torange = By.Name("RoundEnd");
        By premierEventsList = By.Id("test");
        By notConnectedLink = By.CssSelector("li.dropdown i.fa.fa-signal");
        By connect = By.CssSelector("ul.nav.navbar-nav.navbar-right ul.dropdown-menu a.js-connect");
        By userNamefield = By.Id("login-username");
        By passwordfield = By.Id("login-password");
        By loginBtn = By.CssSelector("button.btn.btn-primary.js-login");
        By connected = By.CssSelector("");
        #endregion


        /// <summary>
        /// Method to check whether WLTR home page is displayed or not
        /// </summary>
        /// <param name="bannerTitle"></param>
        /// <returns></returns>
        public bool CheckWLTRPage()
          {
            Sync.VisibilityOfAllElementsLocatedBy(driver, createNewEventBtn);
            return Locate(createNewEventBtn).Displayed;

           }


        /// <summary>
        /// To click on New Event Button in WLTR
        /// </summary>

        public void ClickNewEvent()
        {
            CommonActions.Click(driver,createNewEventBtn);
        }


        /// <summary>
        /// To Login to WLTR
        /// </summary>

        public void Login(string username,string password)
        {
            CommonActions.Type(driver, userNamefield, username);
            CommonActions.Type(driver, passwordfield, password);
            CommonActions.Click(driver, loginBtn);
        }

        /// <summary>
        /// To check if login was successful
        /// </summary>
        /// <param name="eventName"></param>
        /// <returns></returns>
        public bool VerifyLoginSuccess()
        {
            return (Sync.TextToBePresentInElementLocated(driver, connected, "Connected as"));
        }

        /// <summary>
        /// To check event is there in event list
        /// </summary>
        /// <param name="eventName"></param>
        /// <returns></returns>
        public bool VerifyEventCreated(string eventName)
        {
            Sync.VisibilityOfAllElementsLocatedBy(driver, premierEventsList);
            var newEvent = Locate(premierEventsList).FindElements(WLTRCommon.trTag);
            var s1 = newEvent.FirstOrDefault(c => c.Text.Trim().Contains(eventName));
            var eveList = s1.FindElements(WLTRCommon.tdTag);
            var getEvent = eveList.FirstOrDefault(c => c.Text.Trim().Contains(eventName));
            return getEvent.Text.Equals(eventName) ? true : false;

        }

        /// <summary>
        /// To click on Not Connected Link in WLTR homepage
        /// </summary>
        public void ClickNotConLink()
        {
            CommonActions.Click(driver, notConnectedLink);
        }

        /// <summary>
        /// To click on connect drop down option
        /// </summary>
        public void ClickConnectLink()
        {
            CommonActions.Click(driver, connect);
        }

    }
}

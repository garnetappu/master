﻿using Microsoft.Exchange.WebServices.Data;
using System;
using System.Configuration;

namespace Wizards.Automation.Framework.Utils
{
    public class EmailUtility
    {        
        public bool SendEmail(string fromEmailId, string displayName, string toEmailId, string attachmentFileNameWithPath,
            string subject, string body, bool isHtml)
        {
            bool isSuccess = false;
            try
            {
                ExchangeService service = new ExchangeService(ExchangeVersion.Exchange2007_SP1);
                service.Url = new Uri(ConfigurationManager.AppSettings["ServiceUrl"]);
                service.Credentials = new WebCredentials(ConfigurationManager.AppSettings["NTID"],
                    ConfigurationManager.AppSettings["Password"], ConfigurationManager.AppSettings["Domain"]);
                EmailMessage message = new EmailMessage(service);

                message.Subject = subject;
                message.Body = body;

                if (isHtml)
                    message.Body.BodyType = BodyType.HTML;                
                message.ToRecipients.Add(toEmailId);
                message.Subject = subject;
                message.Body = body;
                string[] attachments = attachmentFileNameWithPath.Split(',');
                foreach (string attachment in attachments)
                {
                    message.Attachments.AddFileAttachment(attachment);
                }
                message.SendAndSaveCopy();
                isSuccess = true;

            }
            catch (Exception Ex)
            {
                Console.WriteLine(Ex.Message.ToString());                
                throw;
            }
            return isSuccess;
        }       
    }
}

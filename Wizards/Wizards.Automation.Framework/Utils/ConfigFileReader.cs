﻿using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Xml;

namespace Wizards.Automation.Framework.Utils
{
    public class ConfigFileReader
    {
        public Dictionary<string, string> ReadConfigFile(string Category)
        {
            string deployDir = @"C:\Users\Public\Documents\deploy";
            var dirPath = Directory.GetCurrentDirectory();
            Dictionary<string, string> configList = new Dictionary<string, string>();
            try
            {
                XmlDocument xDoc = new XmlDocument();
                if (Directory.Exists(deployDir))
                {
                    dirPath = deployDir;
                }
                else
                {
                    dirPath = Directory.GetParent(dirPath).Parent.Parent.Parent.FullName;
                    dirPath = dirPath + @"\AutomationFramework\Wizards.Automation.Framework";
                }
                xDoc.Load(Path.Combine(dirPath, "Mobile.config"));
                XmlNodeList nodes = xDoc.SelectNodes("//configuration//" + Category).Item(0).ChildNodes;
                foreach (XmlNode singleNode in nodes)
                {
                    if (singleNode.NodeType == XmlNodeType.Element)
                        configList.Add(singleNode.Name, singleNode.InnerText);
                }
            }
            catch
            {
                throw new ConfigurationErrorsException("Category not found");
            }
            return configList;
        }

        public string TestResultPath
        {
            get
            {
                string path = ConfigurationManager.AppSettings["test-result-path"];

                if (string.IsNullOrEmpty(path))
                {
                    return @"TestResults\";
                }

                return path;
            }
        }

        public string TestDataPath
        {
            get
            {
                string path = ConfigurationManager.AppSettings["test-data-path"];

                if (string.IsNullOrEmpty(path))
                {
                    return @"TestData\";
                }

                return path;
            }
        }


    }


}

﻿using Wizards.Automation.Framework.Utils;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Android;
using OpenQA.Selenium.Appium.iOS;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.PhantomJS;
using OpenQA.Selenium.Remote;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using System.Threading;
using Wizards.Automation.Framework.DataAccess;

namespace Wizards.Automation.Framework.Setup
{
    public class DriverFactory
    {
        private FirefoxProfile ffp;
        public IWebDriver driver;
        public AndroidDriver<AppiumWebElement> appiumAndroid_driver;
        public IOSDriver<AppiumWebElement> appiumIOS_driver;
        public DesiredCapabilities capabilities;
        private string url;
        private ConfigFileReader conf;

        public IWebDriver GetWebDriver()
        {
            string deployDir = "";
            string Browser = ConfigurationManager.AppSettings["Browser"];
            string runLocal = ConfigurationManager.AppSettings["RunLocal"];
            if (runLocal.Equals("False", StringComparison.InvariantCultureIgnoreCase))
            {
                return GetRemoteWebDriver();
            }       
            else if (!String.IsNullOrEmpty(Browser))
            {
                switch (Browser)
                {
                    case "Firefox":
                        ffp = new FirefoxProfile();
                        ffp.AcceptUntrustedCertificates = true;
                        ffp.EnableNativeEvents = false;
                        driver = new FirefoxDriver(ffp);
                        break;
                    case "IE":
                        var optionsIE = new InternetExplorerOptions();
                        optionsIE.IntroduceInstabilityByIgnoringProtectedModeSettings = true;
                        optionsIE.IgnoreZoomLevel = true;
                        optionsIE.EnableNativeEvents = true;
                        optionsIE.EnablePersistentHover = true;
                        optionsIE.ElementScrollBehavior = InternetExplorerElementScrollBehavior.Top;
                        optionsIE.RequireWindowFocus = true;
                        driver = (Directory.Exists(deployDir)) ? new InternetExplorerDriver(deployDir, optionsIE)
                            : new InternetExplorerDriver(optionsIE);
                        break;
                    case "Chrome":
                        driver = (Directory.Exists(deployDir)) ? new ChromeDriver(deployDir)
                            : new ChromeDriver();
                        break;
                    case "ChromeService":                       
                        driver = LaunchNodeWebKitApp(true); 
                        Thread.Sleep(20000);
                        break;
                    default:
                        Debug.WriteLine("App.config key error");
                        Debug.WriteLine("Defaulting to Firefox");
                        driver = new FirefoxDriver();
                        break;
                }
            }
            else
            {
                Debug.WriteLine("* * * DEFAULTMODE * * *");
                Debug.WriteLine("App.config key not present.");
                driver = new FirefoxDriver();
            }
            driver.Manage().Timeouts().ImplicitlyWait(new TimeSpan(0, 0, 25));
            driver.Manage().Timeouts().SetPageLoadTimeout(TimeSpan.FromSeconds(90));
            
            return driver;
        }


        public IWebDriver LaunchNodeWebKitApp()
        {
            return LaunchApp();
           
        }
        public IWebDriver LaunchNodeWebKitApp(bool cleanWLTRData)
        {

            RemoveWltrTempFiles();
            return LaunchApp();


        }

        private void RemoveWltrTempFiles()
        {
            string appdataPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            int index = appdataPath.LastIndexOf('\\');
            appdataPath = appdataPath.Substring(0, index);
            string fileName = "event_reporter_url.txt";
            if (Directory.Exists(appdataPath + "\\Local\\WLTR"))
            {
                string[] filePaths = Directory.GetFiles(appdataPath + "\\Local\\WLTR");
                foreach (string filePath in filePaths)
                {
                    var name = new FileInfo(filePath).Name;
                    if (name != fileName)
                    {
                        File.Delete(filePath);
                    }
                }
            }

        }

        private IWebDriver LaunchApp()
        {
            
            System.Diagnostics.Process[] process = System.Diagnostics.Process.GetProcessesByName("chromedriver2_server");
            // Before starting the new process make sure no other MyProcessName is running.
            foreach (System.Diagnostics.Process p in process)
            {
                p.Kill();
            }
            
            string AppFolderPath = ConfigurationManager.AppSettings["AppFolderPath"];
            
           // var di = new DirectoryInfo(AppFolderPath);
           // di.Attributes &= ~FileAttributes.ReadOnly;
            TestDataPath applicationPath = new TestDataPath();
            string driverpath = applicationPath.PathtoTestData(ConfigurationManager.AppSettings["TestDataFolder"]);
            if (!File.Exists(AppFolderPath + "\\chromedriver2_server.exe"))
            {
                File.Copy(driverpath + "\\chromedriver2_server.exe", AppFolderPath + "\\chromedriver2_server.exe", true);
            }
            if (File.Exists(AppFolderPath + "\\WLTR.exe"))
            {
                if(File.Exists(AppFolderPath + "\\nw.exe"))
                {
                    File.Delete(AppFolderPath + "\\nw.exe");
                }
                File.Move(AppFolderPath + "\\WLTR.exe", AppFolderPath + "\\nw.exe");
            }

            int i = 0;
            bool appLaunched = false;
            while (appLaunched == false && i < 3)
            {
                try
                {
                    i++;
                    ChromeDriverService service = ChromeDriverService.CreateDefaultService(AppFolderPath, "chromedriver2_server.exe");
                    service.Start();
                    Thread.Sleep(20000);
                    driver = new ChromeDriver(service);
                    appLaunched = true;
                }
                catch(Exception)
                {
                    appLaunched = false;
                }
            }
            return driver;
        } 

        public IWebDriver GetRemoteWebDriver()
        {
            ConfigFileReader configReader = new ConfigFileReader();
            Dictionary<string, string> configList = configReader.ReadConfigFile("TestSettings");
            string environment = configList["DriverType"];


            DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
            switch (environment)
            {
                case "FF":
                    desiredCapabilities = DesiredCapabilities.Firefox();
                    desiredCapabilities.SetCapability(CapabilityType.BrowserName, "firefox");
                    break;
                case "IE":
                    desiredCapabilities = DesiredCapabilities.InternetExplorer();
                    desiredCapabilities.SetCapability(CapabilityType.BrowserName, "InternetExplorer");
                    break;
                case "CHROME":
                    desiredCapabilities = DesiredCapabilities.Chrome();
                    desiredCapabilities.SetCapability(CapabilityType.BrowserName, "Chrome");
                    break;

            }

            desiredCapabilities.SetCapability(CapabilityType.Platform, new Platform(PlatformType.Windows));
            

            driver = new RemoteWebDriver(new Uri("http://localhost:4444/wd/hub"), desiredCapabilities);
            driver.Manage().Timeouts().SetPageLoadTimeout(TimeSpan.FromSeconds(60));
            driver.Manage().Window.Maximize();

            return driver;
        }

        public AppiumDriver<AppiumWebElement> GetMobileDriver()
        {
            capabilities = new DesiredCapabilities();
            conf = new ConfigFileReader();
            Dictionary<string, string> configList = conf.ReadConfigFile("TestSettings");
            string target_device = configList["TargetDevice"];

            if (target_device.Equals("Android"))
            {
                capabilities.SetCapability("device", conf.ReadConfigFile("MobileSettings"));
                capabilities.SetCapability(CapabilityType.Platform, configList["CapabilityType.Platform"]);
                capabilities.SetCapability("deviceName", configList["DeviceName"]);
                capabilities.SetCapability(CapabilityType.BrowserName, configList["PlatformName"]);
                capabilities.SetCapability(CapabilityType.Version, configList["PlatformVersion"]);
                capabilities.SetCapability("newCommandTimeout", configList["NewCommandTimeOut"]);
                capabilities.SetCapability("udid", configList["deviceId"]); 
                url = configList["URL"];
                return appiumAndroid_driver = new AndroidDriver<AppiumWebElement>(new Uri(url), capabilities, TimeSpan.FromSeconds(240));
            }
            if (target_device.Equals("IOS"))
            {
                Dictionary<string, string> capabilityList = conf.ReadConfigFile("MobileSettings");
                capabilities.SetCapability(CapabilityType.BrowserName, capabilityList["CapabilityType.BROWSER_NAME"]);
                capabilities.SetCapability(CapabilityType.Version, capabilityList["CapabilityType.VERSION"]);
                capabilities.SetCapability(CapabilityType.Platform, capabilityList["CapabilityType.PLATFORM"]);
                capabilities.SetCapability("deviceName", capabilityList["device"]);
                capabilities.SetCapability("udid", capabilityList["udid"]);
                capabilities.SetCapability("bundleid", capabilityList["bundleid"]);
                capabilities.SetCapability("newCommandTimeout", capabilityList["NewCommandTimeOut"]);               
                url = capabilityList["URL"];
                return appiumIOS_driver = new IOSDriver<AppiumWebElement>(new Uri(url), capabilities, TimeSpan.FromSeconds(240));
            }
            return null;
        }
    }
}

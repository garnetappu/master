﻿using Wizards.Automation.Framework.Utils;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Android;
using OpenQA.Selenium.Appium.iOS;
using OpenQA.Selenium.Remote;
using System;

namespace Wizards.Automation.Framework.Setup
{
    public static class RemoteMobileDriver
    {

        //TODO: Consider abstracting driver 
        private static AppiumDriver<AppiumWebElement> _driver;

        public const int DefaultWaitSeconds = 30;
        public static PlatformMobile Platform { get; set; }

        //TODO: unlikely to fix but not thread safe
        public static void Initialize(Uri hostUri, PlatformMobile platform, DesiredCapabilities capabilities)
        {
            if (_driver != null)
                throw new Exception("Unable to create multiple instances of appium driver");

            Platform = platform;
            switch (platform)
            {
                case PlatformMobile.Android:
                    _driver = new AndroidDriver<AppiumWebElement>(hostUri, capabilities);
                    break;
                case PlatformMobile.Ios:
                    _driver = new IOSDriver<AppiumWebElement>(hostUri, capabilities);
                    break;
                default:
                    throw new Exception("Unsupported driver for platform:  " + platform);
            }

            _driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(DefaultWaitSeconds));
        }

        public static AppiumDriver<AppiumWebElement> GetInstance()
        {
            return _driver;
        }

        public static void CleanUp()
        {
            _driver.Quit();
            _driver.Dispose();
            _driver = null;
        }
    }
}

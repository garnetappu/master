﻿using System.Configuration;
using HandyConfig.Configuration;

namespace Wizards.Automation.Framework.Configuration
{
    public class CapabilitiesElement : HandyConfigElement
    {
        [ConfigurationProperty("android")]
        public AndroidElement Android { get { return (AndroidElement)this["android"]; } }

        [ConfigurationProperty("ios")]
        public IosElement Ios { get { return (IosElement)this["ios"]; } }
    }
}
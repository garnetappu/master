﻿using System.Configuration;
using HandyConfig.Configuration;

namespace Wizards.Automation.Framework.Configuration
{
    public class IosElement : HandyConfigElement
    {
        [ConfigurationProperty("devices", IsRequired = true)]
        public DeviceElementCollection Devices
        {
            get { return this["devices"] as DeviceElementCollection; }
        }
    }
}

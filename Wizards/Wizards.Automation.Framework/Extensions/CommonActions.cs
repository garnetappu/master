﻿using NLog;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wizards.Automation.Framework.Extensions
{
    public class CommonActions
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        // Summary:
        //     Clicks the mouse at the last known mouse coordinates.
        //
        // Returns:
        //     A self-reference to this OpenQA.Selenium.Interactions.Actions.
        public static void Click(IWebDriver driver, By locator)
        {
            if (driver.GetType().Name == "ChromeDriver")
            {
                Sync.ElementToBeClickable(driver, locator).Click();
            }
            else
            {
                IWebElement onElement = Sync.ElementToBeClickable(driver, locator);
                ((IJavaScriptExecutor)driver).ExecuteScript("arguments[0].click();", onElement);
            }       
        }
        //
        // Summary:
        //     Clicks the mouse on the specified element.
        //
        // Parameters:
        //   onElement:
        //     The element on which to click.
        //
        // Returns:
        //     A self-reference to this OpenQA.Selenium.Interactions.Actions.
        public static void Click(IWebDriver driver, IWebElement onElement)
        {
            if (driver.GetType().Name == "ChromeDriver")
            {
                Sync.ElementToBeClickable(driver, onElement).Click();
            }
            else
            {
                ((IJavaScriptExecutor)driver).ExecuteScript("arguments[0].click();", onElement);
            }            
        }
        //
        // Summary:
        //      Clicks and holds the mouse button at the last known mouse coordinates.
        //
        // Returns:
        //     A self-reference to this OpenQA.Selenium.Interactions.Actions.
        public static void ClickAndHold(IWebDriver driver, By locator)
        {
            try
            {
                Sync.ElementToBeClickable(driver, locator);
                Actions oAction = new Actions(driver);
                oAction.ClickAndHold().Perform();
            }
            catch (Exception e)
            {
                logger.Debug(" Element not found");
                Console.WriteLine(e.Message);
            }
        }
        //
        // Summary:
        //     Clicks and holds the mouse button down on the specified element.
        //
        // Parameters:
        //   onElement:
        //     The element on which to click and hold.
        //
        // Returns:
        //     A self-reference to this OpenQA.Selenium.Interactions.Actions.
        public static void ClickAndHold(IWebDriver driver, IWebElement onElement)
        {
            try
            {
                Sync.ElementToBeClickable(driver, onElement);
                Actions oAction = new Actions(driver);
                oAction.ClickAndHold().Perform();
            }
            catch (Exception e)
            {
                logger.Debug(" Element not found.");
                Console.WriteLine(e.Message);
            }
        }
        //
        // Summary:
        //     Right-clicks the mouse at the last known mouse coordinates.
        //
        // Returns:
        //     A self-reference to this OpenQA.Selenium.Interactions.Actions.
        public static void ContextClick(IWebDriver driver, By locator)
        {
            try
            {
                Sync.ElementToBeClickable(driver, locator);
                Actions oAction = new Actions(driver);
                oAction.ContextClick().Perform();
            }
            catch (Exception e)
            {
                logger.Debug(" Element not found.");
                Console.WriteLine(e.Message);
            }
        }
        //
        // Summary:
        //     Right-clicks the mouse on the specified element.
        //
        // Parameters:
        //   onElement:
        //     The element on which to right-click.
        //
        // Returns:
        //     A self-reference to this OpenQA.Selenium.Interactions.Actions.
        public static void ContextClick(IWebDriver driver, IWebElement onElement)
        {
            try
            {
                Sync.ElementToBeClickable(driver, onElement);
                Actions oAction = new Actions(driver);
                oAction.ContextClick().Perform();
            }
            catch (Exception e)
            {
                logger.Debug(" Element not found.");
                Console.WriteLine(e.Message);
            }
        }
        //
        // Summary:
        //     Double-clicks the mouse at the last known mouse coordinates.
        //
        // Returns:
        //     A self-reference to this OpenQA.Selenium.Interactions.Actions.
        public static void DoubleClick(IWebDriver driver, By locator)
        {
            try
            {
                Sync.ElementToBeClickable(driver, locator);
                Actions oAction = new Actions(driver);
                oAction.DoubleClick().Perform();
            }
            catch (Exception e)
            {
                logger.Debug(" Element not found.");
                Console.WriteLine(e.Message);
            }
        }
        //
        // Summary:
        //     Double-clicks the mouse on the specified element.
        //
        // Parameters:
        //   onElement:
        //     The element on which to double-click.
        //
        // Returns:
        //     A self-reference to this OpenQA.Selenium.Interactions.Actions.
        public static void DoubleClick(IWebDriver driver, IWebElement onElement)
        {
            try
            {
                Sync.ElementToBeClickable(driver, onElement);
                Actions oAction = new Actions(driver);
                oAction.DoubleClick(onElement).Perform();
            }
            catch (Exception e)
            {
                logger.Debug(" Element not found.");
                Console.WriteLine(e.Message);
            }
        }
        //
        // Summary:
        //     Performs a drag-and-drop operation from one element to another.
        //
        // Parameters:
        //   source:
        //     The element on which the drag operation is started.
        //
        //   target:
        //     The element on which the drop is performed.
        //
        // Returns:
        //     A self-reference to this OpenQA.Selenium.Interactions.Actions.
        public static void DragAndDrop(IWebDriver driver, IWebElement source, IWebElement target)
        {
            try
            {
                Actions oAction = new Actions(driver);
                oAction.DragAndDrop(Sync.ElementToBeClickable(driver, source), Sync.ElementToBeClickable(driver, target)).Perform();
            }
            catch (Exception e)
            {
                logger.Debug(" One or more elements not found.");
                Console.WriteLine(e.Message);
            }
        }
        //
        // Summary:
        //     Performs a drag-and-drop operation from one element to another.
        //
        // Parameters:
        //   source:
        //     The element on which the drag operation is started.
        //
        //   target:
        //     The element on which the drop is performed.
        //
        // Returns:
        //     A self-reference to this OpenQA.Selenium.Interactions.Actions.
        public static void DragAndDrop(IWebDriver driver, By source, By target)
        {
            try
            {
                IWebElement draggable = Sync.ElementToBeClickable(driver, source);
                IWebElement droppaable = Sync.ElementToBeClickable(driver, target);

                Actions oAction = new Actions(driver);
                oAction.DragAndDrop(draggable, droppaable).Perform();
            }
            catch (Exception e)
            {
                logger.Debug(" One or more elements not found.");
                Console.WriteLine(e.Message);
            }
        }
        //
        // Summary:
        //     Performs a drag-and-drop operation on one element to a specified offset.
        //
        // Parameters:
        //   source:
        //     The element on which the drag operation is started.
        //
        //   offsetX:
        //     The horizontal offset to which to move the mouse.
        //
        //   offsetY:
        //     The vertical offset to which to move the mouse.
        //
        // Returns:
        //     A self-reference to this OpenQA.Selenium.Interactions.Actions.
        public static void DragAndDropToOffset(IWebDriver driver, IWebElement source, int offsetX, int offsetY)
        {
            try
            {
                IWebElement draggable = Sync.ElementToBeClickable(driver, source);

                Actions oAction = new Actions(driver);
                oAction.DragAndDropToOffset(draggable, offsetX, offsetY).Perform();
            }
            catch (Exception e)
            {
                logger.Debug(" Element not found.");
                Console.WriteLine(e.Message);
            }
        }
        //
        // Summary:
        //     Sends a sequence of keystrokes to the browser.
        //
        // Parameters:
        //   keysToSend:
        //     The keystrokes to send to the browser.
        //
        // Returns:
        //     A self-reference to this OpenQA.Selenium.Interactions.Actions.
        public static void Type(IWebDriver driver, By locator, string keysToSend)
        {
            try
            {
                Sync.ElementToBeClickable(driver, locator).Clear();
                Sync.ElementToBeClickable(driver, locator).SendKeys(keysToSend);
            }
            catch (Exception e)
            {
                logger.Debug(" Element not found.");
                Console.WriteLine(e.Message);
            }
        }
        //
        // Summary:
        //     Sends a sequence of keystrokes to the specified element in the browser.
        //
        // Parameters:
        //   element:
        //     The element to which to send the keystrokes.
        //
        //   keysToSend:
        //     The keystrokes to send to the browser.
        //
        // Returns:
        //     A self-reference to this OpenQA.Selenium.Interactions.Actions.
        public static void Type(IWebDriver driver, IWebElement element, string keysToSend)
        {
            try
            {
                Sync.ElementToBeClickable(driver, element).Clear();
                Sync.ElementToBeClickable(driver, element).SendKeys(keysToSend);
            }
            catch (Exception e)
            {
                logger.Debug(" Element not found.");
                Console.WriteLine(e.Message);
            }
        }
        //
        // Summary:
        //     Select the option by the index, as determined by the "index" attribute of the
        //     element.
        //
        // Parameters:
        //   index:
        //     The value of the index attribute of the option to be selected.
        //
        // Exceptions:
        //   T:OpenQA.Selenium.NoSuchElementException:
        //     Thrown when no element exists with the specified index attribute.
        public static void SelectByIndex(IWebDriver driver, IWebElement element, int item)
        {
            try
            {
                Sync.ElementToBeSelected(driver, element);
                SelectElement se = new SelectElement(element);
                se.SelectByIndex(item);
            }
            catch (Exception e)
            {
                logger.Debug(" Element not found.");
                Console.WriteLine(e.Message);
            }
        }
        //
        // Summary:
        //     Select the option by the index, as determined by the "index" attribute of the
        //     element.
        //
        // Parameters:
        //   index:
        //     The value of the index attribute of the option to be selected.
        //
        // Exceptions:
        //   T:OpenQA.Selenium.NoSuchElementException:
        //     Thrown when no element exists with the specified index attribute.
        public static void SelectByValue(IWebDriver driver, IWebElement element, string value)
        {
            try
            {
                //Sync.ElementToBeSelected(driver, element);
                SelectElement se = new SelectElement(element);
                se.SelectByValue(value);
            }
            catch (Exception e)
            {
                logger.Debug(" Element not found.");
                Console.WriteLine(e.Message);
            }
        }
        //
        // Summary:
        //     Select all options by the text displayed.
        //
        // Parameters:
        //   text:
        //     The text of the option to be selected. If an exact match is not found, this method
        //     will perform a substring match.
        //
        // Exceptions:
        //   T:OpenQA.Selenium.NoSuchElementException:
        //     Thrown if there is no element with the given text present.
        //
        // Remarks:
        //     When given "Bar" this method would select an option like:
        //     <option value="foo">Bar</option>
        public static void SelectByText(IWebDriver driver, IWebElement element, string text)
        {
            try
            {
                Sync.ElementToBeSelected(driver, element);
                SelectElement se = new SelectElement(element);
                se.SelectByText(text);
            }
            catch (Exception e)
            {
                logger.Debug(" Element not found.");
                Console.WriteLine(e.Message);
            }
        }

        /// <summary>
        /// Click on element and press Enter
        /// </summary>
        /// <param name="driver"></param>
        /// <param name="element"></param>
        public static void ClickEnter(IWebDriver driver, IWebElement element)
        {
            Actions oAction = new Actions(driver);
            oAction.Click(element).SendKeys(Keys.Enter).Build().Perform();
        }


    }
}

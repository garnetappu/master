﻿using System;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium;
using NLog;


namespace Wizards.Automation.Framework
{
    public class Sync
    {
        static int WaitTime = string.IsNullOrEmpty(ConfigurationManager.AppSettings["DefaultWaitTime"])==true?30: Convert.ToInt32(ConfigurationManager.AppSettings["DefaultWaitTime"]);
        static int pollingWait = 100;
        private static Logger logger = LogManager.GetCurrentClassLogger();

        // Summary:
        //     An expectation for checking the AlterIsPresent
        //
        // Returns:
        //     Alert
        public static IAlert IsAlertIsPresent(IWebDriver driver)
        {
            IAlert alertPresence = null;

            try
            {
                IWait<IWebDriver> wait = new DefaultWait<IWebDriver>(driver);
                wait.Timeout = TimeSpan.FromSeconds(WaitTime);
                wait.PollingInterval = TimeSpan.FromMilliseconds(pollingWait);
                wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
                alertPresence = wait.Until(ExpectedConditions.AlertIsPresent());
            }
            catch (Exception e)
            {
                logger.Debug(" Alert not present.");
                Console.WriteLine(e.Message);
            }
            return alertPresence;
        }
        //
        // Summary:
        //     An expectation for checking the Alert State
        //
        // Parameters:
        //   state:
        //     A value indicating whether or not an alert should be displayed in order to
        //     meet this condition.
        //
        // Returns:
        //     true alert is in correct state present or not present; otherwise, false.
        public static bool AlertState(IWebDriver driver, bool state)
        {
            bool alertState = false;

            try
            {
                IWait<IWebDriver> wait = new DefaultWait<IWebDriver>(driver);
                wait.Timeout = TimeSpan.FromSeconds(WaitTime);
                wait.PollingInterval = TimeSpan.FromMilliseconds(pollingWait);
                wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
                alertState = wait.Until(ExpectedConditions.AlertState(state));
            }
            catch (Exception e)
            {
                logger.Debug(" Alert not present.");
                Console.WriteLine(e.Message);
            }
            return alertState;            
        }
        //
        // Summary:
        //     An expectation for checking that an element is present on the DOM of a page.
        //     This does not necessarily mean that the element is visible.
        //
        // Parameters:
        //   locator:
        //     The locator used to find the element.
        //
        // Returns:
        //     The OpenQA.Selenium.IWebElement once it is located.
        public static IWebElement ElementExists(IWebDriver driver, By locator)
        {
            IWebElement Element = null;

            try
            {
                IWait<IWebDriver> wait = new DefaultWait<IWebDriver>(driver);
                wait.Timeout = TimeSpan.FromSeconds(WaitTime);
                wait.PollingInterval = TimeSpan.FromMilliseconds(pollingWait);
                wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
                Element = wait.Until(ExpectedConditions.ElementExists(locator));
            }
            catch (Exception e)
            {
                logger.Debug(locator +" Element not exists.");
                Console.WriteLine(e.Message);
            }
            return Element; 
        }
        //
        // Summary:
        //     An expectation for checking that an element is present on the DOM of a page
        //     and visible. Visibility means that the element is not only displayed but
        //     also has a height and width that is greater than 0.
        //
        // Parameters:
        //   locator:
        //     The locator used to find the element.
        //
        // Returns:
        //     The OpenQA.Selenium.IWebElement once it is located and visible.
        public static IWebElement ElementIsVisible(IWebDriver driver, By locator)
        {
            IWebElement Element = null;

            try
            {
                IWait<IWebDriver> wait = new DefaultWait<IWebDriver>(driver);
                wait.Timeout = TimeSpan.FromSeconds(WaitTime);
                wait.PollingInterval = TimeSpan.FromMilliseconds(pollingWait);
                wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
                Element = wait.Until(ExpectedConditions.ElementIsVisible(locator));
            }
            catch (Exception e)
            {
                logger.Debug(locator +" not visible.");
                Console.WriteLine(e.Message);
            }
            return Element; 
            
        }
        //
        // Summary:
        //     An expectation for checking if the given element is in correct state.
        //
        // Parameters:
        //   locator:
        //     The locator used to find the element.
        //
        //   selected:
        //     selected or not selected
        //
        // Returns:
        //     true given element is in correct state.; otherwise, false.
        public static bool ElementSelectionStateToBe(IWebDriver driver, By locator, bool selected)
        {
            bool ElementSelection = false;

            try
            {
                IWait<IWebDriver> wait = new DefaultWait<IWebDriver>(driver);
                wait.Timeout = TimeSpan.FromSeconds(WaitTime);
                wait.PollingInterval = TimeSpan.FromMilliseconds(pollingWait);
                wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
                ElementSelection = wait.Until(ExpectedConditions.ElementSelectionStateToBe(locator, selected));
            }
            catch (Exception e)
            {
                logger.Debug(locator +" not present.");
                Console.WriteLine(e.Message);
            }
            return ElementSelection; 
        }
        //
        // Summary:
        //     An expectation for checking if the given element is in correct state.
        //
        // Parameters:
        //   element:
        //     The element.
        //
        //   selected:
        //     selected or not selected
        //
        // Returns:
        //     true given element is in correct state.; otherwise, false.
        public static bool ElementSelectionStateToBe(IWebDriver driver, IWebElement locator, bool selected)
        {
            bool elementSelection = false;
            try
            {
                IWait<IWebDriver> wait = new DefaultWait<IWebDriver>(driver);
                wait.Timeout = TimeSpan.FromSeconds(WaitTime);
                wait.PollingInterval = TimeSpan.FromMilliseconds(pollingWait);
                wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
                elementSelection = wait.Until(ExpectedConditions.ElementSelectionStateToBe(locator, selected));
            }
            catch (Exception e)
            {
                logger.Debug(locator + "is not visible.");
                Console.WriteLine(e.Message);
            }
            return elementSelection;
        }
        //
        // Summary:
        //     An expectation for checking an element is visible and enabled such that you
        //     can click it.
        //
        // Parameters:
        //   locator:
        //     The locator used to find the element.
        //
        // Returns:
        //     The OpenQA.Selenium.IWebElement once it is located and clickable (visible
        //     and enabled).
        public static IWebElement ElementToBeClickable(IWebDriver driver, By locator)
        {
            IWebElement Element = null;
            try
            {
                IWait<IWebDriver> wait = new DefaultWait<IWebDriver>(driver);
                wait.Timeout = TimeSpan.FromSeconds(WaitTime);
                wait.PollingInterval = TimeSpan.FromMilliseconds(pollingWait);
                wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
                Element = wait.Until(ExpectedConditions.ElementToBeClickable(locator));
            }
            catch (Exception e)
            {
                logger.Debug(locator + "is not clickable.");
                Console.WriteLine(e.Message);
            }
            return Element;
        }
        //
        // Summary:
        //     An expectation for checking an element is visible and enabled such that you
        //     can click it.
        //
        // Parameters:
        //   element:
        //     The element.
        //
        // Returns:
        //     The OpenQA.Selenium.IWebElement once it is clickable (visible and enabled).
        public static IWebElement ElementToBeClickable(IWebDriver driver, IWebElement element)
        {
            IWebElement Element=null;
            try
            {
                IWait<IWebDriver> wait = new DefaultWait<IWebDriver>(driver);
                wait.Timeout = TimeSpan.FromSeconds(WaitTime);
                wait.PollingInterval = TimeSpan.FromMilliseconds(pollingWait);
                wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
                Element = wait.Until(ExpectedConditions.ElementToBeClickable(element));
            }
            catch (Exception e)
            {
                logger.Debug(element + "is not clickable.");
                Console.WriteLine(e.Message);
            }
            return Element;
        }
        //
        // Summary:
        //     An expectation for checking if the given element is selected.
        //
        // Parameters:
        //   locator:
        //     The locator used to find the element.
        //
        // Returns:
        //     true given element is selected.; otherwise, false.
        public static bool ElementToBeSelected(IWebDriver driver, By locator)
        {
            bool Element = false;
            try
            {
                IWait<IWebDriver> wait = new DefaultWait<IWebDriver>(driver);
                wait.Timeout = TimeSpan.FromSeconds(WaitTime);
                wait.PollingInterval = TimeSpan.FromMilliseconds(pollingWait);
                wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
                Element = wait.Until(ExpectedConditions.ElementToBeSelected(locator));
            }
            catch (Exception e)
            {
                logger.Debug(locator + "is not to be selected.");
                Console.WriteLine(e.Message);
            }
            return Element;
        }
        //
        // Summary:
        //     An expectation for checking if the given element is selected.
        //
        // Parameters:
        //   element:
        //     The element.
        //
        // Returns:
        //     true given element is selected.; otherwise, false.
        public static bool ElementToBeSelected(IWebDriver driver, IWebElement element)
        {
            bool Element = false;
            try
            {
                IWait<IWebDriver> wait = new DefaultWait<IWebDriver>(driver);
                wait.Timeout = TimeSpan.FromSeconds(WaitTime);
                wait.PollingInterval = TimeSpan.FromMilliseconds(pollingWait);
                wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
                Element = wait.Until(ExpectedConditions.ElementToBeSelected(element));
            }
            catch (Exception e)
            {
                logger.Debug(element + "text is not not as expected.");
                Console.WriteLine(e.Message);
            }
            return Element;
        }
        //
        // Summary:
        //     An expectation for checking if the given element is selected.
        //
        // Parameters:
        //   element:
        //     The element.
        //
        //   selected:
        //     selected or not selected
        //
        // Returns:
        //     true given element is selected.; otherwise, false.
        public static bool ElementToBeSelected(IWebDriver driver, IWebElement element, bool selected)
        {
            bool Element = false;
            try
            {
                IWait<IWebDriver> wait = new DefaultWait<IWebDriver>(driver);
                wait.Timeout = TimeSpan.FromSeconds(WaitTime);
                wait.PollingInterval = TimeSpan.FromMilliseconds(pollingWait);
                wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
                Element = wait.Until(ExpectedConditions.ElementToBeSelected(element, selected));
            }
            catch (Exception e)
            {
                logger.Debug(element + "is not selected.");
                Console.WriteLine(e.Message);
            }
            return Element;
        }     

        //
        // Summary:
        //     An expectation for checking whether the given frame is available to switch
        //     to. If the frame is available it switches the given driver to the specified
        //     frame.
        //
        // Parameters:
        //   locator:
        //     Locator for the Frame
        //
        // Returns:
        //     OpenQA.Selenium.IWebDriver
        public static IWebDriver FrameToBeAvailableAndSwitchToIt(IWebDriver driver, By locator)
        {
            IWebDriver frame = null;

            try
            {
                IWait<IWebDriver> wait = new DefaultWait<IWebDriver>(driver);
                wait.Timeout = TimeSpan.FromSeconds(WaitTime);
                wait.PollingInterval = TimeSpan.FromMilliseconds(pollingWait);
                wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
                frame = wait.Until(ExpectedConditions.FrameToBeAvailableAndSwitchToIt(locator));
            }
            catch (Exception e)
            {
                logger.Debug(locator + "is not available.");
                Console.WriteLine(e.Message);
            }
            return frame;
        }
        //
        // Summary:
        //     An expectation for checking whether the given frame is available to switch
        //     to. If the frame is available it switches the given driver to the specified
        //     frame.
        //
        // Parameters:
        //   frameLocator:
        //     Used to find the frame (id or name)
        //
        // Returns:
        //     OpenQA.Selenium.IWebDriver
        public static IWebDriver FrameToBeAvailableAndSwitchToIt(IWebDriver driver, string frameLocator)
        {
            IWebDriver frame = null;

            try
            {
                IWait<IWebDriver> wait = new DefaultWait<IWebDriver>(driver);
                wait.Timeout = TimeSpan.FromSeconds(WaitTime);
                wait.PollingInterval = TimeSpan.FromMilliseconds(pollingWait);
                wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
                frame = wait.Until(ExpectedConditions.FrameToBeAvailableAndSwitchToIt(frameLocator));
            }
            catch (Exception e)
            {
                logger.Debug(frameLocator + "is not available.");
                Console.WriteLine(e.Message);
            }
            return frame;
        }
        //
        // Summary:
        //     An expectation for checking that an element is either invisible or not present
        //     on the DOM.
        //
        // Parameters:
        //   locator:
        //     The locator used to find the element.
        //
        // Returns:
        //     true if the element is not displayed; otherwise, false.
        public static bool InvisibilityOfElementLocated(IWebDriver driver, By locator)
        {
            bool Element = false;

            try
            {
                IWait<IWebDriver> wait = new DefaultWait<IWebDriver>(driver);
                wait.Timeout = TimeSpan.FromSeconds(WaitTime);
                wait.PollingInterval = TimeSpan.FromMilliseconds(pollingWait);
                wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
                Element = wait.Until(ExpectedConditions.InvisibilityOfElementLocated(locator));
            }
            catch (Exception e)
            {
                logger.Debug(locator + "is not not visible or not in DOM.");
                Console.WriteLine(e.Message);
            }
            return Element; 
        }
        //
        // Summary:
        //     An expectation for checking that an element with text is either invisible
        //     or not present on the DOM.
        //
        // Parameters:
        //   locator:
        //     The locator used to find the element.
        //
        //   text:
        //     Text of the element
        //
        // Returns:
        //     true if the element is not displayed; otherwise, false.
        public static bool InvisibilityOfElementWithText(IWebDriver driver, By locator, string text)
        {
            bool Element = false;

            try
            {
                IWait<IWebDriver> wait = new DefaultWait<IWebDriver>(driver);
                wait.Timeout = TimeSpan.FromSeconds(WaitTime);
                wait.PollingInterval = TimeSpan.FromMilliseconds(pollingWait);
                wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
                Element = wait.Until(ExpectedConditions.InvisibilityOfElementWithText(locator, text));
            }
            catch (Exception e)
            {
                logger.Debug(text + " or element is not available.");
                Console.WriteLine(e.Message);
            }
            return Element;
        }
        //
        // Summary:
        //     An expectation for checking that all elements present on the web page that
        //     match the locator.
        //
        // Parameters:
        //   locator:
        //     The locator used to find the element.
        //
        // Returns:
        //     The list of OpenQA.Selenium.IWebElement once it is located.
        public static ReadOnlyCollection<IWebElement> PresenceOfAllElementsLocatedBy(IWebDriver driver, By locator)
        {
            ReadOnlyCollection<IWebElement> Elements = null;

            try
            {
                IWait<IWebDriver> wait = new DefaultWait<IWebDriver>(driver);
                wait.Timeout = TimeSpan.FromSeconds(WaitTime);
                wait.PollingInterval = TimeSpan.FromMilliseconds(pollingWait);
                wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
                Elements = wait.Until(ExpectedConditions.PresenceOfAllElementsLocatedBy(locator));
            }
            catch (Exception e)
            {
                logger.Debug(locator + " elements are not present.");
                Console.WriteLine(e.Message);
            }
            return Elements;
        }
        //
        // Summary:
        //     Wait until an element is no longer attached to the DOM.
        //
        // Parameters:
        //   element:
        //     The element.
        //
        // Returns:
        //     false is the element is still attached to the DOM; otherwise, true.
        public static bool StalenessOf(IWebDriver driver, IWebElement element)
        {
            bool Element = false;

            try
            {
                IWait<IWebDriver> wait = new DefaultWait<IWebDriver>(driver);
                wait.Timeout = TimeSpan.FromSeconds(WaitTime);
                wait.PollingInterval = TimeSpan.FromMilliseconds(pollingWait);
                wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
                Element = wait.Until(ExpectedConditions.StalenessOf(element));
            }
            catch (Exception e)
            {
                logger.Debug(element + " element still attached to the DOM.");
                Console.WriteLine(e.Message);
            }
            return Element;
        }
        //
        // Summary:
        //     An expectation for checking if the given text is present in the specified
        //     element.
        //
        // Parameters:
        //   element:
        //     The WebElement
        //
        //   text:
        //     Text to be present in the element
        //
        // Returns:
        //     true once the element contains the given text; otherwise, false.
        public static bool TextToBePresentInElement(IWebDriver driver, IWebElement element, string text)
        {
            bool Element = false;

            try
            {
                IWait<IWebDriver> wait = new DefaultWait<IWebDriver>(driver);
                wait.Timeout = TimeSpan.FromSeconds(WaitTime);
                wait.PollingInterval = TimeSpan.FromMilliseconds(pollingWait);
                wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
                Element = wait.Until(ExpectedConditions.TextToBePresentInElement(element, text));
            }
            catch (Exception e)
            {
                logger.Debug(text + " or element is not present.");
                Console.WriteLine(e.Message);
            }
            return Element;
        }
        //
        // Summary:
        //     An expectation for checking if the given text is present in the element that
        //     matches the given locator.
        //
        // Parameters:
        //   locator:
        //     The locator used to find the element.
        //
        //   text:
        //     Text to be present in the element
        //
        // Returns:
        //     true once the element contains the given text; otherwise, false.
        public static bool TextToBePresentInElementLocated(IWebDriver driver, By locator, string text)
        {
            bool Element = false;

            try
            {
                IWait<IWebDriver> wait = new DefaultWait<IWebDriver>(driver);
                wait.Timeout = TimeSpan.FromSeconds(WaitTime);
                wait.PollingInterval = TimeSpan.FromMilliseconds(pollingWait);
                wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
                Element = wait.Until(ExpectedConditions.TextToBePresentInElementLocated(locator, text));
            }
            catch (Exception e)
            {
                logger.Debug(text + " or element is not present.");
                Console.WriteLine(e.Message);
            }
            return Element;
        }
        //
        // Summary:
        //     An expectation for checking if the given text is present in the specified
        //     elements value attribute.
        //
        // Parameters:
        //   locator:
        //     The locator used to find the element.
        //
        //   text:
        //     Text to be present in the element
        //
        // Returns:
        //     true once the element contains the given text; otherwise, false.
        public static bool TextToBePresentInElementValue(IWebDriver driver, By locator, string text)
        {
            bool Element = false;

            try
            {
                IWait<IWebDriver> wait = new DefaultWait<IWebDriver>(driver);
                wait.Timeout = TimeSpan.FromSeconds(WaitTime);
                wait.PollingInterval = TimeSpan.FromMilliseconds(pollingWait);
                wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
                Element = wait.Until(ExpectedConditions.TextToBePresentInElementValue(locator, text));
            }
            catch (Exception e)
            {
                logger.Debug(text + " or element is not available.");
                Console.WriteLine(e.Message);
            }
            return Element;
        }
        //
        // Summary:
        //     An expectation for checking if the given text is present in the specified
        //     elements value attribute.
        //
        // Parameters:
        //   element:
        //     The WebElement
        //
        //   text:
        //     Text to be present in the element
        //
        // Returns:
        //     true once the element contains the given text; otherwise, false.
        public static bool TextToBePresentInElementValue(IWebDriver driver, IWebElement element, string text)
        {
            bool Element = false;

            try
            {
                IWait<IWebDriver> wait = new DefaultWait<IWebDriver>(driver);
                wait.Timeout = TimeSpan.FromSeconds(WaitTime);
                wait.PollingInterval = TimeSpan.FromMilliseconds(pollingWait);
                wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
                Element = wait.Until(ExpectedConditions.TextToBePresentInElementValue(element, text));
            }
            catch (Exception e)
            {
                logger.Debug(text + " or element is not present.");
                Console.WriteLine(e.Message);
            }
            return Element;
        }
        //
        // Summary:
        //     An expectation for checking that the title of a page contains a case-sensitive
        //     substring.
        //
        // Parameters:
        //   title:
        //     The fragment of title expected.
        //
        // Returns:
        //     true when the title matches; otherwise, false.
        public static bool TitleContains(IWebDriver driver, string title)
        {
            bool pageTitle = false;

            try
            {
                IWait<IWebDriver> wait = new DefaultWait<IWebDriver>(driver);
                wait.Timeout = TimeSpan.FromSeconds(WaitTime);
                wait.PollingInterval = TimeSpan.FromMilliseconds(pollingWait);
                wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
                pageTitle = wait.Until(ExpectedConditions.TitleContains(title));
            }
            catch (Exception e)
            {
                logger.Debug(title + " not exists.");
                Console.WriteLine(e.Message);
            }
            return pageTitle;
        }
        //
        // Summary:
        //     An expectation for checking the title of a page.
        //
        // Parameters:
        //   title:
        //     The expected title, which must be an exact match.
        //
        // Returns:
        //     true when the title matches; otherwise, false.
        public static bool TitleIs(IWebDriver driver, string title)
        {
            bool pageTitle = false;

            try
            {
                IWait<IWebDriver> wait = new DefaultWait<IWebDriver>(driver);
                wait.Timeout = TimeSpan.FromSeconds(WaitTime);
                wait.PollingInterval = TimeSpan.FromMilliseconds(pollingWait);
                wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
                pageTitle = wait.Until(ExpectedConditions.TitleIs(title));
            }
            catch (Exception e)
            {
                logger.Debug(title + " not exists.");
                Console.WriteLine(e.Message);
            }
            return pageTitle;
        }
        //
        // Summary:
        //     An expectation for the URL of the current page to be a specific URL.
        //
        // Parameters:
        //   fraction:
        //     The fraction of the url that the page should be on
        //
        // Returns:
        //     true when the URL contains the text; otherwise, false.
        public static bool UrlContains(IWebDriver driver, string fraction)
        {
            bool urlContains = false;

            try
            {
                IWait<IWebDriver> wait = new DefaultWait<IWebDriver>(driver);
                wait.Timeout = TimeSpan.FromSeconds(WaitTime);
                wait.PollingInterval = TimeSpan.FromMilliseconds(pollingWait);
                wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
                urlContains = wait.Until(ExpectedConditions.UrlContains(fraction));
            }
            catch (Exception e)
            {
                logger.Debug(fraction + " does not contain.");
                Console.WriteLine(e.Message);
            }
            return urlContains;
        }
        //
        // Summary:
        //     An expectation for the URL of the current page to be a specific URL.
        //
        // Parameters:
        //   regex:
        //     The regular expression that the URL should match
        //
        // Returns:
        //     true if the URL matches the specified regular expression; otherwise, false.
        public static bool UrlMatches(IWebDriver driver, string regex)
        {
            bool urlMatch = false;

            try
            {
                IWait<IWebDriver> wait = new DefaultWait<IWebDriver>(driver);
                wait.Timeout = TimeSpan.FromSeconds(WaitTime);
                wait.PollingInterval = TimeSpan.FromMilliseconds(pollingWait);
                wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
                urlMatch = wait.Until(ExpectedConditions.UrlMatches(regex));
            }
            catch (Exception e)
            {
                logger.Debug(regex + " does not match.");
                Console.WriteLine(e.Message);
            }
            return urlMatch;
        }
        //
        // Summary:
        //     An expectation for the URL of the current page to be a specific URL.
        //
        // Parameters:
        //   url:
        //     The URL that the page should be on
        //
        // Returns:
        //     true when the URL is what it should be; otherwise, false.
        public static bool UrlToBe(IWebDriver driver, string url)
        {
            bool urlToBe = false;

            try
            {
                IWait<IWebDriver> wait = new DefaultWait<IWebDriver>(driver);
                wait.Timeout = TimeSpan.FromSeconds(WaitTime);
                wait.PollingInterval = TimeSpan.FromMilliseconds(pollingWait);
                wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
                urlToBe = wait.Until(ExpectedConditions.UrlToBe(url));
            }
            catch (Exception e)
            {
                logger.Debug(url + " does not match.");
                Console.WriteLine(e.Message);
            }
            return urlToBe;
        }
        //
        // Summary:
        //     An expectation for checking that all elements present on the web page that
        //     match the locator are visible. Visibility means that the elements are not
        //     only displayed but also have a height and width that is greater than 0.
        //
        // Parameters:
        //   locator:
        //     The locator used to find the element.
        //
        // Returns:
        //     The list of OpenQA.Selenium.IWebElement once it is located and visible.
        public static ReadOnlyCollection<IWebElement> VisibilityOfAllElementsLocatedBy(IWebDriver driver, By locator)
        {
            ReadOnlyCollection<IWebElement> Elements = null;

            try
            {
                IWait<IWebDriver> wait = new DefaultWait<IWebDriver>(driver);
                wait.Timeout = TimeSpan.FromSeconds(WaitTime);
                wait.PollingInterval = TimeSpan.FromMilliseconds(pollingWait);
                wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
                Elements = wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(locator));
            }
            catch (Exception e)
            {
                logger.Debug(locator + " does not match.");
                Console.WriteLine(e.Message);
            }
            return Elements;
        }
        //
        // Summary:
        //     An expectation for checking that all elements present on the web page that
        //     match the locator are visible. Visibility means that the elements are not
        //     only displayed but also have a height and width that is greater than 0.
        //
        // Parameters:
        //   elements:
        //     list of WebElements
        //
        // Returns:
        //     The list of OpenQA.Selenium.IWebElement once it is located and visible.
        public static ReadOnlyCollection<IWebElement> VisibilityOfAllElementsLocatedBy(IWebDriver driver, ReadOnlyCollection<IWebElement> elements)
        {
            ReadOnlyCollection<IWebElement> Elements = null;

            try
            {
                IWait<IWebDriver> wait = new DefaultWait<IWebDriver>(driver);
                wait.Timeout = TimeSpan.FromSeconds(WaitTime);
                wait.PollingInterval = TimeSpan.FromMilliseconds(pollingWait);
                wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
                Elements = wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(elements));
            }
            catch (Exception e)
            {
                logger.Debug(elements + " does not available.");
                Console.WriteLine(e.Message);
            }
            return Elements;
        }
        //
        // Summary:
        //     An expectation for checking an element is displyed and enabled such that you
        //     can click it.
        //
        // Parameters:
        //   locator:
        //     The locator used to find the element.
        //
        // Returns:
        //     The OpenQA.Selenium.IWebElement once it is located and clickable (displayed
        //     and enabled).
        public static bool WaitElement(IWebDriver driver,IWebElement element)
        {
            bool Elements = false;

            try
            {
                IWait<IWebDriver> wait = new DefaultWait<IWebDriver>(driver);
                wait.Timeout = TimeSpan.FromSeconds(WaitTime);
                wait.PollingInterval = TimeSpan.FromMilliseconds(pollingWait);
                wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
                Elements =wait.Until<bool>((d) =>(element.Displayed));
            }
            catch (Exception e)
            {
                logger.Debug(element + " does not available.");
                Console.WriteLine(e.Message);
            }
            return Elements;
            
        }

        //Harhsa.ch
        //Wait For An Element
        public void WaitForElementToLoad(IWebDriver driver,By element)
        {
            IWebElement ele = driver.FindElement(element);
            if(!ele.Displayed)
            {
                driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(3));
                WaitForElementToLoad(driver, element);
            }

        }

        //Harsha.Ch
        //Hightlight Element
        public static void HighLightElement(IWebDriver driver, By element)
        {
            var javaScriptDriver = (IJavaScriptExecutor)driver;
            string highlightJavascript = @"arguments[0].style.cssText = ""border-width: 3px; border-style: solid; border-color: green"";";
            javaScriptDriver.ExecuteScript(highlightJavascript, new object[] { element });
        }
    }
}

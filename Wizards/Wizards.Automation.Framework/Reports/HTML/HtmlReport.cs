﻿using System;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Reflection;
using OpenQA.Selenium;
using AventStack.ExtentReports;
using AventStack.ExtentReports.Reporter;
using Wizards.Automation.Framework.Utils;

namespace Wizards.Automation.Framework.Reports.HTML
{
    public class HtmlReport
    {
        private static ExtentReports extent;
        private static ExtentTest test;
        private static ExtentHtmlReporter htmlReporter;
        private static string tcName;
        Bitmap image;
        private static string solutionPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
        private static DirectoryInfo directory = new DirectoryInfo(
        Path.GetFullPath(Path.Combine(solutionPath, @"..\..\..\")));
        private static string Dir = directory + "Reports";
        private static string ScreenShotDir;
        private static string fileName = "ExtentReport" + string.Format("{0:dd-MMM-yyyy_hh_mm_ss}", DateTime.Now) + ".html";
        private static DirectoryInfo dir = Directory.CreateDirectory(Dir);
        private static String filePath = System.IO.Path.Combine(dir.FullName.ToString(), fileName);
        private static string screenShotPath; 
        public static ExtentReports GetExtent()
        {
            extent = new ExtentReports();
 
            extent.AttachReporter(getHtmlReporter());
            return extent;
        }
 
        private static ExtentHtmlReporter getHtmlReporter()
        {
            if(htmlReporter==null)
            htmlReporter = new ExtentHtmlReporter(filePath);
            htmlReporter.AppendExisting = true;
            return htmlReporter;
        }
 
 
        public void StartReport(string testCaseName,String Description)
        {
           if(extent==null)
            extent = GetExtent();
            tcName = testCaseName;
            test = extent.CreateTest(testCaseName,Description);
 
        }


        public void logMessage(string message)
        {
            test.Pass(message);
        }
        
        public void logStepWeb(string message, string stepinfo, IWebDriver driver)
        {

            HtmlReport.createScreenShotDir();
            if (message == "Passed")
            {
                test.Pass("Passed" + " " + stepinfo);
            }
            else
            {
                CaptureScreenshotUtil.SaveScreenShot(driver, screenShotPath);
                test.Fail("Failed" + " " + stepinfo + " " + test.AddScreenCaptureFromPath(screenShotPath));
            }
            extent.Flush();
        }


        public void logStepWindows(string message, string stepinfo)
        {
            HtmlReport.createScreenShotDir();
            if (message == "Passed")
            {
                test.Pass("Passed" + " " + stepinfo);
            }
            else
            {
                image = CaptureScreenshotUtil.CaptureActiveWindow();
                image.Save(screenShotPath, ImageFormat.Png);
                test.Fail("Failed" + " " + stepinfo + " " + test.AddScreenCaptureFromPath(screenShotPath));
            }
            extent.Flush();


        }

        public static void createScreenShotDir()
        {
            ScreenShotDir = Dir + "\\Screenshots";
            Directory.CreateDirectory(ScreenShotDir);
            screenShotPath = ScreenShotDir + "\\" + tcName + string.Format("{0:ddMMMyyyyhhmmss}", DateTime.Now) + ".png";
        }
    }
}
 
﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.InteropServices;

namespace Wizards.Automation.Framework.DataAccess
{
    public static class SqlDbManager
    {
        public static readonly string ConnectionString = ConfigurationManager.ConnectionStrings["Database"].ConnectionString;        
        public static readonly string defaultSchemaName = ConfigurationManager.AppSettings["DBName"];

        /// <summary>
        /// Executes a stored procedure and pass DataTable as table-valued parameter, can be any entities which require bulk upload 
        /// The calling function must ensure the DataTable is compatible with the table type in the database
        /// </summary>
        /// <param name="storedProcedureName">Name of the stored procedure</param>
        /// /// <param name="data">DataTable filled with values from the source database</param>
        /// <param name="parm">supporting parameters for bulk upload</param>
        /// <returns>number of rows affected, otherwise -1</returns>
        public static int ExecuteStoredProcedureForBulkUpload(string storedProcedureName, object data, Dictionary<string, object> parm, string connectionStringName)
        {
            int numberOfRowsAffected;
            var connString = ConfigurationManager.ConnectionStrings[connectionStringName].ConnectionString;
            using (SqlConnection con = new SqlConnection(connString))
            {
                SqlCommand cmd = new SqlCommand(storedProcedureName, con);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;

                SqlParameter parameter = new SqlParameter();                
                parameter.SqlDbType = System.Data.SqlDbType.Structured;
                parameter.Value = data;
                cmd.Parameters.Add(parameter);

                foreach (KeyValuePair<string, object> kvp in parm)
                {
                    SqlParameter param = new SqlParameter(kvp.Key, kvp.Value);
                    cmd.Parameters.Add(param);
                }
                con.Open();
                numberOfRowsAffected = cmd.ExecuteNonQuery();
            }
            return numberOfRowsAffected;
        }

        /// <summary>
        /// Executes a stored procedure and pass multiple sql parameters
        /// </summary>
        /// <param name="storedProcedureName">Name of the stored procedure</param>
        /// <param name="parm">list of sql parameters that the stored procedure expects</param>
        /// <returns></returns>
        public static int ExecuteNonQuery(string storedProcedureName, Dictionary<string, object> parm)
        {
            int numberOfRowsAffected;
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand(storedProcedureName, con);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;

                foreach (KeyValuePair<string, object> kvp in parm)
                {
                    SqlParameter parameter = new SqlParameter(kvp.Key, kvp.Value);
                    cmd.Parameters.Add(parameter);
                }

                con.Open();
                numberOfRowsAffected = cmd.ExecuteNonQuery();
            }
            return numberOfRowsAffected;
        }

        /// <summary>
        /// Executes a stored procedure and returns a dataset
        /// </summary>
        /// <param name="storedProcedureName">Name of the stored procedure</param>
        /// <param name="parm">list of sql parameters that the stored procedure expects</param>
        /// <returns>dataset with result of the stored procedure</returns>
        public static DataSet ExecuteDataSet(string storedProcedureName, Dictionary<string, object> parm)
        {
            DataSet dsResult = new DataSet();
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand(storedProcedureName, con);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;

                foreach (KeyValuePair<string, object> kvp in parm)
                {
                    SqlParameter parameter = new SqlParameter(kvp.Key, kvp.Value);
                    cmd.Parameters.Add(parameter);
                }

                con.Open();
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(dsResult);
            }
            return dsResult;
        }

        /// <summary>
        /// Executes a stored procedure and returns a scalar
        /// </summary>
        /// <param name="storedProcedureName">Name of the stored procedure</param>
        /// <param name="parm">list of sql parameters that the stored procedure expects</param>
        /// <returns>int as result of the stored procedure</returns>
        public static int ExecuteScalar(string storedProcedureName, Dictionary<string, object> parm)
        {
            int value;
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand(storedProcedureName, con);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;

                foreach (KeyValuePair<string, object> kvp in parm)
                {
                    SqlParameter parameter = new SqlParameter(kvp.Key, kvp.Value);
                    cmd.Parameters.Add(parameter);
                }

                con.Open();
                value = Convert.ToInt32(cmd.ExecuteScalar());
            }
            return value;
        }

        /// <summary>
        /// Executes a Query and returns a dataTable
        /// </summary>
        /// <param name="query">SQL Query that has to be executed</param>
        /// <param name="parm">list of sql parameters that the Query expects</param>
        /// <returns>dataset with result of the query</returns>
        public static DataTable ExecuteQuery(string query)
        {
            DataSet dsResult = new DataSet();
            var connString = ConfigurationManager.ConnectionStrings["Database"].ConnectionString;
            query = AddDefaultSchemaInQuery(query);

            using (SqlConnection con = new SqlConnection(connString))
            {
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandType = System.Data.CommandType.Text;

                con.Open();
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(dsResult);
            }
            return dsResult.Tables[0];
        }

        public static string AddDefaultSchemaInQuery(string query)
        {
            if (query.Contains("[schemaName]"))
            {
                query = query.Replace("schemaName", defaultSchemaName);
            }

            return query;
        }

        

        public static int GetNoOfRowsAffected(string query)
        {
            int numberOfRowsAffected;
            var connString = ConfigurationManager.ConnectionStrings["Database"].ConnectionString;
            query = AddDefaultSchemaInQuery(query);

            using (SqlConnection con = new SqlConnection(connString))
            {
                SqlCommand command = new SqlCommand(query, con);
                con.Open();

                if (query.StartsWith("SELECT"))
                    numberOfRowsAffected = (int)command.ExecuteScalar();

                else
                    numberOfRowsAffected = command.ExecuteNonQuery();
            }

            return numberOfRowsAffected;
        }

        public static int ExectueNonQueryByConnection(string connectionString, string query, [Optional]string ProjectId)
        {
            int numberOfRowsAffected;

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand(query, con);
                con.Open();

                if (query.Contains("_ProjectId_"))
                {
                    query = query.Replace("_ProjectId_", ProjectId);
                }
                
                if (query.StartsWith("SELECT"))
                    numberOfRowsAffected = (int)command.ExecuteScalar();

                else
                    numberOfRowsAffected = command.ExecuteNonQuery();
            }

            return numberOfRowsAffected;
            
        }

        public static string ExecuteQueryByConnection(string connectionString, string query)
        {
            DataSet dsResult = new DataSet();            
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandType = System.Data.CommandType.Text;

                con.Open();
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(dsResult);
            }
            return dsResult.Tables[0].Rows[0]["ProjectID"].ToString();            
        }
    }

}

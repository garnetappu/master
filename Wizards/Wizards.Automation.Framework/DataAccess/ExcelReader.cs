﻿
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Text.RegularExpressions;
using System.Reflection;

namespace Wizards.Automation.Framework.DataAccess
{
    public class ExcelReader
    {
        public string testName, testDirectory;       

        public static DataTable ReadAsDataTable(string fileName)
        {
            DataTable dataTable = new DataTable();

            using (SpreadsheetDocument spreadSheetDocument = SpreadsheetDocument.Open(fileName, false))
            {                
                IEnumerable<Sheet> sheets = spreadSheetDocument.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>();
                string relationshipId = sheets.First().Id.Value;
                WorksheetPart worksheetPart = (WorksheetPart)spreadSheetDocument.WorkbookPart.GetPartById(relationshipId);
                Worksheet workSheet = worksheetPart.Worksheet;
                SheetData sheetData = workSheet.GetFirstChild<SheetData>();
                IEnumerable<Row> rows = sheetData.Descendants<Row>();


                foreach (Cell cell in rows.ElementAt(0))
                {                    
                    dataTable.Columns.Add(GetCellValue(spreadSheetDocument, cell));                   
                }               
                foreach (Row row in rows) //this will also include your header row...
                {
                    DataRow tempRow = dataTable.NewRow();
                    int columnIndex = 0;
                    foreach (Cell cell in row.Descendants<Cell>())
                    {
                        // Gets the column index of the cell with data
                        int cellColumnIndex = (int)GetColumnIndexFromName(GetColumnName(cell.CellReference));
                        cellColumnIndex--; //zero based index
                        if (columnIndex < cellColumnIndex)
                        {
                            do
                            {
                                tempRow[columnIndex] = null; //Insert blank data here;
                                columnIndex++;
                            }
                            while (columnIndex < cellColumnIndex);
                        }
                        tempRow[columnIndex] = GetCellValue(spreadSheetDocument, cell);

                        columnIndex++;
                    }
                    dataTable.Rows.Add(tempRow);
                }

            }
            dataTable.Rows.RemoveAt(0);

            return dataTable;

        }

        public static int? GetColumnIndexFromName(string columnName)
        {            
            string name = columnName;
            int number = 0;
            int pow = 1;
            for (int i = name.Length - 1; i >= 0; i--)
            {
                number += (name[i] - 'A' + 1) * pow;
                pow *= 26;
            }
            return number;
        }

        public static string GetColumnName(string cellReference)
        {
            // Create a regular expression to match the column name portion of the cell name.
            Regex regex = new Regex("[A-Za-z]+");
            Match match = regex.Match(cellReference);
            return match.Value;
        }



        private static string GetCellValue(SpreadsheetDocument document, Cell cell)
        {
            SharedStringTablePart stringTablePart = document.WorkbookPart.SharedStringTablePart;
            string value = cell.CellValue.InnerXml;            
            if (cell.DataType != null && cell.DataType.Value == CellValues.SharedString)
            {
                string str = stringTablePart.SharedStringTable.ChildElements[Int32.Parse(value)].InnerText;
                return stringTablePart.SharedStringTable.ChildElements[Int32.Parse(value)].InnerText;
            }
            else
            {
                return cell.InnerText;
            }
        }



        /// <summary>
        /// Method read data from datatable and provide data in Dictionary 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        /// 
        public T ReadData<T>(string testName)
        {
            Dictionary<string, string> data = new Dictionary<string, string>();
            DataTable testsuitedata;

            TestDataPath objTestDataPath = new TestDataPath();
            string dataFolderName = ConfigurationManager.AppSettings["TestDataFolder"];
            string excelfile = ConfigurationManager.AppSettings["TestFileName"];
            string dataBase = ConfigurationManager.AppSettings["DataBase"];

            Console.WriteLine(dataFolderName + ";" + excelfile + ";" + dataBase);

            string dataPath = objTestDataPath.PathtoTestData(testDirectory != null ? testDirectory : dataFolderName);
            Console.WriteLine("datapath: " + dataPath);
            if (testDirectory == null)
            {
                dataPath = dataPath + dataFolderName;
            }

            testsuitedata = ExcelReader.ReadAsDataTable(dataPath + "\\"+  excelfile);

            return AssignValues<T>(testsuitedata, testName);
        }


        private T AssignValues<T>(DataTable dataTable, string testName)
        {
            DataTable dt = dataTable.Clone();
            Object newObject = CreateParameter<T>();
            DataRow dr = dataTable.AsEnumerable().Where(r => r.Field<string>("TestName").Equals(testName)).FirstOrDefault();
            dt.ImportRow(dr);
            foreach (DataColumn dc in  dt.Columns )
            {
                PropertyInfo str = typeof(T).GetProperty(dc.ToString());
                if (str != null)
                {
                    string value = dt.AsEnumerable().Select(r => r.Field<string>(str.Name)).FirstOrDefault();
                    str.SetValue(newObject, value, null);
                }
            }
            return (T)(object)newObject;
        }

        private T CreateParameter<T>()
        {
            return (T)(object)Activator.CreateInstance(typeof(T), new object[] { });
        }
    }
}

 







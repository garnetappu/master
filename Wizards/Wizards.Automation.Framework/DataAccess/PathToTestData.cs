﻿using System;
using System.IO;

namespace Wizards.Automation.Framework.DataAccess
{
    public class TestDataPath
    {

        public string PathtoTestData(string folderName)
        {

            string fullname = "";
            try
            {
                fullname = Path.GetFullPath(folderName);
                int resultIndex = fullname.IndexOf("TestResult",StringComparison.InvariantCultureIgnoreCase);
                if (resultIndex > 0)
                {
                    fullname = fullname.Substring(0, resultIndex);
                    fullname = fullname + folderName;
                }
                else
                {
                    resultIndex = fullname.IndexOf("Specflow", StringComparison.InvariantCultureIgnoreCase);
                    if (resultIndex > 0)
                    {
                        fullname = fullname.Substring(0, resultIndex);
                        fullname = fullname + folderName;
                    }
                    else
                    {
                        resultIndex = fullname.IndexOf("bin", StringComparison.InvariantCultureIgnoreCase);
                        if (resultIndex > 0)
                        {
                            fullname = fullname.Substring(0, resultIndex);
                        }
                    }
                }

                return fullname;

            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.ToString());
            }
            return fullname;
        }

    }
}

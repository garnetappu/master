﻿using Wizards.Automation.Framework.Utils;
using NLog;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Reflection;
using System.Text;
using OpenQA.Selenium.Support.PageObjects;
using Wizards.Automation.Framework.Extensions;

namespace Wizards.Automation.Framework.Core
{
    public class WebPageBase 
    {
        protected IWebDriver Driver { get; set; }
        private readonly Logger logger = LogManager.GetCurrentClassLogger();
        private readonly TimeSpan defaultTimeSpan = new TimeSpan(0, 0, 10);

        public string WebBrowser { get; set; }
        public int spinnercount = 0;


        ///// <summary>
        ///// Initialize with web driver
        ///// </summary>
        ///// <param name="driver">web driver instance</param>
        public WebPageBase(IWebDriver driver)
        {
            this.Driver = driver;
            PageFactory.InitElements(driver, this);
        }

        ///  Wait for Particular Element to be displayed       
        public void WaitForElement(IWebElement Element, int seconds)
        {
            WebDriverWait wait = new WebDriverWait(Driver, defaultTimeSpan);
            wait.Until<bool>((d) =>
            {
                try
                {
                    if (Element.Enabled)
                        return true;
                    else
                        return false;
                }
                catch (Exception e)
                {
                    logger.Debug(e.Message.ToString());                    
                    return false;
                }
            });
        } 
        /// This method is used for to check whether particular element is present or not.     
        public bool IsPresent( IWebElement locator)
        {
            bool variable = false;
            try
            {
                IWebElement element = locator;
                variable = element != null;
            }
            catch (NoSuchElementException)
            {
                logger.Debug("No such Element Found");
            }
            return variable;
        }

        /// This method is used for to check whether List of WebElement is present or not.        
        public IWebElement GetElement( By elementIdentifier)
        {
            var element = Driver.FindElement(elementIdentifier);
            try
            {
                if (element != null && element.Displayed)
                {
                    logger.Info("Element is present");
                }
            }
            catch (NoSuchElementException)
            {
                logger.Error("Element is not present");
            }
            return Driver.FindElement(elementIdentifier);
        }
       
        ///  To Conmpare the Two List Object        
        public bool CompareList<T>(IEnumerable<T> list1, IEnumerable<T> list2) where T : class, IEquatable<T>
        {
            if (list1 == null)
                return (list2 == null);
            if (list2 == null)
                return false;
            IEnumerator<T> list2Iter = list2.GetEnumerator();
            foreach (T item in list1)
            {
                T object1 = item;
                if (!list2Iter.MoveNext())
                    // check if List2 ends before list 1 
                    return false;
                T object2 = list2Iter.Current;
                if (object1 == null)
                {
                    if (object2 != null)
                        return false;
                }
                else if (!object1.Equals(object2))
                    return false;
            }
            return !list2Iter.MoveNext();
        }

        public IWebElement Locate( By elementIdentifier)
        {
            try
            {
                Sync.ElementIsVisible(Driver, elementIdentifier);
                return Driver.FindElement(elementIdentifier);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                logger.Debug(e.Message.ToString());                
            }
            return null;
        }

        public void Click(By elementIdentifier)
        {
            try
            {
                Sync.ElementIsVisible(Driver, elementIdentifier);
                Driver.FindElement(elementIdentifier).Click();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                logger.Debug(e.Message.ToString());
            }
            
        }

        ///  Wait for Particular Element to be displayed       
        public void WaitElement(IWebElement Element)
        {
            WebDriverWait wait = new WebDriverWait(Driver, defaultTimeSpan);
                     wait.Until<bool>((d) =>
            {
                try
                {
                    if (Element.Displayed)
                        return true;
                    else
                        return false;
                }
                catch (NoSuchElementException)
                {
                    logger.Error("Element is not present");
                    return false;
                }

            });
        }

        /// TO find elements by tagname   
        public IList<IWebElement> FindElements( By element, By elementIdentifier)
        {
            IList<IWebElement> list = Locate(element).FindElements(elementIdentifier);
            return list;
        }

        /// TO find elements by tagname   
        public IList<IWebElement> FindElements(IWebElement element, By elementIdentifier)
        {
            IList<IWebElement> list = element.FindElements(elementIdentifier);
            return list;
        }

        /// TO find elements by driver   
        public IList<IWebElement> FindElementsByDriver(By element)
        {
            IList<IWebElement> list = Driver.FindElements(element);
            return list;
        }

        //To Find whether the element is displayed or not
        public bool IsDisplayed( By locator)
        {
            bool elementPresent;
            try
            {
                Driver.FindElement(locator);
                elementPresent = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                elementPresent = false;
            }
            return elementPresent;
        }
  
        
        public void WaitForLoadingDialog( int time)
        {
            try
            {
               new WebDriverWait(Driver, TimeSpan.FromSeconds(20)).Until(d => ((IJavaScriptExecutor)Driver).ExecuteScript("return document.querySelector('.k-loading-image').style.display").Equals("none"));
            }
            catch
            {
                //Do nothing as the driver cannot find the element.
            }
        }
        /// A temp method to pause the execution for few seconds

        public void WaitForSpecificTime(int seconds)
        {
            var waitTime = new TimeSpan(0, 0, seconds);
            var waitUntil = DateTime.Now + waitTime;

            while (DateTime.Now <= waitUntil)
            {
                System.Threading.Thread.Sleep(1000);
            }
        }

        /// To Get a Current folder path
        public String GetCurrentFolderPath()
        {
            string path = Path.GetDirectoryName(Assembly.GetAssembly(typeof(WebPageBase)).CodeBase);
            path = path.Replace("bin\\", "");
            path = path.Replace("Debug", "");
            return path;
        }

          
        
        /// This method is used for handle popup alert        
        public void PopupAlert(IWebDriver driver)
        {           
            IWebDriver popup;
            var windowIterator = driver.WindowHandles;
            foreach (var windowHandle in windowIterator)
            {
                popup = driver.SwitchTo().Window(windowHandle);
                popup.FindElement(By.Name("ok")).Submit();
            }
        }

        public void WaitUntilOperationCompletes( StringBuilder sbScript)
        {
            int totalAttempts = 3;
            int currentAttempts = 0;
            WebDriverWait WaitTillComplete = new WebDriverWait(Driver, TimeSpan.FromSeconds(80));
            while (currentAttempts < totalAttempts)
            {
                try
                {
                    WaitTillComplete.Until(driver1 => ((IJavaScriptExecutor)Driver).ExecuteScript(sbScript.ToString()).Equals("success"));
                    break;
                }
                catch { currentAttempts++; }
            }
            if (currentAttempts >= 3)
                throw new Exception("3 wait attempts exceeded. Still getting TimeOutException");
        }

        
        public void WaitTillPageLoad(string page)
        {
            string baseUrl = ConfigurationManager.AppSettings["URL"];
            StringBuilder sbScript;            
            sbScript = new StringBuilder();
            if (!page.ToUpperInvariant().Equals("DYNAMIC)"))
            {
                sbScript.Append("if (document.URL == '" + baseUrl + "/" + page + "' && document.readyState ==  'complete') {return 'success';} else {return 'failure';}");
            }
            else
            {
                sbScript.Append("if (document.readyState ==  'complete') {return 'success';} else {return 'failure';}");
            }
            WaitUntilOperationCompletes(sbScript);
        }


        //clicking Icon in any page
        public void ClickIcon(string icon)
        {
            string iconname = icon.ToLowerInvariant();
            By locator = By.CssSelector("img[src='/AppCore/Content/icon/" + iconname + ".svg']");
            var roleOperation = Locate(locator);
            WaitForElement(roleOperation, 10);
            roleOperation.Click();
        }

    }
}
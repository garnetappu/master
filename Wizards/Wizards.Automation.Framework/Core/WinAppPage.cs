﻿/*
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;
using NLog;

namespace Wizards.Automation.Framework.Core
{
    public class WinAppPage
    {
        readonly string time = ConfigurationManager.AppSettings["WaitTime"];
        readonly int waitTime;
        private readonly Logger logger = LogManager.GetCurrentClassLogger();

        protected WinWindow CurrentWindow { get; set; }

        public WinAppPage(WinWindow WindowDriver)
        {
            this.waitTime = string.IsNullOrEmpty(time) ? 2000 : Convert.ToInt32(time);
            this.CurrentWindow = WindowDriver;
        }

        protected WinWindow GetMainWindow()
        {
            return CurrentWindow;
        }

        public T FindElement<T>(params string[] searchProperties)
        {
            UITestControl control = (UITestControl)Activator.CreateInstance(typeof(T), new object[] { GetMainWindow() });
            control.SearchProperties.Add(searchProperties);
            return (T)(object)control;
        }

        public T FindElement<T>(UITestControl context, params string[] searchProperties)
        {
            UITestControl control = (UITestControl)Activator.CreateInstance(typeof(T), new object[] { context });
            control.SearchProperties.Add(searchProperties);
            return (T)(object)control;
        }

        public T FindElement<T>(string[] searchProperties, string[] filterProperties)
        {
            UITestControl control = (UITestControl)Activator.CreateInstance(typeof(T), new object[] { GetMainWindow() });
            control.SearchProperties.Add(searchProperties);
            control.FilterProperties.Add(filterProperties);
            return (T)(object)control;
        }


        public void MouseClick(UITestControl control)
        {
            try
            {
                Mouse.Click(control);
                //  logger.Info("Element" + control.FriendlyName + "Clicked");
            }
            catch (Exception e)

            {
                //  logger.Debug(e.Message.ToString());
            }
        }

        public void EnterText(UITestControl control, string text)
        {
            WinEdit winEdit = (WinEdit)control;
            winEdit.Text = text;
        }

        public bool WaitForControlEnabled(UITestControl control)
        {
            try
            {
                control.WaitForControlEnabled(waitTime);
                logger.Info("Element" + control.FriendlyName + "active");
                return true;
            }
            catch (Exception e)
            {
                logger.Debug(e.Message.ToString());
            }
            return false;
        }

        public bool WaitForControlReady(UITestControl control)
        {
            try
            {
                control.WaitForControlReady(waitTime);
                logger.Info("Element" + control.FriendlyName + "ready");
                return true;
            }
            catch (Exception e)
            {
                logger.Debug(e.Message.ToString());
            }
            return false;
        }

        public bool WaitForControlExist(UITestControl control)
        {
            try
            {
                control.WaitForControlExist(waitTime);
                logger.Info("Element" + control.FriendlyName + "Present");
                return true;
            }
            catch (Exception e)
            {
                logger.Debug(e.Message.ToString());
            }
            return false;
        }

        public bool WaitForControlNotExist(UITestControl control)
        {
            try
            {
                control.WaitForControlNotExist(waitTime);
                logger.Info("Element" + control.FriendlyName + "Present");
                return true;
            }
            catch (Exception e)
            {
                logger.Debug(e.Message.ToString());
            }
            return false;
        }

        public bool WaitForControlPropertyEqual(string PropertyName, Object propertyValue, UITestControl control)
        {
            try
            {
                control.WaitForControlPropertyEqual(PropertyName, propertyValue, waitTime);
                logger.Info("Element" + control.FriendlyName + "Value met expectation");
                return true;
            }
            catch (Exception e)
            {
                logger.Debug(e.Message.ToString());
            }
            return false;
        }

        public bool WaitForControlPropertyNotEqual(string PropertyName, Object propertyValue, UITestControl control)
        {
            try
            {
                control.WaitForControlPropertyNotEqual(PropertyName, propertyValue, waitTime);
                logger.Info("Element" + control.FriendlyName + PropertyName + "value met expectation");
                return true;
            }
            catch (Exception e)
            {
                logger.Debug(e.Message.ToString());
            }
            return false;
        }

          public static bool CloseWindow( UITestControl testControl)
           {
               var closeSuccess =
                   testControl.WaitForControlCondition(
                       tc =>
                       {
                           closeCallback();
                           return !tc.Exists;
                       }, 3000);
               return closeSuccess;
           }
    }
}
*/

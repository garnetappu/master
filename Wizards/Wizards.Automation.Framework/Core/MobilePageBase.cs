﻿using Wizards.Automation.Framework.Utils;
using NLog;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Interfaces;
using OpenQA.Selenium.Support.UI;
using System;
using OpenQA.Selenium.Appium.iOS;
using System.Collections.Generic;

namespace Wizards.Automation.Framework.Core
{
    //@Author : Saravanakumar
    public class MobilePageBase
    {        
        protected IOSDriver<AppiumWebElement> Appiumdriver;
        protected TimeSpan DefaultTimeSpan = new TimeSpan(0, 0, 90);
        protected Logger logger = LogManager.GetCurrentClassLogger();
       

        //Function for Hide Keyboard
        public void HideKeyboard()
        {
            try
            {
                Appiumdriver.HideKeyboard("Hide keyboard");
            }
            catch (Exception e)
            {
                logger.Debug(e.Message.ToString());
                logger.Error("Hide Keyboard is not working");
            }
        }

        //Enters the desired text value in the text field.
        public void SendKeys(By Element, string Value)
        {
            Locate(Element).SendKeys(Value);
        }

        //Performs tap on a particular element. 
        public void TapElement(By Element)
        {
            Locate(Element).Tap(1, 1);
        }

        //return the webDriver
        public IOSDriver<AppiumWebElement> GetDriver()
        {
            return Appiumdriver;
        }

        //Scroll and Tap on the specified element using expected String
        public void ScrollandSelect(List<AppiumWebElement> list, string name)
        {
            bool flag = true;
            int Startx, Starty, Endx, Endy;
            while (flag)
            {
                foreach (AppiumWebElement Element in list)
                {
                    if (Element.Text.Equals(name))
                    {                       
                        flag = false;
                        break;
                    }
                }
                if (flag)
                {
                    Startx = list[0].Location.X;
                    Starty = list[0].Location.Y;
                    Endx = list[list.Count - 1].Location.X;
                    Endy = list[list.Count - 1].Location.Y;
                    GetDriver().Swipe(Startx, Starty, Endx, Endy, 1000);
                }
            }
        }

        //Confirm the presence of List of mobile elements in UI.
        public void ScrollandTap_IOS(AppiumWebElement FirstElement, AppiumWebElement LastElement, AppiumWebElement ExpectedNameToBeClicked)
        {
            int StartX, StartY, EndX, EndY;
            StartX = FirstElement.Location.X;
            StartY = FirstElement.Location.Y;
            EndX = LastElement.Location.X;
            EndY = LastElement.Location.Y;
            for (int i = 1; i <= 50; i++)
            {
                if (ExpectedNameToBeClicked.Displayed)
                { 
                    break;
                }               
                GetDriver().Swipe(EndX, EndY, StartX, StartY, 1000);
            }
        }

        //To Search a Webelement and wait until its displayed
        public AppiumWebElement Locate(By elementIdentifier)
        {
            AppiumWebElement element = null;
            try
            {
                element = Appiumdriver.FindElement(elementIdentifier);
            }
            catch (NoSuchElementException e)
            {
                new WebDriverWait(Appiumdriver, DefaultTimeSpan)
                   .Until(ExpectedConditions.ElementIsVisible(elementIdentifier));
                element = Appiumdriver.FindElement(elementIdentifier);

                Console.WriteLine(e.Message.ToString());
                logger.Debug(e.Message.ToString());
            }
            catch (Exception e)
            {
                logger.Debug(e.Message.ToString());
                CaptureScreenshotUtil.SaveScreenShot(this.Appiumdriver, "Failed at");
            }
            return element;
        }

        //Wait until the element appears
        public void WaitForElement(AppiumDriver<AppiumWebElement> driver, AppiumWebElement Element, int seconds)
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(seconds));
            wait.Until<bool>((d) =>
            {
                try
                {
                    if (Element.Displayed)
                        return true;
                    else
                        return false;
                }
                catch (NoSuchElementException)
                {
                    logger.Error("Element is not present");
                    return false;
                }
            });
        }

        //Verify if element is available 
        public bool IsDisplayed(By locator)
        {
            bool elementPresent = false;
            try
            {
                Appiumdriver.FindElement(locator);
                
                elementPresent = true;

            }
            catch (NoSuchElementException e)
            {
                logger.Debug("Element not present", e.StackTrace);
            }
           
            return elementPresent;
        }

        public bool IsVisible(By locator)
        {
            bool elementVisible = false;
            try
            {
                elementVisible = Appiumdriver.FindElement(locator).Selected;
            }
            catch (NoSuchElementException e)
            {
                logger.Debug("Element not present", e.StackTrace);
            }
            return elementVisible;
        } 

        //Method : To remove the last letter in a string
        public string RemovingLastLetter(string stringName)
        {
            string contentAfterRemoving = stringName.Remove(stringName.Length - 1);
            string finalStringValue = contentAfterRemoving.Trim();
            return finalStringValue;
        }

        //Method : To remove the first letter in a string
       public string RemovingFirstLetter(string stringName)
        {
            string contentAfterRemoving = stringName.Substring(1, stringName.Length - 1);
            string finalStringValue = contentAfterRemoving.Trim();
            return finalStringValue;
        }
    }
}
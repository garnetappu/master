﻿/*
using System;
using System.Configuration;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Wizards.Automation.Framework.DataAccess;
using Wizards.Automation.Framework.API;
using Wizards.Automation.Framework.Reports.HTML;
using System.Reflection;

namespace Wizards.Automation.Framework.Core
{
    [CodedUITest]
    public class WinAppTest
    {
        protected ApplicationUnderTest Application { get; set; }
        protected WinWindow MainWindow { get; set; }
        
        ReadTestData objReadTestData;
      
        public static Data testData;
        HtmlReport report;
        



        [TestInitialize]
        public void LaunchApplication()
        {
           string[] classNameArray = TestContext.FullyQualifiedTestClassName.Split('.');
            int length = classNameArray.Length;
            ReadTestData.fileName = classNameArray[length - 1];
            report = new HtmlReport();
            objReadTestData = new ReadTestData();
            ReadTestData.testName = TestContext.TestName;
            report.StartReport(TestContext.TestName, "");
            testData = new Data();
            testData = objReadTestData.ReadData<Data>();

            string ApplicationPath;
            if (TestContext.TestName.Equals("WER_OpeningFirstTime"))
            {
               ApplicationPath = ConfigurationManager.AppSettings["WERAppInstall"];
            }
             ApplicationPath = ConfigurationManager.AppSettings["WERApp"];
             Application = ApplicationUnderTest.Launch(ApplicationPath);
             MainWindow = new WinWindow(Application);
            // WinWindow popupwindow = FindWindow(WinWindow.PropertyNames.ClassName, "#32770", WinWindow.PropertyNames.MaxDepth, "3");

            //below if code is to handle the sporadic pop up come which comes 
            //after the launch of the application saying "server down"....

           if (popupwindow == MainWindow)
                
            {

                popupwindow.WaitForControlExist();
                UITestControl control = (UITestControl)Activator.CreateInstance(typeof(UITestControl), new object[] { popupwindow });
                control.SearchProperties.Add(WinWindow.PropertyNames.ClassName, "#32770");
                control.SetFocus();
                Keyboard.SendKeys("{Enter}");
            } 
            
        }

        [TestCleanup]
        public void CloseApplication()
        {
            //Application.Close();
            UnitTestOutcome status = TestContext.CurrentTestOutcome;
            report.logStepWindows(status.ToString(), WinAppTest.GetErrorStackTraceFromTestContext(TestContext));

            //Script to update the spira test run
         //   test.SpiraPostServiceCall(status.ToString(), testData.ArtifactTypeId, testData.TestCaseId, testData.TestRunTypeId, testData.TestName, testData.TestRunFormatId, testData.TestRunURI1, testData.TestRunURI2);
        }

        /// <summary>
        /// Returns a window matching the given properyy
        /// </summary>
        /// <param name="searchProps"> search properties</param>
        /// <returns>found window</returns>
        public WinWindow FindWindow(params String[] searchProps)
        {
            MainWindow = new WinWindow();
            MainWindow.SearchProperties.Add(searchProps);
            return MainWindow;
            
        }

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;


        public static string GetErrorStackTraceFromTestContext(TestContext testContext)
        {
            const BindingFlags privateGetterFlags = System.Reflection.BindingFlags.GetField |
                                                    System.Reflection.BindingFlags.GetProperty |
                                                    System.Reflection.BindingFlags.NonPublic |
                                                    System.Reflection.BindingFlags.Instance |
                                                    System.Reflection.BindingFlags.FlattenHierarchy;

            var m_message = string.Empty; // Returns empty if TestOutcome is not failed
            if (testContext.CurrentTestOutcome == UnitTestOutcome.Failed)
            {
                // Get hold of TestContext.m_currentResult.m_errorInfo.m_stackTrace (contains the stack trace details from log)
                var field = testContext.GetType().GetField("m_currentResult", privateGetterFlags);
                var m_currentResult = field.GetValue(testContext);
                field = m_currentResult.GetType().GetField("m_errorInfo", privateGetterFlags);
                var m_errorInfo = field.GetValue(m_currentResult);
                field = m_errorInfo.GetType().GetField("m_stackTrace", privateGetterFlags);
                m_message = field.GetValue(m_errorInfo) as string;
            }

            return m_message;
        }
    }
}
*/

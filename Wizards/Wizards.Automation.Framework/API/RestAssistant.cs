﻿using Wizards.Automation.Framework.DataAccess;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NLog;
using RestSharp;
using System;
using System.Data;
using System.Net;
using System.Net.Http;
using System.Collections.Generic;
using System.Configuration;


namespace Wizards.Automation.Framework.API
{
    public class RestAssistant
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        public string actualJsonResponse = "";
        public string targetDatabaseResponse = "";
        public JToken jsonTokenValue = null;
        public JToken databaseTokenValue = null;
        public int jsonResponseCount;
        public int databaseResponseCount;
        public string authorazationTokenValue = null;
        public string authorazationTokenType = null;
		readonly List<string> dataBlockArray = new List<string>();

        /// <summary>
        /// Plain GET request
        /// </summary>
        /// <param name="baseUrl"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public RestResponse GetServiceCall(string baseUrl, RestRequest request)
        {

            var client = new RestClient(baseUrl);

            request.AddHeader("Accept", "application/json");
            request.AddHeader("Authorization", authorazationTokenType + " " + authorazationTokenValue);

            request.Method = Method.GET;
            request.RequestFormat = DataFormat.Json;

            var response = client.Execute(request);

            if (response.ErrorException != null)
            {
                const string message = "Error retriving response. Check inner details for more info";
                var restException = new ApplicationException(message, response.ErrorException);
                logger.Error(restException);
                throw restException;
            }

            logger.Info(response.Content);

            //Parse json into required format if the status check is ok
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                GetJsonResponse(response);
            }

            return response as RestResponse;
        }
        /// <summary>
        /// GET Request with auto Json Deserilization
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="baseUrl"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public RestResponse<T> GetServiceCall<T>(string baseUrl, RestRequest request) where T : new()
        {

            var client = new RestClient(baseUrl);

            request.Method = Method.GET;
            request.RequestFormat = DataFormat.Json;

            var response = client.Execute<T>(request);

            if (response.ErrorException != null)
            {
                const string message = "Error retriving response. Check inner details for more info";
                var restException = new ApplicationException(message, response.ErrorException);
                logger.Error(restException);
                throw restException;
            }
            logger.Info(response.Content);
            return response as RestResponse<T>;
        }

        /// <summary>
        /// Plain PUT Request
        /// </summary>
        /// <param name="baseUrl"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public RestResponse PutServiceCall(string baseUrl, RestRequest request)
        {

            var client = new RestClient(baseUrl);

            request.Method = Method.PUT;
            request.RequestFormat = DataFormat.Json;

            var response = client.Execute(request);

            if (response.ErrorException != null)
            {
                const string message = "Error retriving response. Check inner details for more info";
                var restException = new ApplicationException(message, response.ErrorException);
                logger.Error(restException);
                throw restException;
            }
            logger.Info(response.Content);
            return response as RestResponse;
        }

   //     /// <summary>
   //     /// Plain POST Request
   //     /// </summary>
   //     /// <param name="baseUrl"></param>
   //     /// <param name="request"></param>
   //     /// <returns></returns>
   //     public RestResponse PostServiceCall(string baseUrl, RestRequest request)
   //     {

   //         var client = new RestClient(baseUrl);

			//request.AddHeader("Accept", "application/json");
			//request.AddHeader("Authorization", authorazationTokenType + " " + authorazationTokenValue);

   //         request.Method = Method.POST;
   //         request.RequestFormat = DataFormat.Json;

   //         var response = client.Execute(request);

   //         if (response.ErrorException != null)
   //         {
   //             const string message = "Error retriving response. Check inner details for more info";
   //             var restException = new ApplicationException(message, response.ErrorException);
   //             logger.Error(restException);
   //             throw restException;
   //         }
   //         Console.WriteLine("Post Reply : " + response.Content);
   //         logger.Info(response.Content);

   //         //Parse json into required format if the status check is ok
   //         if (response.StatusCode == System.Net.HttpStatusCode.Created)
   //         {
   //             ParsePostedResponse(response);
   //         }

   //         return response as RestResponse;
   //     }

        /// <summary>
        /// Plain Update (PATCH) Request
        /// </summary>
        /// <param name="baseUrl"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public RestResponse PatchServiceCall(string baseUrl, RestRequest request)
        {

            var client = new RestClient(baseUrl);

            request.Method = Method.PATCH;
            request.RequestFormat = DataFormat.Json;

            var response = client.Execute(request);

            if (response.ErrorException != null)
            {
                const string message = "Error retriving response. Check inner details for more info";
                var restException = new ApplicationException(message, response.ErrorException);
                logger.Error(restException);
                throw restException;
            }
            logger.Info(response.Content);
            return response as RestResponse;
        }

        /// <summary>
        /// Delete Request
        /// </summary>
        /// <param name="baseUrl"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public RestResponse DeleteServiceCall(string baseUrl, RestRequest request)
        {

            var client = new RestClient(baseUrl);
            request.Method = Method.DELETE;
            request.RequestFormat = DataFormat.Json;

            var response = client.Execute(request);

            if (response.ErrorException != null)
            {
                const string message = "Error retriving response. Check inner details for more info";
                var restException = new ApplicationException(message, response.ErrorException);
                logger.Error(restException);
                throw restException;
            }
            logger.Info(response.Content);
            return response as RestResponse;
        }


        public void GetAuthorizationToken(string userName, string password, string tokenUri)
        {
            string response = string.Empty;
            try
            {
                var pairs = new List<KeyValuePair<string, string>>
                        {
                            new KeyValuePair<string, string>( "grant_type", "password" ), 
                            new KeyValuePair<string, string>( "userName", userName ), 
                            new KeyValuePair<string, string> ( "password", password )
                        };
                var content = new FormUrlEncodedContent(pairs);
                using (var client = new HttpClient())
                {
                    var postResponse = client.PostAsync(tokenUri, content).Result;
                    response = postResponse.Content.ReadAsStringAsync().Result;
                    var parsedResponse = JValue.Parse(response);
                    authorazationTokenValue = parsedResponse.SelectToken("access_token").ToString();
                    authorazationTokenType = parsedResponse.SelectToken("token_type").ToString();

                }
            }
            catch (Exception ex)
            {                
                Console.WriteLine("Exception occured when getting authorization token : " + ex.StackTrace);                
            }
        }

        public string GetSpecificTokenValueFromUniqueID(string uniqueID, int indexValue = 0)
        {
            var uniqueValue = string.Empty;

            try
            {
                uniqueValue = jsonTokenValue[indexValue].SelectToken(uniqueID).ToString();
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception occured when selecting specific token value from " + uniqueID);
            }

            return uniqueValue;
        }
       
        public string ConvertDataTabletoString(string query)
        {
            string JSONString = string.Empty;

            try
            {
                DataTable dt = SqlDbManager.ExecuteQuery(query);
                JSONString = JsonConvert.SerializeObject(dt);

                databaseTokenValue = GetInjsonArrayFormat(JSONString);
                targetDatabaseResponse = JSONString;
                databaseResponseCount = GetResponseCount(targetDatabaseResponse);
            }
            catch (Exception e)
            {
                logger.Error("Error when creating datatables : " + e.InnerException);
            }

            return JSONString;
        }
        
        public JArray GetInjsonArrayFormat(string jsonString)
        {
            var arrayFormat = JArray.Parse(jsonString);
            return arrayFormat;
        }

        public JToken GetJsonResponse(IRestResponse response)
        {
            var responseContent = response.Content;
            var jvalueResponse = JValue.Parse(response.Content);
            if (responseContent.Contains("\"value\":"))
            {
                jsonTokenValue = jvalueResponse.SelectToken("value");
                actualJsonResponse = jsonTokenValue.ToString();
                if (!actualJsonResponse.StartsWith("[") && (!actualJsonResponse.EndsWith("]")))
                {
                    jsonTokenValue = actualJsonResponse;
                    return jsonTokenValue;
                }
                else
                {
                    actualJsonResponse = jsonTokenValue.ToString();
                    jsonResponseCount = GetResponseCount(actualJsonResponse);
                }

            }
            else if (responseContent.StartsWith("[") && responseContent.EndsWith("]"))
            {
                jsonTokenValue = jvalueResponse;
                jsonResponseCount = GetResponseCount(responseContent);
            }
            else
            {
                actualJsonResponse = "[" + jvalueResponse.ToString() + "]";
                jsonTokenValue = JValue.Parse(actualJsonResponse);
                jsonResponseCount = GetResponseCount(actualJsonResponse);
            }

            return jsonTokenValue;
        }        

        public bool IsJsonCountMatched()
        {
            bool isCountMatched = true;

            if (jsonResponseCount != databaseResponseCount)
            {
                isCountMatched = false;
                Console.WriteLine("Both response counts MISMATCHED: JSONCount = " + jsonResponseCount + ", DatabaseCount = " + databaseResponseCount);
            }

            return isCountMatched;
        }

        public int GetResponseCount(string responseString)
        {
            int ResponseCount = GetInjsonArrayFormat(responseString).Count;
            return ResponseCount;
        }   

        public void ParsePostedResponse(IRestResponse response)
        {
            jsonTokenValue = JValue.Parse("[" + response.Content + "]");
            jsonResponseCount = GetResponseCount(jsonTokenValue.ToString());
        }        

        public string GetColumnValue(string lookUpData, string keyValue, int indexValue = 0)
        {
            JToken columnValue = null;
            string getType = null;

            try
            {
                if (lookUpData == "DatabaseResponse")
                {
                    columnValue = databaseTokenValue[indexValue].SelectToken(keyValue);
                }
                else if (lookUpData == "JSONResponse")
                {
                    columnValue = jsonTokenValue[indexValue].SelectToken(keyValue);
                }

                getType = columnValue.Type.ToString();
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception when accessing " + lookUpData + " column value for : " + keyValue);
            }


            //Format Date into ISO Fromat
            if (getType.Equals("Date") && columnValue.ToString().Contains("M"))
            {
                string dateFormattedOutput = GetUtcFormattedDate(columnValue);
                return dateFormattedOutput;
            }

            return columnValue.ToString();
        }

        public string GetUtcFormattedDate(JToken columnValue)
        {
            string isoJson = JsonConvert.SerializeObject(columnValue);
            JsonSerializerSettings isoDateFormatSettings = new JsonSerializerSettings
            {
                //DateFormatHandling = DateFormatHandling.IsoDateFormat
                DateTimeZoneHandling = DateTimeZoneHandling.Utc
            };
            string output = JsonConvert.SerializeObject(isoJson, isoDateFormatSettings);
            string dateFormattedOutput = output.Substring(3, output.Length - 6);

            DateTimeOffset dateTimeOffset = DateTimeOffset.Parse(dateFormattedOutput);
            var dateUTCFormat = dateTimeOffset.UtcDateTime.ToString("o");

            return dateUTCFormat;
        }
        
		public string GetAssociatedPropertyValue(IRestResponse response, string lookupValue, string lookupKey, string filterKey)
		{
			var filterValue = "";
			GetJsonResponse(response);

			for (int indexValue = 0; indexValue < jsonResponseCount; indexValue++)
			{
				var columnValue = jsonTokenValue[indexValue].SelectToken(lookupKey).ToString();
				if (columnValue == lookupValue)
				{
					filterValue = jsonTokenValue[indexValue].SelectToken(filterKey).ToString().Replace("\r\n", " ");
					break;
				}
			}
			Console.WriteLine("The result is :: " + filterValue);
			return filterValue;
		}

		public string [] GetAssociatedDataBlockIDsWithGivenWorkspace(IRestResponse response, string lookupValue, string lookupKey, string filterKey)
		{
			GetJsonResponse(response);
			for (int indexValue = 0; indexValue < jsonResponseCount; indexValue++)
			{
				var columnValue = jsonTokenValue[indexValue].SelectToken(lookupKey).ToString();
				if (columnValue == lookupValue)
				{
					var filterValue = jsonTokenValue[indexValue].SelectToken(filterKey).ToString().Replace("\r\n", " ");
					dataBlockArray.Add(filterValue.ToString());
				}
			}			
			return dataBlockArray.ToArray();
		}

        public string GetAssociatedPropertyValueFromDatabaseRespone(string propertyName)
        {
            var propertyValue = databaseTokenValue[0].SelectToken(propertyName.ToLower()).ToString();
            Console.WriteLine("The cell value is :: " + propertyValue);
            return propertyValue;
        }

        public HttpStatusCode SpiraPostServiceCall(string executionStatus, params string[] obj)
        {
            var client = new RestClient(ConfigurationManager.AppSettings["SpiraURL"]);
            Console.WriteLine(ConfigurationManager.AppSettings["SpiraURL"]);
            int ReleaseId = Convert.ToInt32(ConfigurationManager.AppSettings["ReleaseId"]);
            int TestsetId = Convert.ToInt32(ConfigurationManager.AppSettings["TestSetId"]);


            var request = new RestRequest(obj[5] + ConfigurationManager.AppSettings["ProjectId"] + obj[6], Method.POST);
            
            request.RequestFormat = DataFormat.Json;
            int Status = executionStatus == "Passed" ? 2 : 1;

            var json = new JObject();
            json.Add("ReleaseId", ReleaseId);
            json.Add("TestSetId", TestsetId);
            json.Add("ArtifactTypeId", Convert.ToInt32(obj[0]));
            json.Add("IsAttachments", false);
            json.Add("EndDate", DateTime.Now);
            json.Add("ExecutionStatusId", Status);
            json.Add("StartDate", DateTime.Now);
            json.Add("TestCaseId", Convert.ToInt32(obj[1]));
            json.Add("TestRunId", null);
            json.Add("TestRunTypeId", Convert.ToInt32(obj[2]));
            json.Add("RunnerName", ConfigurationManager.AppSettings["RunnerName"]);
            json.Add("RunnerTestName", obj[3]);
            json.Add("TestRunFormatId", Convert.ToInt32(obj[4]));
            json.Add("ConcurrencyDate", DateTime.Now);


            JsonSerializerSettings microsoftDateFormatSettings = new JsonSerializerSettings
            {
                DateFormatHandling = DateFormatHandling.MicrosoftDateFormat
            };
            string microsoftJson = JsonConvert.SerializeObject(json, microsoftDateFormatSettings);
            request.Parameters.Clear();
            request.AddHeader("content-type", "application/json");
            request.AddHeader("Accept", "application/json");
            request.AddParameter("username", ConfigurationManager.AppSettings["SpiraUserName"], ParameterType.HttpHeader);
            request.AddParameter("api-key", ConfigurationManager.AppSettings["SpiraApiKey"], ParameterType.HttpHeader);
            request.AddParameter("application/json", microsoftJson, ParameterType.RequestBody);
            var response = client.Execute(request);
            Console.WriteLine(JValue.Parse(response.Content));


            return response.StatusCode;
        }

    }

}

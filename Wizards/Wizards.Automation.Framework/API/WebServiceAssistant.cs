﻿using System;
using System.IO;
using System.Net;
using System.Text;

namespace Wizards.Automation.Framework.API
{
    public class WebServiceAssistant
    {
       protected WebServiceAssistant()
        {

        }
        private static StringBuilder OutputBuilder;
        private static void WriteLine(string Message)
        {
            OutputBuilder.AppendLine(Message);
        }

        public static void InvokeRestService(string Address, string Method, string ContentType, string requestbody)
        {
            OutputBuilder = new StringBuilder();           
            switch (Method.ToLowerInvariant().ToString())
            {
                case "get": InvokeServiceWithGet(Address); break;
                case "put": InvokeServiceWithPut(Address, ContentType, requestbody); break;
                case "post": InvokeServiceWithPost(Address, ContentType, requestbody); break;
                case "delete": InvokeServiceWithDelete(Address); break;
            }
        }

        private static void InvokeServiceWithGet(string Address)
        {
            HttpWebRequest request;
            HttpWebResponse response = null;
            StreamReader reader;
            StringBuilder sbSource;

            if (Address == null) { throw new ArgumentNullException("address"); }

            try
            {
                // Create and initialize the web request  
                request = WebRequest.Create(Address) as HttpWebRequest;
                request.UserAgent = "REST Sample";
                request.KeepAlive = false;
                // Set timeout to 15 seconds  
                request.Timeout = 15 * 1000;
                request.Method = "GET";

                WriteLine("Response:");
                // Get response  
                response = request.GetResponse() as HttpWebResponse;

                if (request.HaveResponse == true && response != null)
                {
                    // Get the response stream  
                    reader = new StreamReader(response.GetResponseStream());

                    // Read it into a StringBuilder  
                    sbSource = new StringBuilder(reader.ReadToEnd());

                    // Console application output  
                    Console.WriteLine(sbSource.ToString());

                    WriteLine((int)response.StatusCode + " " + response.StatusDescription);
                    WriteLine("Content-Type: " + response.ContentType.ToString());
                    WriteLine("Content-Length: " + response.ContentLength.ToString());
                    WriteLine(sbSource.ToString());
                }

            }
            catch (WebException wex)
            {
                WriteLine("Response:");
                // This exception will be raised if the server didn't return 200 - OK   
                // Try to retrieve more information about the network error   
                if (wex.Response != null)
                {
                    using (HttpWebResponse errorResponse = (HttpWebResponse)wex.Response)
                    {
                        string msg = String.Format(
                            "The server returned '{0}' with the status code {1} ({2:d}).",
                            errorResponse.StatusDescription, errorResponse.StatusCode,
                            errorResponse.StatusCode);
                        WriteLine(msg);
                    }
                }
            }
            finally
            {
                if (response != null) { response.Close(); }
            }

        }

        private static void InvokeServiceWithDelete(string Address)
        {
            HttpWebRequest request;
            HttpWebResponse response = null;
            StreamReader reader;
            StringBuilder sbSource;

            if (Address == null) { throw new ArgumentNullException("address"); }

            try
            {
                // Create and initialize the web request   
                request = WebRequest.Create(Address) as HttpWebRequest;
                request.UserAgent = "REST Sample - Delete";
                request.KeepAlive = false;
                request.Timeout = 15 * 1000;
                request.Method = "DELETE";              

                // Get response   
                response = request.GetResponse() as HttpWebResponse;

                if (request.HaveResponse == true && response != null)
                {
                    // Get the response stream   
                    reader = new StreamReader(response.GetResponseStream());
                    sbSource = new StringBuilder(reader.ReadToEnd());
                    WriteLine("Response:");
                    WriteLine((int)response.StatusCode + " " + response.StatusDescription);
                    WriteLine("Content-Type: " + response.ContentType.ToString());
                    WriteLine("Content-Length: " + response.ContentLength.ToString());
                    WriteLine(sbSource.ToString());
                }
            }
            catch (WebException wex)
            {
                WriteLine("Response:");
                // This exception will be raised if the server didn't return 200 - OK   
                // Try to retrieve more information about the network error   
                if (wex.Response != null)
                {
                    using (HttpWebResponse errorResponse = (HttpWebResponse)wex.Response)
                    {
                        string msg = String.Format(
                            "The server returned '{0}' with the status code {1} ({2:d}).",
                            errorResponse.StatusDescription, errorResponse.StatusCode,
                            errorResponse.StatusCode);
                        WriteLine(msg);
                    }
                }
            }
            finally
            {
                if (response != null) { response.Close(); }
            }
        }

        private static void InvokeServiceWithPut(string Address, string ContentType, string requestbody)
        {
            HttpWebRequest request = null;
            HttpWebResponse response = null;
            StreamReader reader;
            StringBuilder sbSource;

            try
            {
                // Create and initialize the web request   
                request = WebRequest.Create(Address) as HttpWebRequest;
                request.UserAgent = "REST - Put";
                request.KeepAlive = false;
                request.Timeout = 15 * 1000;

                string urlEncodedBody = "";

                // check to see if this is single part url encoded body, because if so
                // it needs to be included in the OAuth signature.
                if (ContentType == "application/x-www-form-urlencoded")
                {
                    urlEncodedBody = requestbody;
                }

                // Call the right routine that addes the OAuth stuff. The OAuth 
                // parameters required are slightly different if getting the request token,
                // approving the token, or authorizating with access token, etc.


                request.Method = "PUT";
                if (!string.IsNullOrEmpty(ContentType))
                {
                    request.ContentType = ContentType;
                    request.ContentLength = requestbody.Length;
                    WriteLine("Content-Type: " + request.ContentType.ToString());
                    WriteLine("Content-Length: " + request.ContentLength.ToString());
                    WriteLine(requestbody);
                    
                    using (var stOut = new StreamWriter(request.GetRequestStream(), Encoding.ASCII))
                    {
                        stOut.Write(requestbody);
                        stOut.Close();
                    }
                }
                // Get response   
                response = request.GetResponse() as HttpWebResponse;

                WriteLine("Response:");
                // This is response headers
                StringBuilder sbHeader = new StringBuilder();
                foreach (string item in response.Headers.Keys)
                {
                    WriteLine(item + " : " + response.Headers[item]);
                    sbHeader.AppendLine(item + " : " + response.Headers[item]);
                }
                WriteLine("");
               
                // This is for the body
                if (request.HaveResponse == true && response != null)
                {
                    // Get the response stream   
                    reader = new StreamReader(response.GetResponseStream());
                  
                    // Read it into a StringBuilder   
                    sbSource = new StringBuilder(reader.ReadToEnd());

                    WriteLine((int)response.StatusCode + " " + response.StatusDescription);
                    WriteLine("Content-Type: " + response.ContentType.ToString());
                    WriteLine("Content-Length: " + response.ContentLength.ToString());
                    string sbResponse = sbSource.ToString();                    
                    WriteLine(sbResponse);
                }
                else { WriteLine("Response:"); }
            }
            catch (WebException wex)
            {
                WriteLine("Response:");
                // This exception will be raised if the server didn't return 200 - OK   
                // Try to retrieve more information about the network error   
                if (wex.Response != null)
                {
                    using (HttpWebResponse errorResponse = (HttpWebResponse)wex.Response)
                    {
                        string msg = String.Format("The server returned '{0}' with the status code {1} ({2:d}).", errorResponse.StatusDescription, errorResponse.StatusCode, errorResponse.StatusCode);
                        WriteLine(msg);
                        reader = new StreamReader(errorResponse.GetResponseStream());
                        WriteLine("Application Error Response");
                        sbSource = new StringBuilder(reader.ReadToEnd());
                        WriteLine(sbSource.ToString());
                    }
                }
            }
            finally
            {
                if (response != null) { response.Close(); }
            }
        }

        private static void InvokeServiceWithPost(string Address, string ContentType, string requestbody)
        {
            HttpWebRequest request = null;
            HttpWebResponse response = null;
            StreamReader reader;
            StringBuilder sbSource;

            try
            {
                // Create and initialize the web request   
                request = WebRequest.Create(Address) as HttpWebRequest;
                request.UserAgent = "AirWinWIn/1.0";
                request.KeepAlive = false;
                request.Timeout = 15 * 1000;
                string authHeader = "";
                string urlEncodedBody = "";

                // check to see if this is single part url encoded body, because if so
                // it needs to be included in the OAuth signature.
                if (ContentType == "application/x-www-form-urlencoded")
                {
                    urlEncodedBody = requestbody;
                }

                request.Headers.Add(HttpRequestHeader.Authorization, authHeader);
                request.Method = "POST";

                if (!string.IsNullOrEmpty(ContentType))
                {
                    request.ContentType = ContentType;
                    request.ContentLength = requestbody.Length;
                    WriteLine("Content-Type: " + request.ContentType.ToString());
                    WriteLine("Content-Length: " + request.ContentLength.ToString());
                    WriteLine(requestbody);                   
                    using (var stOut = new StreamWriter(request.GetRequestStream(), Encoding.ASCII))
                    {
                        stOut.Write(requestbody);
                        stOut.Close();
                    }
                }
                // Get response   
                response = request.GetResponse() as HttpWebResponse;

                WriteLine("Response:");
                // This is response headers
                StringBuilder sbHeader = new StringBuilder();
                foreach (string item in response.Headers.Keys)
                {
                    WriteLine(item + " : " + response.Headers[item]);
                    sbHeader.AppendLine(item + " : " + response.Headers[item]);
                }
                WriteLine("");
               
                // This is for the body
                if (request.HaveResponse == true && response != null)
                {
                    // Get the response stream   
                    reader = new StreamReader(response.GetResponseStream());
                   
                    // Read it into a StringBuilder   
                    sbSource = new StringBuilder(reader.ReadToEnd());

                    WriteLine((int)response.StatusCode + " " + response.StatusDescription);
                    WriteLine("Content-Type: " + response.ContentType.ToString());
                    WriteLine("Content-Length: " + response.ContentLength.ToString());
                    string sbResponse = sbSource.ToString();                  
                    WriteLine(sbResponse);
                }
                else { WriteLine("Response:"); }
            }
            catch (WebException wex)
            {
                WriteLine("Response:");
                // This exception will be raised if the server didn't return 200 - OK   
                // Try to retrieve more information about the network error   
                if (wex.Response != null)
                {
                    using (HttpWebResponse errorResponse = (HttpWebResponse)wex.Response)
                    {
                        string msg = String.Format(
                            "The server returned '{0}' with the status code {1} ({2:d}).",
                            errorResponse.StatusDescription, errorResponse.StatusCode,
                            errorResponse.StatusCode);
                        WriteLine(msg);
                        reader = new StreamReader(errorResponse.GetResponseStream());
                        WriteLine("Application Error Response");
                        sbSource = new StringBuilder(reader.ReadToEnd());
                        WriteLine(sbSource.ToString());

                    }
                }
            }
            finally
            {
                if (response != null) { response.Close(); }
            }
        }
    }
}
